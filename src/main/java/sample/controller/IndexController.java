package sample.controller;


import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import sample.model.*;
import sample.model.modelJSON.ProductJSON;
import sample.repo.*;
import sample.utility.Authorization;
import sample.utility.googleMaps.CreateResponse;
import sample.utility.googleMaps.GoogleResponse;
import sample.utility.googleMaps.Response;
import sample.utility.googleMaps.Result;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.*;

/**
 * Index page
 * gestione della pagina iniziale e del login
 */
@Controller
public class IndexController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private WarehouseRepository warehouseRepository;
    @Autowired
    private ProductWarehouseRepository productWarehouseRepository;
    @Autowired
    private MailConfigRepository mailConfigRepository;

    List<ProductJSON> ii = new ArrayList<>();

    /**
     * Pagina di login
     * @param model
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String greetingRoot(Model model) {
        return "login";
    }


    /**
     * Pagina dove vengono rendirizzati gli utenti che cercano
     * di accedere a un URL non permesso dal suo livello
     * di accesso
     * @param model
     * @return
     */
    @RequestMapping(value = "/notAuthorized", method = RequestMethod.GET)
    public String forbidden(Model model) {
        model = createMenu(model);


        return "notAuthorized";
    }

    /**
     * Pagina per la configuarazione della mail
     * in caso di prodotto critico
     * @param model
     * @return
     */
    @RequestMapping(value = "/mailConfig", method = RequestMethod.GET)
    public String mailConfig(Model model) {
        model = createMenu(model);

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Configurazione mail")) {
            return "redirect:/notAuthorized";
        }
        MailConfig mailConfig = mailConfigRepository.findOne(1l);
        model.addAttribute("mail", mailConfig);

        return "mailConfig";
    }

    /**
     * Salvataggio del oggetto MailConfig su database ricevuto dal form in post
     * Ci sarà sempre solo un oggetto nella tabella
     * @param model
     * @param mailConfig
     * @return
     */
    @RequestMapping(value = "/mailConfig", method = RequestMethod.POST)
    public String saveMail(Model model, @ModelAttribute MailConfig mailConfig) {
        MailConfig mc = mailConfigRepository.findOne(1l);
        mc.setObject(mailConfig.getObject());
        mc.setText(mailConfig.getText());

        mailConfigRepository.save(mc);

        return "redirect:/mailConfig";
    }


    /**
     * Pagina iniziale
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String dashboard(Model model) {

        model = createMenu(model);

        List<Item> items = new ArrayList<>();
        List<Item> inStock = new ArrayList<>();
        for (Item i : itemRepository.findAll()) {
            items.add(i);
            if (i.getLocation() instanceof Warehouse) {
                inStock.add(i);
            }
        }
        model.addAttribute("items", items.size());
        model.addAttribute("inStock", inStock.size());


        HashMap<Warehouse, HashMap<Article, ProductJSON>> itemWarehouseHashMap = new HashMap<>();
        ii.removeAll(ii);
        for (Article a : articleRepository.findAll()) {
            List<Item> i = itemRepository.findItemByArticle(a);

            if (a.isActive() && !i.isEmpty()) {
                int quantity = 0;
                int alarm = 0;
                boolean b = false;
                int warning = 0;
                String warehouse = "";
                Warehouse warehouse1 = null;
                int label = -1;
                for (Item item : i) {
                    if (item.getLocation() instanceof Warehouse) {
                        List<Product_warehouse> pw = productWarehouseRepository.findByArticleAndWarehouse(a, item.getLocation());
                        quantity = itemRepository.countItemByArticleAndLocation(a, item.getLocation());


                        alarm = pw.get(0).getAlert();
                        warning = pw.get(0).getWarining();
                        warehouse = item.getLocation().getName();

                        if (quantity <= warning) {
                            label = 1;
                        } else if (quantity > warning && quantity <= alarm) {
                            label = 0;
                        } else if (quantity > alarm) {
                            label = -1;
                        }

                        warehouse1 = (Warehouse) item.getLocation();
                        if (!itemWarehouseHashMap.containsKey(warehouse1)) {
                            itemWarehouseHashMap.put(warehouse1, new HashMap<>());
                        }
                        itemWarehouseHashMap.get(warehouse1).put(item.getArticle(), new ProductJSON(
                                a.getId(),
                                a.getBrand(),
                                a.isActive(),
                                warehouse,
                                a.getArticle_nr(),
                                a.getArticleCategory().getCategory(),
                                a.getName(),
                                quantity,
                                warning,
                                alarm,
                                a.isBoxItem(),
                                label));
                    }
                }

                if (quantity <= warning) {

                    if (a.getCheckNotification() != 3) {
                        a.setCheckNotification(2);
                    }
                } else if (quantity > warning && quantity <= alarm) {

                    if (a.getCheckNotification() != 3) {
                        a.setCheckNotification(1);
                    }
                } else if (quantity > alarm) {

                    if (a.getCheckNotification() != 3) {
                        a.setCheckNotification(0);
                    }
                }

                articleRepository.save(a);
                if (warehouse1 != null)
                    checkNotification(a, warehouse1);
            }
        }

        Set<Warehouse> listLocation = itemWarehouseHashMap.keySet();
        Iterator<Warehouse> iterW = listLocation.iterator();


        while (iterW.hasNext()) {
            Warehouse w = iterW.next();
            Set<Article> listArticle = itemWarehouseHashMap.get(w).keySet();
            Iterator<Article> iterA = listArticle.iterator();
            while (iterA.hasNext()) {
                Article article = iterA.next();
                ProductJSON productJson = (ProductJSON) itemWarehouseHashMap.get(w).get(article);
                ii.add(productJson);
            }
        }

        model.addAttribute("productAlarm", ii);

        int alarm = 0;
        int warining = 0;
        for (ProductJSON p : ii) {
            if (p.getLabel() == 1) {
                alarm++;
            }
            if (p.getLabel() == 0) {
                warining++;
            }
        }

        model.addAttribute("alarm", alarm);
        model.addAttribute("warining", warining);


        return "dashboard";
    }

    /**
     * Controllo se bisogna inviare l'email di progetto critico al responsabile del magazzino
     * @param article
     * @param warehouse
     */
    public void checkNotification(Article article, Warehouse warehouse) {
        String email = warehouse.getUsers().getEmail();

        switch (article.getCheckNotification()) {
            case 3:
                break;
            case 2:
                if (!warehouse.getUsers().getNotifications().getNotification().equals("busy")) {
                    send(email, "Allarme", warehouse.getName(), article.getArticle_nr());
                    article.setCheckNotification(3);
                } else {
                    String emailSub = warehouse.getUsers().getSubstitute().getEmail();
                    send(emailSub, "Allarme", warehouse.getName(), article.getArticle_nr());
                    article.setCheckNotification(3);
                }
                break;
            case 1:
                if (warehouse.getUsers().getNotifications().getNotification().equals("full")) {
                    send(email, "Preallarme", warehouse.getName(), article.getArticle_nr());
                    article.setCheckNotification(3);
                } else if (warehouse.getUsers().getNotifications().getNotification().equals("busy")) {
                    String emailSub = warehouse.getUsers().getSubstitute().getEmail();
                    send(emailSub, "Preallarme", warehouse.getName(), article.getArticle_nr());
                    article.setCheckNotification(3);
                }
                break;
            case 0:
                break;
        }

        articleRepository.save(article);
    }

    /**
     * Metodo che gestisce la creazione del menu
     * la lista contiene tutte le funzionalità permesse dall'utente loggato
     * controllo se mostrare o no lato html
     * @param model
     * @return
     */
    public Model createMenu(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        List<Functionality> functionalities = logged.getRole().getFunctionalities();
        List<String> func = new ArrayList<>();
        for (Functionality f : functionalities) {
            func.add(f.getFunctionality());
        }
        model.addAttribute("functionalites", func);
        return model;
    }

    /**
     * metodo per l'invio della mail in caso di prodotti critici
     * @param mailTO
     * @param status
     * @param warehouse
     * @param article
     */
    public void send(String mailTO, String status, String warehouse, String article) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        MailConfig mailConfig = mailConfigRepository.findOne(1l);

        mailMessage.setTo(mailTO);
        mailMessage.setSubject("[Avviso " + status + ": " + warehouse + "] \n " + mailConfig.getObject());
        mailMessage.setText("[Articolo: " + article + "]\n\n " + mailConfig.getText());

        (new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("invio messaggio ");
                javaMailSender.send(mailMessage);
                System.out.println("mail inviata");

            }
        })).start();
    }

    /**
     * EndPoint del WebService interno per la visualizzazione della mappa Google
     * chiamato da javascript
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/addressConverter", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    String addressConverter() throws IOException {
        List<Warehouse> warehouses = (ArrayList) warehouseRepository.findByIsActive(true);
        List<String> addresses = new ArrayList<String>();

        for (Warehouse w : warehouses) {
            addresses.add(w.getRoute() + " " + w.getNap() + " " + w.getNation());
        }

        List<Response> response = new ArrayList<Response>();
        String lat = "";
        String lng = "";

        for (Warehouse address : warehouseRepository.findByIsActive(true)) {
            Response r = new Response();
            GoogleResponse res = convertToLatLong(address.getRoute() + " " + address.getNap() + " " + address.getNation());
            if (res.getStatus().equals("OK")) {
                for (Result result : res.getResults()) {
                    lat = result.getGeometry().getLocation().getLat();
                    lng = result.getGeometry().getLocation().getLng();
                    r.setLat(lat);
                    r.setLng(lng);
                }
            } else {
                System.out.println(res.getStatus());
            }
            Warehouse w = warehouseRepository.findWarehouseByRoute(address.getRoute());

            int quantity = Integer.MAX_VALUE;
            int alarm = 0;
            int warning = 0;
            for (ProductJSON p : ii) {
                if (p.getLocation().equals(w.getName())) {
                    if (p.getQuantity() != 0) {
                        quantity = p.getQuantity();
                        alarm = p.getAlarm();
                        warning = p.getWarning();
                    }
                }
            }

            if (quantity > warning) {
                r.setSubtext("Stato del magazzino: disponibile");
            } else if (quantity > alarm && quantity <= warning) {
                r.setSubtext("Stato del magazzino: preallarme");
            } else if (quantity <= alarm) {
                r.setSubtext("Stato del magazzino: allarme");
            }

            r.setName(w.getName());
            r.setAddress(address.getRoute() + " " + address.getNap() + " " + address.getNation());
            response.add(r);
        }

        // Stringa da tornare al client
        String json = new CreateResponse().createResponse(response);
        return json;
    }

    /**
     * riceve un address e lo converte in latitudine e longitudine
     * tramite le API di Google
     * @param fullAddress
     * @return
     * @throws IOException
     */
    public GoogleResponse convertToLatLong(String fullAddress) throws IOException {
        final String URL = "http://maps.googleapis.com/maps/api/geocode/json";
        java.net.URL url = new URL(URL + "?address=" + URLEncoder.encode(fullAddress, "UTF-8") + "&sensor=false");

        URLConnection conn = url.openConnection();
        InputStream in = conn.getInputStream();
        ObjectMapper mapper = new ObjectMapper();
        GoogleResponse response = mapper.readValue(in, GoogleResponse.class);
        in.close();
        return response;
    }


}
