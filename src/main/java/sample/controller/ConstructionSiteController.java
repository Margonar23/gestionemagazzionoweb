package sample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sample.model.*;
import sample.model.modelJSON.CompanyJSON;
import sample.model.modelJSON.ConstructionSiteJSON;
import sample.model.modelJSON.ItemJSON;
import sample.model.modelJSON.PersonJSON;
import sample.repo.*;
import sample.utility.Authorization;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;


/**
 * Controller per la gestione dei cantieri
 */
@Controller
public class ConstructionSiteController {
    @Autowired
    ConstructioSiteRepository constructioSiteRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    PersonRepository personRepository;
    @Autowired
    RespMunicipalityRepository respMunicipalityRepository;
    @Autowired
    ItemRepository itemRepository;
    @Autowired
    ArticleRepository articleRepository;
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    LogOperationRepository logOperationRepository;
    @Autowired
    MovementRepository movementRepository;
    @Autowired
    private ProductWarehouseRepository productWarehouseRepository;

    /**
     * pagina principale dove viene visualizzata la lista dei cantieri
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/construction", method = RequestMethod.GET)
    public String constructionManagement(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione cantieri")) {
            return "redirect:/notAuthorized";
        }

        model.addAttribute("constrSiteList", constructioSiteRepository.findByIsActive(true));
        model.addAttribute("constrSiteListClosed", constructioSiteRepository.findByIsActive(false));
        return "constructionSite/constructionSite";
    }

    /**
     * Form di registrazione di un cantiere
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/construction/register", method = RequestMethod.GET)
    public String constructionRegistration(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Registrazione cantieri")) {
            return "redirect:/notAuthorized";
        }

        model.addAttribute("listOfChef", userRepository.findAll());
        model.addAttribute("newConstructioSite", new ConstructionSite());

        return "constructionSite/constructionSiteRegister";
    }

    /**
     * Salvataggio del cantiere nel db
     *
     * @param constructionSite
     * @param date
     * @param pManager
     * @param responsable
     * @param model
     * @param idPavimentazione
     * @param idCostruzione
     * @param idPersonPavimentazione
     * @param idPersonCostruzione
     * @return
     */
    @RequestMapping(value = "/construction/register", method = RequestMethod.POST)
    public String constructionRegistration(@ModelAttribute ConstructionSite constructionSite, @RequestParam("dateRespMun") String date, @RequestParam("pManager") String pManager, @RequestParam("responsable") String responsable, Model model, @RequestParam("idPavimentazione") String idPavimentazione, @RequestParam("idCostruzione") String idCostruzione, @RequestParam("idPersonPavimentazione") String idPersonPavimentazione, @RequestParam("idPersonCostruzione") String idPersonCostruzione) {

        // se il campo con l'id del impresa esiste -> carico la company gia presente nel db
        // altrimento procedo all'inserimento nel db
        if (idPavimentazione.equals("")) {
            Person flooringPerson;
            if (idPersonPavimentazione.equals("")) {
                flooringPerson = new Person();
                flooringPerson.setName(constructionSite.getFlooringCompany().getContact().getName());
                flooringPerson.setLastname(constructionSite.getFlooringCompany().getContact().getLastname());
                flooringPerson.setEmail(constructionSite.getFlooringCompany().getContact().getEmail());
                personRepository.save(flooringPerson);
            } else {
                flooringPerson = personRepository.findOne(Long.parseLong(idPersonPavimentazione));
            }
            Company notFoundPavimentazione = new Company();
            notFoundPavimentazione.setName(constructionSite.getFlooringCompany().getName());
            notFoundPavimentazione.setPhone(constructionSite.getFlooringCompany().getPhone());
            notFoundPavimentazione.setFax(constructionSite.getFlooringCompany().getFax());
            notFoundPavimentazione.setContact(flooringPerson);
            notFoundPavimentazione.setNap(constructionSite.getFlooringCompany().getNap());
            notFoundPavimentazione.setNation(constructionSite.getFlooringCompany().getNation());
            notFoundPavimentazione.setRoute(constructionSite.getFlooringCompany().getRoute());
            constructionSite.setFlooringCompany(notFoundPavimentazione);
            companyRepository.save(notFoundPavimentazione);
        } else {
            Company foundPavimentazione = companyRepository.findOne(Long.parseLong(idPavimentazione));
            constructionSite.setFlooringCompany(foundPavimentazione);
        }

        if (idCostruzione.equals("")) {
            Person firmContact;
            if (idPersonCostruzione.equals("")) {
                firmContact = new Person();
                firmContact.setName(constructionSite.getConstructionFirm().getContact().getName());
                firmContact.setLastname(constructionSite.getConstructionFirm().getContact().getLastname());
                firmContact.setEmail(constructionSite.getConstructionFirm().getContact().getEmail());
                personRepository.save(firmContact);
            } else {
                firmContact = personRepository.findOne(Long.parseLong(idPersonCostruzione));
            }

            Company constructionFirm = new Company();
            constructionFirm.setName(constructionSite.getConstructionFirm().getName());
            constructionFirm.setPhone(constructionSite.getConstructionFirm().getPhone());
            constructionFirm.setFax(constructionSite.getConstructionFirm().getFax());
            constructionFirm.setContact(firmContact);
            constructionFirm.setNap(constructionSite.getConstructionFirm().getNap());
            constructionFirm.setNation(constructionSite.getConstructionFirm().getNation());
            constructionFirm.setRoute(constructionSite.getConstructionFirm().getRoute());
            constructionSite.setConstructionFirm(constructionFirm);
            companyRepository.save(constructionFirm);
        } else {
            Company foundCostruzione = companyRepository.findOne(Long.parseLong(idCostruzione));
            constructionSite.setConstructionFirm(foundCostruzione);
        }


        RespMunicipality respMunicipality = new RespMunicipality();
        respMunicipality.setCredit(constructionSite.getRespMunicipality().getCredit());
        respMunicipality.setNumber(constructionSite.getRespMunicipality().getNumber());

        DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        Date d = new Date();
        try {
            d = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        respMunicipality.setDate(d);
        constructionSite.setRespMunicipality(respMunicipality);

        Users u = userRepository.findOne(Long.parseLong(pManager));
        constructionSite.setProjectManager(u);
        Users uu = userRepository.findOne(Long.parseLong(responsable));
        constructionSite.setResponsible(uu);


        respMunicipalityRepository.save(respMunicipality);


        constructionSite.setActive(true);
        ConstructionSite c = constructioSiteRepository.save(constructionSite);
        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Cantiere", "REGISTRAZIONE", c.getId());
        ////////////////////////END LOG OPERATION///////////////////
        return "redirect:/construction";
    }

    /**
     * Form di modifica di un cantiere
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/construction/update/{id}", method = RequestMethod.GET)
    public String constructionUpdate(@PathVariable("id") long id, Model model) {
        model = createMenu(model);

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione cantieri")) {
            return "redirect:/notAuthorized";
        }
        model.addAttribute("constructionSite", constructioSiteRepository.findOne(id));
        model.addAttribute("listOfChef", userRepository.findAll());
        model.addAttribute("date", constructioSiteRepository.findOne(id).getRespMunicipality().getDate().toString());


        return "constructionSite/constructionSiteUpdate";
    }

    /**
     * salvataggio su db del cantiere modificato
     *
     * @param id
     * @param constructionSite
     * @param pManager
     * @param responsable
     * @param model
     * @return
     */
    @RequestMapping(value = "/construction/update/{id}", method = RequestMethod.POST)
    public String constructionUpdate(@PathVariable("id") long id, @ModelAttribute ConstructionSite constructionSite, @RequestParam("pManager") String pManager, @RequestParam("responsable") String responsable, Model model) {
        constructionSite.setActive(true);
        Users u = userRepository.findOne(Long.parseLong(pManager));
        constructionSite.setProjectManager(u);
        Users uu = userRepository.findOne(Long.parseLong(responsable));
        constructionSite.setResponsible(uu);

        personRepository.save(constructionSite.getFlooringCompany().getContact());
        personRepository.save(constructionSite.getConstructionFirm().getContact());
        respMunicipalityRepository.save(constructionSite.getRespMunicipality());

        companyRepository.save(constructionSite.getConstructionFirm());
        companyRepository.save(constructionSite.getFlooringCompany());

        ConstructionSite c = constructioSiteRepository.save(constructionSite);

        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Cantiere", "MODIFICA", c.getId());
        ////////////////////////END LOG OPERATION///////////////////

        return "redirect:/construction";
    }


    /**
     * informazioni sul cantiere con @param id
     * inventario cantiere
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/construction/information/{id}", method = RequestMethod.GET)
    public String constructionInformation(@PathVariable("id") long id, Model model) {
        model = createMenu(model);


        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione cantieri")) {
            return "redirect:/notAuthorized";
        }
        ConstructionSite constructionSite = constructioSiteRepository.findOne(id);
        LinkedHashSet<Article> articles = new LinkedHashSet<>();
        for (Item item : itemRepository.findBylocation(constructioSiteRepository.findOne(id))) {
            articles.add(item.getArticle());
        }
        model.addAttribute("construction", constructionSite);
        model.addAttribute("articles", new ArrayList<>(articles));


        return "constructionSite/constructionSiteInformation";
    }

    /**
     * sposta un item dal cantiere @param id
     *
     * @param redir
     * @param articles
     * @param id
     * @return
     */
    @RequestMapping(value = "/construction/constructionToWarehouse/{id}", method = RequestMethod.POST)
    public ModelAndView constructionToWarehouse(RedirectAttributes redir, @RequestParam("checked") String[] articles, @PathVariable("id") long id) {

        ModelAndView modelAndView = new ModelAndView();
        ArrayList<Article> articleArrayList = new ArrayList<>();
        for (String s : articles) {
            Article a = articleRepository.findOne(Long.parseLong(s));
            a.setCheckNotification(0);
            articleRepository.save(a);
            articleArrayList.add(a);
        }
        modelAndView.setViewName("redirect:/construction/constructionToWarehouse/" + id);
        redir.addFlashAttribute("articles", articleArrayList);

        return modelAndView;
    }

    /**
     * elimina il cantiere passato in url
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/construction/delete/{id}", method = RequestMethod.GET)
    public String constructionDelete(@PathVariable("id") long id, Model model) {
        model = createMenu(model);

        ConstructionSite constructionSite = constructioSiteRepository.findOne(id);
        constructionSite.setActive(false);
        ConstructionSite c = constructioSiteRepository.save(constructionSite);
        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Cantiere", "ELIMINAZIONE", c.getId());
        ////////////////////////END LOG OPERATION///////////////////
        return "redirect:/construction";
    }

    /**
     * spostare un item dal cantiere @param id
     *
     * @param id
     * @param model
     * @param articles
     * @return
     */
    @RequestMapping(value = "/construction/constructionToWarehouse/{id}", method = RequestMethod.GET)
    public String constructionReturnWarehouse(@PathVariable("id") long id, Model model, @ModelAttribute("articles") ArrayList<Article> articles) {
        model = createMenu(model);
        model.addAttribute("construction", constructioSiteRepository.findOne(id));
        model.addAttribute("warehouse", warehouseRepository.findAll());
        model.addAttribute("articles", articles);

        return "constructionSite/constructionSiteToWarehouse";
    }

    /**
     * Riattivazione del cantiere @param id
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/construction/open/{id}", method = RequestMethod.GET)
    public String constructionOpen(@PathVariable("id") long id, Model model) {
        model = createMenu(model);
        ConstructionSite constructionSite = constructioSiteRepository.findOne(id);
        constructionSite.setActive(true);
        constructioSiteRepository.save(constructionSite);
        return "redirect:/construction";
    }

    /**
     * Spostamento effettivo del item
     * da @param idConstr
     * a @param WareID
     *
     * @param idConstr
     * @param items
     * @param WareID
     * @param model
     * @return
     */
    @RequestMapping(value = "/contruction/constructionToWarehouse/{id}", method = RequestMethod.POST)
    public String construction(@PathVariable("id") long idConstr, @RequestParam("items") String[] items, @RequestParam("warehouse") long WareID, Model model) {
        Location warehouse = warehouseRepository.findOne(WareID);
        Item item;

        for (String i : items) {
            item = itemRepository.findOne(Long.parseLong(i));
            Location locationFrom = item.getLocation();
            item.setLocation(warehouse);
            itemRepository.save(item);
            if (productWarehouseRepository.findByArticleAndWarehouse(item.getArticle(), item.getLocation()).size() == 0) {
                List<Product_warehouse> pw = productWarehouseRepository.findByArticle(item.getArticle());
                Product_warehouse product_warehouse = new Product_warehouse();
                product_warehouse.setAlert(pw.get(0).getAlert());
                product_warehouse.setWarining(pw.get(0).getWarining());
                product_warehouse.setWarehouse((Warehouse) warehouse);
                product_warehouse.setArticle(item.getArticle());
                productWarehouseRepository.save(product_warehouse);
            }


            // MOVEMENT
            Movement movement = new Movement();
            movement.setTo(warehouse);
            movement.setDate(LocalDate.now());
            movement.setItem(item);
            movement.setFrom(locationFrom);
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            Users logged = userRepository.findUsersByUsername(username);
            movement.setUsers(logged);
            movementRepository.save(movement);

        }

        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Magazzino", "MOVIMENTO", warehouse.getId());
        ////////////////////////END LOG OPERATION///////////////////

        return "redirect:/construction/information/" + idConstr;
    }

    /**
     * ricerca asincrona dei cantieri
     * gestito da js
     *
     * @param q
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/company/search", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<CompanyJSON> company(@RequestParam(value = "q", required = true) String q, Model model) {
        List<CompanyJSON> u = new ArrayList<>();
        for (Company us : companyRepository.findByNameContainingIgnoreCase(q)) {
            u.add(new CompanyJSON(us.getId(), us.getName(), us.getRoute(), us.getNap(), us.getNation(), us.getPhone(), us.getFax(), us.getContact().getName() + " " + us.getContact().getLastname()));
        }
        return u;
    }

    /**
     * Ricerca di una persona responsabile di un impresa di pavimentazione/costruzione
     * durante la creazione di un nuovo cantiere
     *
     * @param q
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/company/personPavimentazione/search", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<PersonJSON> personPavimentazione(@RequestParam(value = "q", required = true) String q, Model model) {
        List<PersonJSON> u = new ArrayList<>();
        for (Person us : personRepository.findByNameOrLastnameContainingIgnoreCase(q, q)) {
            u.add(new PersonJSON(us.getId(), us.getName(), us.getLastname(), us.getEmail()));
        }
        return u;
    }

    /**
     * Ritorna un json con la persona trovata durante la ricerca
     * utilizzato nella creazione dei cantieri
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/company/personPavimentazione/{id}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    PersonJSON singlePersonPavimentazione(@PathVariable("id") long id, Model model) {
        Person p = personRepository.findOne(id);
        return new PersonJSON(p.getId(), p.getName(), p.getLastname(), p.getEmail());

    }

    /**
     * ritorna un json rappresentate la company trovata
     * usato durante la creazione di un cantiere
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/company/{id}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    CompanyJSON singleCompany(@PathVariable("id") long id, Model model) {
        Company c = companyRepository.findOne(id);

        return new CompanyJSON(c.getId(), c.getName(), c.getRoute(), c.getNap(), c.getNation(), c.getPhone(), c.getFax(), c.getContact().getName() + " " + c.getContact().getLastname() + " (" + c.getContact().getEmail() + ")");
    }

    /**
     * ritorna la lista di item dato un determinato articlo e un determinato cantiere
     * usato per spostare gli item
     *
     * @param idArticle
     * @param idConstr
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/construction/itemListforArticle/{idConstr}/{idArticle}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<ItemJSON> item(@PathVariable("idArticle") long idArticle, @PathVariable("idConstr") long idConstr, Model model) {
        List<ItemJSON> itemJSONList = new ArrayList<>();
        for (Item item : itemRepository.findByArticleAndLocation(articleRepository.findOne(idArticle), constructioSiteRepository.findOne(idConstr))) {
            itemJSONList.add(new ItemJSON(item.getId(), item.getItemCod(), item.getQuantity()));
        }
        return itemJSONList;

    }

    /**
     * Ricerca di un cantiere
     *
     * @param q
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/constructionSite/search", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<ConstructionSiteJSON> usersJSON(@RequestParam(value = "q", required = true) String q, Model model) {
        List<ConstructionSiteJSON> u = new ArrayList<>();
        for (ConstructionSite us : constructioSiteRepository.findByNameContainingIgnoreCase(q)) {
            if (us.isActive()) {
                u.add(new ConstructionSiteJSON(
                        us.getId(),
                        us.getName(),
                        us.getRoute() + ", " + us.getNap() + " " + us.getNation(),
                        us.getRespMunicipality().getNumber(),
                        us.getConstructionFirm().getName(),
                        us.getFlooringCompany().getName(),
                        us.getProjectManager().getName() + " " + us.getProjectManager().getLastname() + " (" + us.getProjectManager().getUsername() + ")",
                        us.getResponsible().getName() + " " + us.getResponsible().getLastname() + " (" + us.getResponsible().getUsername() + ")"

                ));
            }
        }
        return u;
    }

    /**
     * Scrive il log delle operazioni su db
     *
     * @param data
     * @param operation
     * @param id
     */
    public void writeLog(String data, String operation, long id) {
        LogOperation log = new LogOperation();
        log.setDate(new Date(System.currentTimeMillis()));
        log.setUser(SecurityContextHolder.getContext().getAuthentication().getName());
        log.setData(data);
        log.setOperation(operation);
        log.setIdData(id);
        logOperationRepository.save(log);
    }

    /**
     * Metodo che gestisce la creazione del menu
     * la lista contiene tutte le funzionalità permesse dall'utente loggato
     * controllo se mostrare o no lato html
     *
     * @param model
     * @return
     */
    public Model createMenu(Model model) {
        // SECURITY
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        List<Functionality> functionalities = logged.getRole().getFunctionalities();
        List<String> func = new ArrayList<>();
        for (Functionality f : functionalities) {
            func.add(f.getFunctionality());
        }
        model.addAttribute("functionalites", func);
        return model;
        // END SECURITY
    }


}
