package sample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sample.model.*;
import sample.model.modelJSON.UsersJSON;
import sample.repo.*;
import sample.utility.Authorization;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Margonar on 27.02.2017.
 */

@Controller
public class UserController {

    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected UserRoleRepository userRoleRepository;
    @Autowired
    protected FunctionalityRepository functionalityRepository;
    @Autowired
    protected AuthoritiesRepository authoritiesRepository;
    @Autowired
    protected LogOperationRepository logOperationRepository;
    @Autowired
    protected NotificationRepository notificationRepository;


    /**
     * ritornate template user, aggiungendo al model la lista di utenti abilitati e disabilitati
     * @param model
     * @return
     */
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String userManagement(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione utenti")) {
            return "redirect:/notAuthorized";
        }


        model.addAttribute("usersList", userRepository.findUsersByEnabled(true));
        model.addAttribute("usersListDisable", userRepository.findUsersByEnabled(false));
        return "user/user";
    }

    /**
     * restituisce template html che permette di registrare un utente
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/register", method = RequestMethod.GET)
    public String userRegistration(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione ruoli")) {
            return "redirect:/notAuthorized";
        }

        model.addAttribute("newuser", new Users());
        model.addAttribute("roleList", userRoleRepository.findAll());
        return "user/userRegistration";
    }

    /**
     * riceve user e ruolo per inserire nel db il nuovo utente
     * @param model
     * @param roleId
     * @param users
     * @return
     */
    @RequestMapping(value = "/user/register", method = RequestMethod.POST)
    public String userRegistration(Model model, @RequestParam("role") String roleId, @ModelAttribute Users users) {

        users.setRole(userRoleRepository.findOne(Long.parseLong(roleId)));
        users.setPassword("password");
        users.setEnabled(true);
        users.setSubstitute(users);
        users.setNotifications(notificationRepository.findOne(2l));
        Users u;
        if (userRepository.findUsersByUsername(users.getUsername()) == null) {
            u = userRepository.save(users);
        } else {
            String username = users.getUsername();
            users.setUsername(null);
            u = userRepository.save(users);
            u.setUsername(username + u.getId());
            userRepository.save(u);
        }


        Authorities authorities = new Authorities();
        authorities.setUsername(u.getUsername());
        authorities.setAuthority(u.getRole().getRole());

        authoritiesRepository.save(authorities);

        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Utente", "REGISTRAZIONE", u.getId());
        ////////////////////////END LOG OPERATION///////////////////

        return "redirect:/user";
    }

    /**
     * controller che resetta la password dell'utente inserendo la password di default
     * @param model
     * @param id id utente a cui resettare password
     * @return richiesta get su url "user"
     */
    @RequestMapping(value = "/user/resetpassword/{id}", method = RequestMethod.GET)
    public String userResetPassword(Model model, @PathVariable("id") long id) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione utenti")) {
            return "redirect:/notAuthorized";
        }
        Users u = userRepository.findOne(id);
        u.setPassword("password");
        Users user = userRepository.save(u);

        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Utente", "MODIFICA PASSWORD", user.getId());
        ////////////////////////END LOG OPERATION///////////////////

        return "redirect:/user";
    }

    /**
     * restituisce template html che permette di modificare utente
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/update/{id}", method = RequestMethod.GET)
    public String userUpdate(@PathVariable("id") long id, Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione utenti")) {
            return "redirect:/notAuthorized";
        }
        model.addAttribute("userv", userRepository.findOne(id));
        model.addAttribute("roleList", userRoleRepository.findAll());
        return "user/userUpdate";
    }

    /**
     * modidica utente con id ricevuto, sostituisce i valori e salva
     * @param id
     * @param roleId
     * @param users
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/update/{id}", method = RequestMethod.POST)
    public String userUpdate(@PathVariable("id") long id, @RequestParam("role") String roleId, @ModelAttribute Users users, Model model) {

        Users u = userRepository.findOne(id);
        u.setName(users.getName());
        u.setLastname(users.getLastname());
        u.setEmail(users.getEmail());
        u.setRole(userRoleRepository.findOne(Long.parseLong(roleId)));

        Users users1 = userRepository.save(u);

        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Utente", "MODIFICA", users1.getId());
        ////////////////////////END LOG OPERATION///////////////////
        return "redirect:/user";
    }

    /**
     * modifica l'utente loggato
     * @param id user da moficare
     * @param roleId nuovo ruolo utente
     * @param users user modificato
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/logged/update/{id}", method = RequestMethod.POST)
    public String userLoggedUpdate(@PathVariable("id") long id, @ModelAttribute Users users, Model model) {

        Users u = userRepository.findOne(id);
        u.setName(users.getName());
        u.setLastname(users.getLastname());
        u.setEmail(users.getEmail());
        Users users1 = userRepository.save(u);

        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Utente", "MODIFICA", users1.getId());
        ////////////////////////END LOG OPERATION///////////////////
        return "redirect:/user/logged";
    }

    /**
     * restituisce template con informazioni necessari, log utente, utente ruoli, livelli di notifica
     * @param model
     * @return template per utente loggato
     */
    @RequestMapping(value = "/user/logged", method = RequestMethod.GET)
    public String userLogged(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("username", username);

        Users user = userRepository.findUsersByUsername(username);
        model.addAttribute("userLogged", user);
        model.addAttribute("roleList", userRoleRepository.findAll());
        model.addAttribute("userLogged",user);
        model.addAttribute("roleList",userRoleRepository.findAll());
        ArrayList<Notification> notifications = new ArrayList<>();
        notifications = (ArrayList<Notification>) notificationRepository.findAll();
        System.out.println(notifications.size());
        for (Notification notification : notifications) {
            System.out.println(notification.getNotification()+""+ notification.getId());
        }
        model.addAttribute("notifications",notificationRepository.findAll());

        List<LogOperation> logOperations = logOperationRepository.findLogOperationByUser(SecurityContextHolder.getContext().getAuthentication().getName());
        model.addAttribute("log", logOperations);
        model.addAttribute("log",logOperations);
        model.addAttribute("users",userRepository.findAll());


        return "user/userInformationLogged";
    }

    /**
     * cambia livello notifica utente
     * @param model
     * @param userSubstitute user sostituivo
     * @param notificationLevel nuovo livello notifiche utente
     * @return pagina utente loggato
     */
    @RequestMapping(value = "/user/logged/notification", method = RequestMethod.POST)
    public String userChangeLevelNotification(Model model, @RequestParam("usersubstitute") long userSubstitute,@RequestParam("notificationS") String notificationLevel){
        Notification n = notificationRepository.findOne(Long.parseLong(notificationLevel));
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        Users user = userRepository.findUsersByUsername(username);
        user.setNotifications(n);
        user.setBusy(false);

        if(Long.parseLong(notificationLevel) == 3){
            Users userSub = userRepository.findOne(userSubstitute);
            user.setSubstitute(userSub);
            user.setBusy(true);
        }
        userRepository.save(user);

        return "redirect:/user/logged";

    }

    /**
     * esegue dovuti controlli e cambia password
     * @param model
     * @param oldP vecchia password
     * @param newP nuova password
     * @param newPR  conferma per nuova password
     * @return pagina utente loggato
     */

    @RequestMapping(value = "/user/logged/changepassword", method = RequestMethod.POST)
    public String userChangePassword(Model model, @RequestParam("oldPassword") String oldP, @RequestParam("newPassword") String newP, @RequestParam("newPasswordRepet") String newPR) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        PasswordEncoder token = new BCryptPasswordEncoder();
        model.addAttribute("username", username);
        Users users = userRepository.findUsersByUsername(username);
        System.out.println(users + " " + username);
        boolean b = token.matches(oldP, users.getPassword());
        boolean isChanged = b && newP.equals(newPR);
        if (isChanged) {
            users.setPassword(newP);
            System.out.println("password cambiata");
            Users users1 = userRepository.save(users);
            model.addAttribute("changed", isChanged);

            //////////////////////LOG OPERATION/////////////////////////
            writeLog("Utente", "MODIFICA PASSWORD", users1.getId());
            ////////////////////////END LOG OPERATION///////////////////

            return "redirect:/user/logged";
        } else {
            model.addAttribute("changed", isChanged);
            return "redirect:/user/logged?error";
        }
    }

    /**
     *  retituisce la pagina di gestione dei ruoli, aggiunge al model la lista dei ruoli attuali,
     *  e un nuovo ruole nel caso se ne voglia creare un altro
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/roles", method = RequestMethod.GET)
    public String rolesManagement(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione ruoli")) {
            return "redirect:/notAuthorized";
        }
        model.addAttribute("roleList", userRoleRepository.findAll());
        model.addAttribute("roleObj", new UserRole());
        return "user/roles";
    }

    /**
     * aggiunta di un nuovo ruolo, lo salva de db
     * @param userRole ruolo da aggiungere
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/roles", method = RequestMethod.POST)
    public String rolesAdd(@ModelAttribute UserRole userRole, Model model) {
        UserRole u = userRoleRepository.save(userRole);
        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Ruolo", "REGISTRAZIONE", u.getId());
        ////////////////////////END LOG OPERATION///////////////////
        return "redirect:/user/roles";
    }

    /**
     *  restituisce  template per modificare ruolo
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/roles/update/{id}", method = RequestMethod.GET)
    public String updateRole(@PathVariable("id") long id, Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione ruoli")) {
            return "redirect:/notAuthorized";
        }
        UserRole userRole = userRoleRepository.findOne(id);
        model.addAttribute("role", userRole);
        model.addAttribute("functionality", functionalityRepository.findAll());
        model.addAttribute("functionaltyObj", new Functionality());
        model.addAttribute("functionalityOfRole", userRole.getFunctionalities());

        return "user/roleUpdate";
    }

    /**
     *  disabilita un user
     * @param id user da disabilitare
     * @param model
     * @return pagina per gestione user
     */
    @RequestMapping(value = "/user/disable/{id}", method = RequestMethod.GET)
    public String disableUser(@PathVariable("id") long id, Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione utenti")) {
            return "redirect:/notAuthorized";
        }
        Users u = userRepository.findOne(id);
        u.setEnabled(false);
        userRepository.save(u);
        return "redirect:/user";
    }

    /**
     * abilita un user precedentemente disabilitato
     * @param id user da abilitare
     * @param model
     * @return  pagina per gestione user
     */
    @RequestMapping(value = "/user/enable/{id}", method = RequestMethod.GET)
    public String enableUser(@PathVariable("id") long id, Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione utenti")) {
            return "redirect:/notAuthorized";
        }
        Users u = userRepository.findOne(id);
        u.setEnabled(true);
        userRepository.save(u);
        return "redirect:/user";
    }

    /**
     * aggiunge funzionalità ad un user
     * @param id user da modificare
     * @param model
     * @param funcList lista di funzionalità del ruolo
     * @return pagina ruolo modificato
     */
    @RequestMapping(value = "/user/roles/update/{id}/addFunctionality", method = RequestMethod.POST)
    public String addFunctionalityToRole(@PathVariable("id") long id, Model model, @RequestParam("roles") String[] funcList) {
        List<Functionality> functionalities = new ArrayList<>();
        UserRole userRole = userRoleRepository.findOne(id);
        for (String s : funcList) {
            System.out.println(s);
            functionalities.add(functionalityRepository.findOne(Long.parseLong(s)));
            // System.out.println(userRole.isfuncPermitted(Long.parseLong(s))+" di "+functionalityRepository.findOne(Long.parseLong(s)).getFunctionality());
        }

        userRole.setFunctionalities(functionalities);
        UserRole u = userRoleRepository.save(userRole);
        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Ruolo", "MODIFICA", u.getId());
        ////////////////////////END LOG OPERATION///////////////////

        return "redirect:/user/roles/update/" + id;
    }

    /**
     * funzione che restituisce user che stiamo cercando
     * @param q stringa su qui stiamo effettuando la ricerca
     * @param model
     * @return lista di user che corrispondono alla ricerca
     */
    @RequestMapping(value = "/json/utenti/search", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<UsersJSON> usersJSON(@RequestParam(value = "q", required = true) String q, Model model) {
        List<UsersJSON> u = new ArrayList<>();
        for (Users us : userRepository.findByUsernameOrEmailOrNameOrLastnameContainingIgnoreCase(q, q, q, q)) {
            if (us.isEnabled()) {
                u.add(new UsersJSON(us.getId(), us.getName(), us.getLastname(), us.getUsername(), us.getEmail(), us.getRole().getRole(), us.isEnabled()));
            }
        }
        return u;
    }

    /**
     * metodo che aggiunge log al db
     * @param data
     * @param operation
     * @param id
     */
    public void writeLog(String data, String operation, long id) {
        LogOperation log = new LogOperation();
        log.setDate(new Date(System.currentTimeMillis()));
        log.setUser(SecurityContextHolder.getContext().getAuthentication().getName());
        log.setData(data);
        log.setOperation(operation);
        log.setIdData(id);
        logOperationRepository.save(log);
    }



    public Model createMenu(Model model) {
        // SECURITY
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        List<Functionality> functionalities = logged.getRole().getFunctionalities();
        List<String> func = new ArrayList<>();
        for (Functionality f : functionalities) {
            func.add(f.getFunctionality());
        }
        model.addAttribute("functionalites", func);
        return model;
        // END SECURITY
    }

}
