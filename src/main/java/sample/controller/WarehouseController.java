package sample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sample.model.*;
import sample.model.modelJSON.ItemJSON;
import sample.model.modelJSON.ProductJSON;
import sample.model.modelJSON.WarehouseJSON;
import sample.repo.*;
import sample.utility.Authorization;

import java.time.LocalDate;
import java.util.*;

/**
 * Controller per la gestione dei magazzini
 */
@Controller
public class WarehouseController {

    @Autowired
    protected WarehouseRepository warehouseRepository;
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected ItemRepository itemRepository;
    @Autowired
    protected ArticleRepository articleRepository;
    @Autowired
    protected ConstructioSiteRepository constructioSiteRepository;
    @Autowired
    protected LogOperationRepository logOperationRepository;
    @Autowired
    protected ProductWarehouseRepository productWarehouseRepository;
    @Autowired
    protected MovementRepository movementRepository;

    /**
     * lista di warehouse attivi/disattivi
     * @param model
     * @return
     */
    @RequestMapping(value = "/warehouse", method = RequestMethod.GET)
    public String warehouseManagement(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione magazzini")) {
            return "redirect:/notAuthorized";
        }



        if (logged.getRole().getRole().equals("gestore magazzino")  || logged.getRole().getRole().equals("magazziniere")) {

            model.addAttribute("listOfWarehouse", warehouseRepository.findByUsersAndIsActive(logged, true));
            model.addAttribute("listOfWarehouseClosed", warehouseRepository.findByUsersAndIsActive(logged, false));
        } else {
            model.addAttribute("listOfWarehouse", warehouseRepository.findByIsActive(true));
            model.addAttribute("listOfWarehouseClosed", warehouseRepository.findByIsActive(false));
        }


        return "warehouse/warehouse";
    }

    /**
     * Form per la registrazione di un warehouse
     * @param model
     * @return
     */
    @RequestMapping(value = "/warehouse/register", method = RequestMethod.GET)
    public String warehouseRegistration(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Registrazione magazzini")) {
            return "redirect:/notAuthorized";
        }

        model.addAttribute("warehouseObj", new Warehouse());
        model.addAttribute("userResponsable", userRepository.findAll());
        return "warehouse/warehouseRegistration";
    }

    /**
     * Salvataggio su db di un warehouse
     * @param model
     * @param warehouse
     * @param idUser
     * @param name
     * @param route
     * @param nap
     * @param nation
     * @return
     */
    @RequestMapping(value = "/warehouse/register", method = RequestMethod.POST)
    public String warehouseRegistration(Model model, @ModelAttribute("warehouse") Warehouse warehouse, @RequestParam("responsable") String idUser, @RequestParam("name") String name, @RequestParam("route") String route, @RequestParam("nap") String nap, @RequestParam("nation") String nation) {

        Users responsable = userRepository.findOne(Long.parseLong(idUser));
        warehouse.setUsers(responsable);
        warehouse.setName(name);
        warehouse.setNap(nap);
        warehouse.setNation(nation);
        warehouse.setRoute(route);
        warehouse.setActive(true);
        Warehouse u = warehouseRepository.save(warehouse);
        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Magazzino", "REGISTRAZIONE", u.getId());
        ////////////////////////END LOG OPERATION///////////////////

        return "redirect:/warehouse";
    }

    /**
     * visualizza l'invetario del warehouse @param id
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/warehouse/inventory/{id}", method = RequestMethod.GET)
    public String warehouseInventory(Model model, @PathVariable("id") long id) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione magazzini")) {
            return "redirect:/notAuthorized";
        }
        LinkedHashSet<Article> articles = new LinkedHashSet<>();

        for (Item item : itemRepository.findBylocation(warehouseRepository.findOne(id))) {
            articles.add(item.getArticle());
        }
        ArrayList<Article> articles1 = new ArrayList<>(articles);
        model.addAttribute("articles", articles1);
        model.addAttribute("warehouse", warehouseRepository.findOne(id));


        List<ProductJSON> items = new ArrayList<>();

        for (Article a : articles1) {
            if (a.isActive()) {
                List<Item> i = itemRepository.findItemByArticle(a);
                int quantity = 0;
                int alarm = 0;
                int warning = 0;
                String warehouse = "";
                boolean b = false;
                for (Item item : i) {
                    if (item.getLocation() instanceof Warehouse) {
                        List<Product_warehouse> pw = productWarehouseRepository.findByArticleAndWarehouse(a, item.getLocation());
                        quantity = itemRepository.countItemByArticleAndLocation(a, item.getLocation());
                        alarm = pw.get(0).getAlert();
                        warning = pw.get(0).getWarining();
                        warehouse = item.getLocation().getName();
                        b = true;
                    }
                }

                int label = -1;
                if (quantity <= warning) {
                    label = 1;
                } else if (quantity > warning && quantity <= alarm) {
                    label = 0;
                } else if (quantity > alarm) {
                    label = -1;
                }
                if (b) {
                    items.add(
                            new ProductJSON(
                                    a.getId(),
                                    a.getBrand(),
                                    a.isActive(),
                                    warehouse,
                                    a.getArticle_nr(),
                                    a.getArticleCategory().getCategory(),
                                    a.getName(),
                                    quantity,
                                    alarm,
                                    warning,
                                    a.isBoxItem(),
                                    label
                            )
                    );
                }

            }
        }

        model.addAttribute("items", items);

        return "warehouse/warehouseInventory";
    }

    /**
     * Form per modificare un warehouse
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/warehouse/update/{id}", method = RequestMethod.GET)
    public String warehouseUpdate(@PathVariable("id") long id, Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione magazzini")) {
            return "redirect:/notAuthorized";
        }
        model.addAttribute("warehouseObj", warehouseRepository.findOne(id));
        model.addAttribute("userResponsable", userRepository.findAll());
        return "warehouse/warehouseUpdate";
    }

    /**
     * Savataggio di un warehouse modificato
     * @param id
     * @param idUser
     * @param warehouse
     * @param model
     * @return
     */
    @RequestMapping(value = "/warehouse/update/{id}", method = RequestMethod.POST)
    public String warehouseUpdate(@PathVariable("id") long id, @RequestParam("responsable") String idUser, @ModelAttribute Warehouse warehouse, Model model) {

        Warehouse u = warehouseRepository.findOne(id);
        Users user = userRepository.findOne(Long.parseLong(idUser));
        u.setUsers(user);
        u.setRoute(warehouse.getRoute());
        u.setNation(warehouse.getNation());
        u.setNap(warehouse.getNap());
        u.setName(warehouse.getName());
        Warehouse uu = warehouseRepository.save(u);
        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Magazzino", "MODIFICA", uu.getId());
        ////////////////////////END LOG OPERATION///////////////////

        return "redirect:/warehouse";
    }

    /**
     * Disattivazione di un warehouse
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/warehouse/delete/{id}", method = RequestMethod.GET)
    public String warehouseDelete(@PathVariable("id") long id, Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione magazzini")) {
            return "redirect:/notAuthorized";
        }
        Warehouse warehouse = warehouseRepository.findOne(id);

        warehouse.setActive(false);
        Warehouse u = warehouseRepository.save(warehouse);

        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Magazzino", "ELIMINA", u.getId());
        ////////////////////////END LOG OPERATION///////////////////
        return "redirect:/warehouse";
    }

    /**
     * Riattivazione di un warehouse
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/warehouse/open/{id}", method = RequestMethod.GET)
    public String warehouseActive(@PathVariable("id") long id, Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione magazzini")) {
            return "redirect:/notAuthorized";
        }
        Warehouse warehouse = warehouseRepository.findOne(id);
        warehouse.setActive(true);
        warehouseRepository.save(warehouse);
        return "redirect:/warehouse";
    }

    /**
     * Selezionare gli articoli che si vogliono spostare
     * @param articles
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/warehouse/warehouseToConstructionSite/{id}", method = RequestMethod.GET)
    public String warehouseToConstruction(@ModelAttribute("articles") ArrayList<Article> articles, @PathVariable("id") long id, Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione magazzini")) {
            return "redirect:/notAuthorized";
        }
        for (Article article : articles) {
            model.addAttribute("items_" + article.getId(), itemRepository.findByArticleAndLocation(article, warehouseRepository.findOne(id)));
        }
        model.addAttribute("warehouse", warehouseRepository.findOne(id));
        model.addAttribute("constructionSite", constructioSiteRepository.findAll());
        model.addAttribute("articles", articles);
        return "/warehouse/warehouseToConstructionSite";
    }

    /**
     *
     * @param redir
     * @param articles
     * @param warehouseId
     * @return
     */
    @RequestMapping(value = "/warehouse/warehouseToConstructionSite/{id}", method = RequestMethod.POST)
    public ModelAndView warehouseInventory(RedirectAttributes redir, @RequestParam("checked") String[] articles, @PathVariable("id") long warehouseId) {
        if (articles == null) {
            ModelAndView tmp = new ModelAndView();
            tmp.setViewName("redirect:/warehouse/warehouseToConstructionSite/" + warehouseId);
            return tmp;
        }
        ModelAndView modelAndView = new ModelAndView();
        ArrayList<Article> articleArrayList = new ArrayList<>();
        for (String s : articles) {
            Article a = articleRepository.findOne(Long.parseLong(s));
            a.setCheckNotification(0);
            articleRepository.save(a);
            articleArrayList.add(a);
        }
        modelAndView.setViewName("redirect:/warehouse/warehouseToConstructionSite/" + warehouseId);
        redir.addFlashAttribute("articles", articleArrayList);

        return modelAndView;
    }

    /**
     * Form per cercare gli articoli da inserire in magazzino
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/warehouse/addproduct/{id}", method = RequestMethod.GET)
    public String registerEntryToWarehouse(@PathVariable("id") long id, Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione magazzini")) {
            return "redirect:/notAuthorized";
        }

        model.addAttribute("idWarehouse", id);

        return "/warehouse/addProductToWarehouse";
    }

    /**
     * form per selezionare gli articoli da spostare
     * @param idWarehouse
     * @param items
     * @param constId
     * @param model
     * @return
     */
    @RequestMapping(value = "/warehouse/inventory/{id}", method = RequestMethod.POST)
    public String warehouseToConstruction(@PathVariable("id") String idWarehouse, @RequestParam("items") String[] items, @RequestParam("constrID") String constId, Model model) {
        Item item;
        Location l = constructioSiteRepository.findOne(Long.parseLong(constId));

        for (String i : items) {

            item = itemRepository.findOne(Long.parseLong(i));
            Location locationFrom = item.getLocation();
            item.setLocation(l);
            item.getArticle().setCheckNotification(0);
            itemRepository.save(item);
            // MOVEMENT
            Movement movement = new Movement();
            movement.setTo(l);
            movement.setDate(LocalDate.now());
            movement.setItem(item);
            movement.setFrom(locationFrom);
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            Users logged = userRepository.findUsersByUsername(username);
            movement.setUsers(logged);
            movementRepository.save(movement);

        }


        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Magazzino", "MOVIMENTO", l.getId());
        ////////////////////////END LOG OPERATION///////////////////
        return "redirect:/warehouse/inventory/" + idWarehouse;
    }

    /**
     * json rappresentante i warehouse cercati
     * @param q
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/warehouse/search", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<WarehouseJSON> warehouseJSON(@RequestParam(value = "q", required = true) String q, Model model) {
        List<WarehouseJSON> u = new ArrayList<>();
        for (Warehouse us : warehouseRepository.findByNameContainingIgnoreCase(q)) {
            if (us.isActive()) {
                u.add(new WarehouseJSON(us.getId(), us.getName(), us.getRoute() + ", " + us.getNap() + " " + us.getNation(), us.getUsers().getName() + " " + us.getUsers().getLastname() + " (" + us.getUsers().getUsername() + ")", us.isActive()));
            }
        }
        return u;
    }

    /**
     * json rappresentante gli articoli da spostare
     * @param idArticle
     * @return
     */
    @RequestMapping(value = "/json/article/{idArticle}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    ProductJSON productJSON(@PathVariable("idArticle") long idArticle) {
        Article a = articleRepository.findOne(idArticle);
        ProductJSON productJSON = new ProductJSON(a.getId(), a.getBrand(), a.isActive(), "", a.getArticle_nr(), a.getArticleCategory().getCategory(), a.getName(), 0, 0, 0, false, 0);
        return productJSON;
    }

    /**
     *
     * @param idArticle
     * @param idWarehouse
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/warehouse/itemListforArticle/{idWarehouse}/{idArticle}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<ItemJSON> item(@PathVariable("idArticle") long idArticle, @PathVariable("idWarehouse") long idWarehouse, Model model) {
        model = createMenu(model);
        List<ItemJSON> itemJSONList = new ArrayList<>();
        for (Item item : itemRepository.findByArticleAndLocation(articleRepository.findOne(idArticle), warehouseRepository.findOne(idWarehouse))) {
            itemJSONList.add(new ItemJSON(item.getId(), item.getItemCod(), item.getQuantity()));
        }

        return itemJSONList;

    }

    /**
     * Scrive il log delle operazioni su db
     *
     * @param data
     * @param operation
     * @param id
     */
    public void writeLog(String data, String operation, long id) {
        LogOperation log = new LogOperation();
        log.setDate(new Date(System.currentTimeMillis()));
        log.setUser(SecurityContextHolder.getContext().getAuthentication().getName());
        log.setData(data);
        log.setOperation(operation);
        log.setIdData(id);
        logOperationRepository.save(log);
    }

    /**
     * Metodo che gestisce la creazione del menu
     * la lista contiene tutte le funzionalità permesse dall'utente loggato
     * controllo se mostrare o no lato html
     *
     * @param model
     * @return
     */
    public Model createMenu(Model model) {

        // SECURITY
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        List<Functionality> functionalities = logged.getRole().getFunctionalities();
        List<String> func = new ArrayList<>();
        for (Functionality f : functionalities) {
            func.add(f.getFunctionality());
        }
        model.addAttribute("functionalites", func);
        return model;
        // END SECURITY
    }


}
