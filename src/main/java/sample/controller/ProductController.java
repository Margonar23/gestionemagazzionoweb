package sample.controller;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.qrcode.QRCodeWriter;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sample.model.*;
import sample.model.modelJSON.*;
import sample.repo.*;
import sample.utility.Authorization;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;

/**
 * Controller per la completa gestione dei prodotti
 */
@Controller
public class ProductController {
    @Autowired
    protected ArticleCategoriesRepository articleCategoriesRepository;
    @Autowired
    protected ArticleFeaturesRepository articleFeaturesRepository;
    @Autowired
    protected ArticleDetailRepository articleDetailRepository;
    @Autowired
    protected ArticleRepository articleRepository;
    @Autowired
    protected ProductWarehouseRepository productWarehouseRepository;
    @Autowired
    protected WarehouseRepository warehouseRepository;
    @Autowired
    protected ItemRepository itemRepository;
    @Autowired
    protected LogOperationRepository logOperationRepository;

    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected MovementRepository movementRepository;
    @Autowired
    LocationRepository locationRepository;

    /**
     * pagina principale
     * mostra la lista degli articoli
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public String greetingRoot(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione prodotti")) {
            return "redirect:/notAuthorized";
        }

        model.addAttribute("categories", articleCategoriesRepository.findAll());
        model.addAttribute("products", articleRepository.findByIsActive(true));
        model.addAttribute("productsDisable", articleRepository.findByIsActive(false));

        List<ProductJSON> items = new ArrayList<>();

        for (Article a : articleRepository.findAll()) {
            if (a.isActive()) {
                List<Item> i = itemRepository.findItemByArticle(a);
                int quantity = 0;
                int alarm = 0;
                int warning = 0;
                String warehouse = "";
                boolean b = false;
                for (Item item : i) {
                    if (item.getLocation() instanceof Warehouse) {
                        List<Product_warehouse> pw = productWarehouseRepository.findByArticleAndWarehouse(a, item.getLocation());
                        quantity = itemRepository.countItemByArticleAndLocation(a, item.getLocation());
                        alarm = pw.get(0).getAlert();
                        warning = pw.get(0).getWarining();
                        warehouse = item.getLocation().getName();
                        b = true;
                    }
                }

                int label = -1;
                if (quantity <= warning) {
                    label = 1;
                } else if (quantity > warning && quantity <= alarm) {
                    label = 0;
                } else if (quantity > alarm) {
                    label = -1;
                }
                if (b) {
                    items.add(
                            new ProductJSON(
                                    a.getId(),
                                    a.getBrand(),
                                    a.isActive(),
                                    warehouse,
                                    a.getArticle_nr(),
                                    a.getArticleCategory().getCategory(),
                                    a.getName(),
                                    quantity,
                                    alarm,
                                    warning,
                                    a.isBoxItem(),
                                    label
                            )
                    );
                }

            }
        }


        model.addAttribute("items", items);


        return "products/products";
    }

    /**
     * Form di registrazione di un articolo
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/product/register", method = RequestMethod.GET)
    public String productsRegistration(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione prodotti")) {
            return "redirect:/notAuthorized";
        }
        model.addAttribute("newArticle", new Article());
        model.addAttribute("categoryList", articleCategoriesRepository.findAll());

        return "products/productRegistration";
    }

    /**
     * Salvataggio su db dell articolo
     *
     * @param model
     * @param attributevalues
     * @param article
     * @param idCategory
     * @return
     */
    @RequestMapping(value = "/product/register", method = RequestMethod.POST)
    public String productsRegistration(Model model, @RequestParam("attributeValue") String[] attributevalues, @ModelAttribute Article article, @RequestParam("category") long idCategory) {
        article.setArticleCategory(articleCategoriesRepository.findOne(idCategory));

        articleRepository.save(article);


        int i = 0;
        for (ArticleFeature f : article.getArticleCategory().getFeatures()) {
            ArticleDetail detail = new ArticleDetail();
            detail.setFeatures(f);
            detail.setAttributeValue(attributevalues[i]);
            detail.setArticle(article);
            article.getDetails().add(detail);
            articleDetailRepository.save(detail);
            i++;
        }
        article.setActive(true);
        Article u = articleRepository.save(article);

        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Articolo", "REGISTRAZIONE", u.getId());
        ////////////////////////END LOG OPERATION///////////////////

        return "redirect:/products";
    }

    /**
     * elimina un articolo
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/product/delete/{id}", method = RequestMethod.GET)
    public String productsDelete(Model model, @PathVariable("id") long id) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione prodotti")) {
            return "redirect:/notAuthorized";
        }
        Article article = articleRepository.findOne(id);
        article.setActive(false);
        articleRepository.save(article);

        return "redirect:/products";
    }

    /**
     * Attiva un articolo
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/product/enable/{id}", method = RequestMethod.GET)
    public String productEnable(Model model, @PathVariable("id") long id) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione prodotti")) {
            return "redirect:/notAuthorized";
        }
        Article article = articleRepository.findOne(id);
        article.setActive(true);
        articleRepository.save(article);

        return "redirect:/products";
    }

    /**
     * form di modifica di un articolo
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/product/update/{id}", method = RequestMethod.GET)
    public String productsUpdate(Model model, @PathVariable("id") long id) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione prodotti")) {
            return "redirect:/notAuthorized";
        }
        model.addAttribute("article", articleRepository.findOne(id));
        model.addAttribute("categoryList", articleCategoriesRepository.findAll());

        return "products/productUpdate";
    }

    /**
     * salvataggio su db del articolo modificato
     *
     * @param model
     * @param id
     * @param attributeValues
     * @param idCategory
     * @param article
     * @return
     */
    @RequestMapping(value = "/product/update/{id}", method = RequestMethod.POST)
    public String productsUpdate(Model model, @PathVariable("id") long id, @RequestParam("attributeValues") String[] attributeValues, @RequestParam("category") String idCategory, @ModelAttribute Article article) {


        article = articleRepository.findOne(article.getId());
        if (article.getArticleCategory().getId() != Long.parseLong(idCategory)) {
            articleDetailRepository.delete(article.getDetails());

            ArticleCategory c = articleCategoriesRepository.findOne(Long.parseLong(idCategory));
            article.setArticleCategory(c);

            ArrayList<ArticleDetail> articleDetails = new ArrayList<>();
            for (int i = 0; i < c.getFeatures().size(); i++) {
                ArticleDetail articleDetail = new ArticleDetail();
                articleDetail.setAttributeValue(attributeValues[i]);
                articleDetail.setFeature(c.getFeatures().get(i));
                articleDetail.setArticle(article);
                articleDetails.add(articleDetail);

            }
            articleDetailRepository.save(articleDetails);
            article.setDetails(articleDetails);
        } else {
            for (int i = 0; i < article.getDetails().size(); i++) {
                article.getDetails().get(i).setAttributeValue(attributeValues[i]);
            }
        }
        Article u = articleRepository.save(article);

        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Articolo", "MODIFICA", u.getId());
        ////////////////////////END LOG OPERATION///////////////////
        return "redirect:/products";
    }

    /**
     * Visualizza le info sul articolo
     * Inventario e dettagli tecnici
     * Print di tutti i qrcode di tutti gli item di quel articolo
     * Print di un singolo qrcode di un item
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/product/information/{id}", method = RequestMethod.GET)
    public String productsInformation(Model model, @PathVariable("id") long id) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione prodotti")) {
            return "redirect:/notAuthorized";
        }
        Article a = articleRepository.findOne(id);
        List<Item> items = itemRepository.findItemByArticle(a);
        List<Product_warehouse> pw;

        model.addAttribute("article", a);
        ArrayList<ItemWarehouse> itemWarehouses = new ArrayList<>();
        ArrayList<ItemConstruction> itemConstructions = new ArrayList<>();
        HashMap<Location, ItemWarehouse> itemWarehouseHashMap = new HashMap<>();
        HashMap<Location, ItemConstruction> itemConstructionHashMap = new HashMap<>();

        for (Item i : items) {
            int quantity = itemRepository.countItemByArticleAndLocation(a, i.getLocation());

            if (i.getLocation() instanceof Warehouse) {

                pw = productWarehouseRepository.findByArticleAndWarehouse(a, i.getLocation());
                int alarm = -1;
                if (quantity <= pw.get(0).getWarining()) {
                    alarm = 1;
                } else if (quantity > pw.get(0).getWarining() && quantity <= pw.get(0).getAlert()) {
                    alarm = 0;
                } else if (quantity > pw.get(0).getAlert()) {
                    alarm = -1;
                }
                itemWarehouseHashMap.put(i.getLocation(), new ItemWarehouse(i.getArticle().getName(), i.getArticle().getBrand(), i.getArticle().getArticle_nr(), i.getLocation().getName(), quantity, alarm));
            } else if (i.getLocation() instanceof ConstructionSite) {
                itemConstructionHashMap.put(i.getLocation(), new ItemConstruction(i.getArticle().getName(), i.getArticle().getBrand(), i.getArticle().getArticle_nr(), i.getLocation().getName(), quantity));
            }

        }


        Iterator it = itemWarehouseHashMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            itemWarehouses.add((ItemWarehouse) pair.getValue());
            it.remove();
        }
        Iterator itCS = itemConstructionHashMap.entrySet().iterator();
        while (itCS.hasNext()) {
            Map.Entry pair = (Map.Entry) itCS.next();
            itemConstructions.add((ItemConstruction) pair.getValue());
            itCS.remove();
        }


        List<Item> itemList = new ArrayList<>();
        for (Item i : itemRepository.findItemByArticle(a)) {
            itemList.add(i);
        }


        model.addAttribute("itemList", itemList);
        model.addAttribute("items", itemWarehouses);
        model.addAttribute("itemConstructions", itemConstructions);
        return "products/productInformation";
    }

    /**
     * pagina che permette la scansione di qrcode
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/product/barcode", method = RequestMethod.GET)
    public String productsBarcode(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione prodotti")) {
            return "redirect:/notAuthorized";
        }
        List<Location> locations = new ArrayList<>();
        for (Location location : locationRepository.findAll()) {
            if (!(location instanceof Company)) {
                locations.add(location);
            }
        }

        model.addAttribute("to", locations);

        return "products/productBarcode";
    }

    /**
     * form per la registrazione di una categoria
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/product/category/register", method = RequestMethod.GET)
    public String categoryRegistration(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione categorie prodotti")) {
            return "redirect:/notAuthorized";
        }

        model.addAttribute("articleCategory", new ArticleCategory());


        return "products/categoryRegistration";
    }

    /**
     * salvataggio della categoria su db
     *
     * @param model
     * @param articleCategories
     * @return
     */
    @RequestMapping(value = "/product/category/register", method = RequestMethod.POST)
    public String categoryRegistration(Model model, @ModelAttribute ArticleCategory articleCategories) {

        ArticleCategory u = articleCategoriesRepository.save(articleCategories);
        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Categoria", "REGISTRAZIONE", u.getId());
        ////////////////////////END LOG OPERATION///////////////////

        return "redirect:/product/category";
    }

    /**
     * salvataggio su db features della categoria @param id
     *
     * @param model
     * @param id
     * @param feature
     * @return
     */
    @RequestMapping(value = "/product/category/configuration/{id}", method = RequestMethod.POST)
    public String categoryConfig(Model model, @PathVariable("id") long id, @ModelAttribute ArticleFeature feature) {
        ArticleCategory a = articleCategoriesRepository.findOne(id);
        ArticleFeature f = new ArticleFeature();
        f.setAttributeName(feature.getAttributeName());
        a.getFeatures().add(f);
        articleCategoriesRepository.save(a);
        f.setArticleCategories(a);
        ArticleFeature u = articleFeaturesRepository.save(f);
        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Categoria", "MODIFICA", u.getId());
        ////////////////////////END LOG OPERATION///////////////////
        return "redirect:/product/category/configuration/" + a.getId();
    }

    /**
     * form per aggiungere features alla categoria @param id
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/product/category/configuration/{id}", method = RequestMethod.GET)
    public String categoryConfig(Model model, @PathVariable("id") long id) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione categorie prodotti")) {
            return "redirect:/notAuthorized";
        }
        model.addAttribute("newArticleFeatures", new ArticleFeature());
        model.addAttribute("category", articleCategoriesRepository.findOne(id));
        model.addAttribute("features", articleFeaturesRepository.findByArticleCategory(articleCategoriesRepository.findOne(id)));
        return "products/categoryConfiguration";
    }

    /**
     * visualizza la lista delle categoria
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/product/category", method = RequestMethod.GET)
    public String category(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione categorie prodotti")) {
            return "redirect:/notAuthorized";
        }
        model.addAttribute("categoriesList", articleCategoriesRepository.findAll());
        return "products/category";
    }

    /**
     * json rappresentante le features della categoria @param id
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/product/register/{id}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<ArticleDetailJSON> featuresOfCategory(@PathVariable("id") long id, Model model) {
        ArticleCategory c = articleCategoriesRepository.findOne(id);
        List<ArticleDetailJSON> articleDetailList = new ArrayList<>();
        for (ArticleFeature af : c.getFeatures()) {
            articleDetailList.add(new ArticleDetailJSON(af.getId(), af.getAttributeName()));
        }

        return articleDetailList;
    }

    /**
     * form per l'update di un articolo
     *
     * @param idArticle
     * @param idCategory
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/product/update/{idArticle}/{idCategory}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<ArticleDetailJSON> singleCompanyUpdate(@PathVariable("idArticle") long idArticle, @PathVariable("idCategory") long idCategory, Model model) {
        List<ArticleDetailJSON> articleDetailList = new ArrayList<>();
        Article a = articleRepository.findOne(idArticle);

        if (a.getArticleCategory().getId() == idCategory) {
            for (int i = 0; i < a.getDetails().size(); i++) {
                articleDetailList.add(new ArticleDetailJSON(a.getDetails().get(i).getFeature().getId(), a.getDetails().get(i).getId(), a.getDetails().get(i).getFeature().getAttributeName(), a.getDetails().get(i).getAttributeValue()));
            }
        } else {
            ArticleCategory c = articleCategoriesRepository.findOne(idCategory);
            for (ArticleFeature af : c.getFeatures()) {
                articleDetailList.add(new ArticleDetailJSON(af.getId(), -1, af.getAttributeName(), ""));
            }
        }
        return articleDetailList;
    }

    /**
     * Ricerca di un prodotto tramite js ajax request
     *
     * @param q
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/product/search", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<ProductJSON> usersJSON(@RequestParam(value = "q", required = true) String q, Model model) {
        List<ProductJSON> u = new ArrayList<>();
        for (Article us : articleRepository.findByNameOrBrandContainingIgnoreCase(q, q)) {
            if (us.isActive()) {
                u.add(new ProductJSON(us.getId(), us.getBrand(), us.isActive(), "", us.getArticle_nr(), us.getArticleCategory().getCategory(), us.getName(), 0, 0, 0, false, 0));
            }
        }
        return u;
    }

    /**
     * ricerca di un articolo all'interno dell'inventario di un magazzino
     *
     * @param idWarehouse
     * @param q
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/product/register/{idWarehouse}/search", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<ProductJSON> warehouseJSON(@PathVariable("idWarehouse") long idWarehouse, @RequestParam(value = "q", required = true) String q, Model model) {
        List<ProductJSON> u = new ArrayList<>();
        for (Article article : articleRepository.findByNameOrBrandContainingIgnoreCase(q, q)) {
            if (article.isActive()) {
                u.add(new ProductJSON(article.getId(), article.getBrand(), article.isActive(), "", article.getArticle_nr(), article.getArticleCategory().getCategory(), article.getName(), 0, 0, 0, article.isBoxItem(), 0));
            }
        }
        return u;
    }

    /**
     * quando inserisci dei nuovi item in un magazzino
     * completa automaticamente i campi di allarme e preallarme guardando se ci sono gia i dati su
     * questo e questo magazzino
     *
     * @param id
     * @param idWarehouse
     * @param model
     * @return
     */
    @RequestMapping(value = "/json/product/register/choose/{idWarehouse}/{id}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    ProductJSON singleProductRegister(@PathVariable("id") long id, @PathVariable("idWarehouse") long idWarehouse, Model model) {

        Warehouse warehouse = warehouseRepository.findOne(idWarehouse);
        Article article = articleRepository.findOne(id);
        List<Product_warehouse> pw = productWarehouseRepository.findByArticleAndWarehouse(article, warehouse);

        if (pw.isEmpty()) {
            return new ProductJSON(article.getId(), article.getBrand(), article.isActive(), "", article.getArticle_nr(), article.getArticleCategory().getCategory(), article.getName(), 0, 0, 0, article.isBoxItem(), 0);
        } else {
            return new ProductJSON(article.getId(), article.getBrand(), article.isActive(), "", article.getArticle_nr(), article.getArticleCategory().getCategory(), article.getName(), 0, pw.get(0).getAlert(), pw.get(0).getWarining(), article.isBoxItem(), 0);
        }

    }

    /**
     * metodo che registra vari item nel magazino, crea e salva i qrcode e reindirizza nella pagina per la stampa
     * parametri necessari sono l'id del magazzino dove inserirli, l'id degli articoli e la quantità di essi.
     * registra inoltre le soglie di allarme e preallarme mettendo in relazione articolo e magazzino
     *
     * @param redir
     * @param idWarehouse
     * @param model
     * @param id
     * @param quantity
     * @param pezzi
     * @param warning
     * @param alarm
     * @return
     */
    @RequestMapping(value = "/product/register/item/{idLocation}", method = RequestMethod.POST)
    public ModelAndView addItem(RedirectAttributes redir, @PathVariable("idLocation") long idWarehouse, Model model, @RequestParam("idArticle") String[] id, @RequestParam("qta") String[] quantity, @RequestParam("pezzi") String[] pezzi, @RequestParam("warning") String[] warning, @RequestParam("alarm") String[] alarm) {
        ModelAndView modelAndView = new ModelAndView();
        Warehouse warehouse = warehouseRepository.findOne(idWarehouse);
        List<Item> itemList = new ArrayList<>();
        for (int i = 0; i < id.length; i++) {
            Article article = articleRepository.findOne(Long.parseLong(id[i]));

            for (int j = 0; j < Integer.parseInt(quantity[i]); j++) {
                Item item = new Item();
                item.setQuantity(Integer.parseInt(pezzi[i]));
                item.setArticle(article);

                item.setLocation(warehouse);
                DateTimeFormatter format = DateTimeFormatter.ofPattern("ddMMyy");
                item.setDate(LocalDate.now());

                String username = SecurityContextHolder.getContext().getAuthentication().getName();
                item = itemRepository.save(item);
                itemList.add(item);
                String itemCod = article.getArticle_nr() + "-" + item.getDate().format(format) + "-" + username + "-" + item.getId();
                item.setItemCod(itemCod);

                // MOVEMENT
                Movement movement = new Movement();
                movement.setTo(warehouse);
                movement.setDate(LocalDate.now());
                movement.setItem(item);
                Users logged = userRepository.findUsersByUsername(username);
                movement.setUsers(logged);
                movementRepository.save(movement);
            }

            List<Product_warehouse> pw = productWarehouseRepository.findByArticleAndWarehouse(article, warehouse);
            if (pw.size() != 0) {
                pw.get(0).setArticle(article);
                pw.get(0).setWarehouse(warehouse);
                pw.get(0).setAlert(Integer.parseInt(alarm[i]));
                pw.get(0).setWarining(Integer.parseInt(warning[i]));
                productWarehouseRepository.save(pw.get(0));
            } else {
                Product_warehouse product_warehouse = new Product_warehouse();
                product_warehouse.setArticle(article);
                product_warehouse.setWarehouse(warehouse);
                product_warehouse.setAlert(Integer.parseInt(alarm[i]));
                product_warehouse.setWarining(Integer.parseInt(warning[i]));
                productWarehouseRepository.save(product_warehouse);
            }


        }

        //////////////////////LOG OPERATION/////////////////////////
        writeLog("Item", "REGISTRAZIONE", warehouse.getId());
        ////////////////////////END LOG OPERATION///////////////////

        /**
         * Creazione dei qrcode per ogni item
         */
        for (Item item : itemList) {
            int width = 400;
            int height = 200;

            String imageFormat = "png";

            Path currentRelativePath = Paths.get("");
            String s = currentRelativePath.toAbsolutePath().toString();
            String path = s + "/src/main/resources/static/resources/barcode/";
            redir.addFlashAttribute("path", path);
            try {
                /**
                 * Cambiare il BarcodeFormat.QR_CODE in BarcodeFormat.CODE_128 per genereare barcode
                 */
                BitMatrix bitMatrix = new QRCodeWriter().encode(item.getItemCod(), BarcodeFormat.QR_CODE, width, height);
                String finalPath = path + item.getItemCod() + ".png";
                item.setPath(finalPath);
                FileOutputStream fileOutputStream = new FileOutputStream(new File(path + item.getItemCod() + ".png"));
                MatrixToImageWriter.writeToStream(bitMatrix, imageFormat, fileOutputStream);
                /**
                 * Aggiunta del testo sotto il qrcode per visibilità umana
                 */
                BufferedImage image = ImageIO.read(new File(path + item.getItemCod() + ".png"));
                BufferedImage img = new BufferedImage(image.getWidth(), image.getHeight() + 30, BufferedImage.TYPE_INT_ARGB);
                Graphics2D g2d = img.createGraphics();
                g2d.setBackground(Color.white);
                g2d.drawImage(image, 0, 0, null);

                g2d.setFont(new Font("Arial", Font.PLAIN, 20));
                String text = item.getItemCod();
                FontMetrics fm = g2d.getFontMetrics();
                int x = img.getWidth() - fm.stringWidth(text) - (img.getHeight() / 2) + 30;
                int y = img.getHeight() - 6;
                g2d.setColor(Color.black);
                g2d.drawString(text, x, y);
                g2d.dispose();
                File outputfile = new File(path + item.getItemCod() + ".png");
                ImageIO.write(img, "png", outputfile);


            } catch (WriterException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        itemRepository.save(itemList);
        redir.addFlashAttribute("items", itemList);
        modelAndView.setViewName("redirect:/product/barcodePrint");

        return modelAndView;
    }

    /**
     * permette la stampa di tutti i qrcode di tutti gli item del articolo @param idA
     *
     * @param redir
     * @param model
     * @param idA
     * @return
     */
    @RequestMapping(value = "/product/print/article/{idArticle}", method = RequestMethod.GET)
    public ModelAndView printArticle(RedirectAttributes redir, Model model, @PathVariable("idArticle") long idA) {
        ModelAndView modelAndView = new ModelAndView();
        ArrayList<Item> itemList = (ArrayList<Item>) itemRepository.findItemByArticle(articleRepository.findOne(idA));
        redir.addFlashAttribute("items", itemList);
        modelAndView.setViewName("redirect:/product/barcodePrint");
        return modelAndView;
    }

    /**
     * permette la stampa del qrcode del singolo item
     *
     * @param redir
     * @param model
     * @param idI
     * @return
     */
    @RequestMapping(value = "/product/print/item/{idItem}", method = RequestMethod.GET)
    public ModelAndView printItem(RedirectAttributes redir, Model model, @PathVariable("idItem") long idI) {
        ModelAndView modelAndView = new ModelAndView();
        Item i = itemRepository.findOne(idI);
        ArrayList<Item> itemList = new ArrayList<>();
        itemList.add(i);
        redir.addFlashAttribute("items", itemList);
        modelAndView.setViewName("redirect:/product/barcodePrint");
        return modelAndView;
    }

    /**
     * Richiedo al server l'immagine del qrcode
     * utile per le immagini appena caricate sul server
     *
     * @param image
     * @param model
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/resources/image/{image}", method = RequestMethod.GET)
    public void image(@PathVariable("image") String image, Model model, HttpServletResponse response) throws IOException {

        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        String path = s + "/src/main/resources/static/resources/barcode/" + image + ".png";
        File file = new File(path);
        if (file.exists()) {
            FileInputStream inputStream = new FileInputStream(path);
            response.getOutputStream().write(IOUtils.toByteArray(inputStream));
        } else {
            File file2 = new File("/src/main/resources/static/resources/icon/barcode.png");
            FileInputStream inputStream = new FileInputStream(file2);
            response.getOutputStream().write(IOUtils.toByteArray(inputStream));
        }
//        IOUtils.copy(inputStream, response.getOutputStream());
        response.setContentType(MediaType.IMAGE_PNG_VALUE); // o il tipo delle vostre immagini
        response.flushBuffer();
    }

    /**
     * pagina raffigurante i qrCode
     *
     * @param model
     * @param items
     * @return
     */
    @RequestMapping(value = "/product/barcodePrint", method = RequestMethod.GET)
    public String printBarcode(Model model, @ModelAttribute("items") List<Item> items) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Gestione prodotti")) {
            return "redirect:/notAuthorized";
        }

        model.addAttribute("items", items);
        return "warehouse/barcodePrint";
    }


    /**
     * Spostare un prodotto tramite qrcode
     *
     * @param itemBarcode
     * @return
     */
    @RequestMapping(value = "/movement/{item_barcode}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<Movement> getMovementList(@PathVariable("item_barcode") String itemBarcode) {
        String[] parts = itemBarcode.split("-");
        long iditem = Long.parseLong(parts[3]);
        Item i = itemRepository.findOne(iditem);
        List<Movement> movementList = movementRepository.findByItem(i);
        return movementList;

    }

    /**
     * Json rappresentante le informazioni dell'item scansionato
     *
     * @param itemCode
     * @return
     */
    @RequestMapping(value = "/json/product/findItem/{itemCode}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    ItemJSON getItemFromCode(@PathVariable("itemCode") String itemCode) {
        Item i = itemRepository.findItemByItemCod(itemCode);
        if (i == null) {
            return new ItemJSON(0, "error", 0);
        } else {
            return new ItemJSON(i.getId(), i.getItemCod(), i.getQuantity(), i.getArticle().getBrand(), i.getArticle().getName(), i.getArticle().getArticle_nr());
        }
    }

    /**
     * json rappresentante la lista degli spostamenti di un singolo item
     *
     * @param itemCode
     * @return
     */
    @RequestMapping(value = "/json/product/findItemMovement/{itemCode}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<MovementJSON> getItemMovementFromCode(@PathVariable("itemCode") String itemCode) {
        Item i = itemRepository.findItemByItemCod(itemCode);
        List<MovementJSON> movements = new ArrayList<>();
        if (i == null) {
            movements.add(new MovementJSON(0, LocalDate.now(), "", "", 1l, "error"));
        } else {
            for (Movement movement : movementRepository.findByItem(i)) {
                String from = "";
                if (movement.getFrom() == null) {
                    from = "Fornitore";
                } else {
                    from = movement.getFrom().getName();
                }
                movements.add(
                        new MovementJSON(
                                movement.getId(),
                                movement.getDate(),
                                from,
                                movement.getTo().getName(),
                                movement.getItem().getId(),
                                movement.getUsers().getName() + " " + movement.getUsers().getLastname()
                        )
                );
            }
        }
        return movements;

    }

    /**
     * Sposta un item scansionato in un altra location
     *
     * @param model
     * @param redir
     * @param article
     * @param toLocation
     * @param itemCode
     * @return
     */
    @RequestMapping(value = "/product/move", method = RequestMethod.POST)
    public ModelAndView moveItemTo(Model model, RedirectAttributes redir, @ModelAttribute Article article, @RequestParam("toLocation") String toLocation, @RequestParam("itemCode") String itemCode) {
        Item i = itemRepository.findItemByItemCod(itemCode);

        ModelAndView tmp = new ModelAndView();
        if (i == null) {
            tmp.setViewName("redirect:/product/barcode");
            redir.addFlashAttribute("error", "Item not found");
            return tmp;

        }
        Location locationFrom = i.getLocation();
        Location locationTo = locationRepository.findOne(Long.parseLong(toLocation));
        i.setLocation(locationTo);

        Item item = itemRepository.save(i);


        if (productWarehouseRepository.findByArticleAndWarehouse(i.getArticle(), locationTo).size() == 0) {
            if (locationTo instanceof Warehouse) {
                if (locationFrom instanceof Warehouse) {
                    List<Product_warehouse> pw = productWarehouseRepository.findByArticleAndWarehouse(i.getArticle(), locationFrom);
                    Product_warehouse product_warehouse = new Product_warehouse();
                    product_warehouse.setAlert(pw.get(0).getAlert());
                    product_warehouse.setWarining(pw.get(0).getWarining());
                    product_warehouse.setWarehouse((Warehouse) locationTo);
                    product_warehouse.setArticle(i.getArticle());
                    productWarehouseRepository.save(product_warehouse);
                } else {
                    List<Product_warehouse> pw = productWarehouseRepository.findByArticle(i.getArticle());
                    Product_warehouse product_warehouse = new Product_warehouse();
                    product_warehouse.setAlert(pw.get(0).getAlert());
                    product_warehouse.setWarining(pw.get(0).getWarining());
                    product_warehouse.setWarehouse((Warehouse) locationTo);
                    product_warehouse.setArticle(i.getArticle());
                    productWarehouseRepository.save(product_warehouse);
                }
            }
        }

        // MOVEMENT
        Movement movement = new Movement();
        movement.setTo(locationTo);
        movement.setDate(LocalDate.now());
        movement.setItem(item);
        movement.setFrom(locationFrom);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        movement.setUsers(logged);
        movementRepository.save(movement);
        tmp.setViewName("redirect:/product/barcode");
        return tmp;
    }

    /**
     * Scrive il log delle operazioni su db
     *
     * @param data
     * @param operation
     * @param id
     */
    public void writeLog(String data, String operation, long id) {
        LogOperation log = new LogOperation();
        log.setDate(new Date(System.currentTimeMillis()));
        log.setUser(SecurityContextHolder.getContext().getAuthentication().getName());
        log.setData(data);
        log.setOperation(operation);
        log.setIdData(id);
        logOperationRepository.save(log);
    }

    /**
     * Metodo che gestisce la creazione del menu
     * la lista contiene tutte le funzionalità permesse dall'utente loggato
     * controllo se mostrare o no lato html
     *
     * @param model
     * @return
     */
    public Model createMenu(Model model) {
        // SECURITY
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        List<Functionality> functionalities = logged.getRole().getFunctionalities();
        List<String> func = new ArrayList<>();
        for (Functionality f : functionalities) {
            func.add(f.getFunctionality());
        }
        model.addAttribute("functionalites", func);
        return model;
        // END SECURITY
    }


}
