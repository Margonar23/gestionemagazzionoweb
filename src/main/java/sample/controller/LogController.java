package sample.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import sample.model.Functionality;
import sample.model.LogOperation;
import sample.model.Users;
import sample.repo.LogOperationRepository;
import sample.repo.UserRepository;
import sample.utility.Authorization;

import java.util.ArrayList;
import java.util.List;


/**
 * Gestione dei log di sistema
 */
@Controller
public class LogController {

    @Autowired
    protected LogOperationRepository logOperationRepository;
    @Autowired
    protected UserRepository userRepository;

    /**
     * Visualizza la lista dei log
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/log", method = RequestMethod.GET)
    public String greetingLog(Model model) {
        model = createMenu(model);
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        if (!Authorization.isAuthorized(logged, "Visualizzazione log")) {
            return "redirect:/notAuthorized";
        }
        List<LogOperation> operationList = (ArrayList) logOperationRepository.findAllByOrderByIdDesc();
        model.addAttribute("log", operationList);
        return "log";
    }

    /**
     * Metodo che gestisce la creazione del menu
     * la lista contiene tutte le funzionalità permesse dall'utente loggato
     * controllo se mostrare o no lato html
     *
     * @param model
     * @return
     */
    public Model createMenu(Model model) {
        // SECURITY
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users logged = userRepository.findUsersByUsername(username);
        List<Functionality> functionalities = logged.getRole().getFunctionalities();
        List<String> func = new ArrayList<>();
        for (Functionality f : functionalities) {
            func.add(f.getFunctionality());
        }
        model.addAttribute("functionalites", func);
        return model;
        // END SECURITY
    }


   
}
