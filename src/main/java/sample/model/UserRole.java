package sample.model;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;



@Entity
public class UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
//prova
    private String role;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, mappedBy = "role")
    private List<Users> users = new ArrayList<>();

    @ManyToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private List<Functionality> functionalities = new ArrayList<>();


    public UserRole(String role, List<Users> users, List<Functionality> functionalities) {
        this.role = role;
        this.users = users;
        this.functionalities = functionalities;
    }

    public UserRole() {

    }

    @Override
    public String toString() {
        return role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Users> getUsers() {
        return users;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }

    public List<Functionality> getFunctionalities() {
        return functionalities;
    }

    public void setFunctionalities(List<Functionality> functionalities) {
        this.functionalities = functionalities;
    }


}

