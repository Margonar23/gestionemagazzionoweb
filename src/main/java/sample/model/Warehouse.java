package sample.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by vincenzo on 06/03/17.
 */
@Entity
public class Warehouse extends Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @ManyToOne
    private Users users;

    private boolean isActive;

    @OneToMany
    private List<Movement> movements;

    public Warehouse(Users users, List<Movement> movements) {
        this.users = users;
        this.movements = movements;
    }

    public Warehouse() {
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<Movement> getMovements() {
        return movements;
    }

    public void setMovements(List<Movement> movements) {
        this.movements = movements;
    }

    public long getLocationId(){
        return super.getId();
    }
    public Location getLocation(){
        return super.getObject();
    }

    @Override
    public String toString() {
        return "Warehouse{" +
                "id=" + id +
                ", users=" + users +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Warehouse)) return false;

        Warehouse warehouse = (Warehouse) o;

        if (getId() != warehouse.getId()) return false;
        if (isActive() != warehouse.isActive()) return false;
        return getUsers() != null ? getUsers().equals(warehouse.getUsers()) : warehouse.getUsers() == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getUsers() != null ? getUsers().hashCode() : 0);
        result = 31 * result + (isActive() ? 1 : 0);
        return result;
    }
}
