package sample.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by vincenzo on 06/03/17.
 */
@Entity
public class ConstructionSite extends Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @OneToOne
    private RespMunicipality respMunicipality;
    @ManyToOne
    private Company constructionFirm;
    @ManyToOne
    private Company flooringCompany;
    @ManyToOne
    private Users projectManager;
    @ManyToOne
    private Users responsible;
    @OneToMany private List<Movement> movementsC;

    private boolean isActive;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public RespMunicipality getRespMunicipality() {
        return respMunicipality;
    }

    public void setRespMunicipality(RespMunicipality respMunicipality) {
        this.respMunicipality = respMunicipality;
    }

    public Company getConstructionFirm() {
        return constructionFirm;
    }

    public void setConstructionFirm(Company constructionFirm) {
        this.constructionFirm = constructionFirm;
    }

    public Company getFlooringCompany() {
        return flooringCompany;
    }

    public void setFlooringCompany(Company flooringCompany) {
        this.flooringCompany = flooringCompany;
    }

    public Users getProjectManager() {
        return projectManager;
    }

    public void setProjectManager(Users projectManager) {
        this.projectManager = projectManager;
    }

    public Users getResponsible() {
        return responsible;
    }

    public void setResponsible(Users responsible) {
        this.responsible = responsible;
    }

    public List<Movement> getMovementsC() {
        return movementsC;
    }

    public void setMovementsC(List<Movement> movementsC) {
        this.movementsC = movementsC;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }


}
