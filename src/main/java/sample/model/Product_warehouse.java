package sample.model;

import javax.persistence.*;

/**
 * Created by Margonar on 27.03.2017.
 */
@Entity
public class Product_warehouse {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @ManyToOne
    private Article article;
    @ManyToOne
    private Warehouse warehouse;
    private int warining;
    private int alert;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public int getWarining() {
        return warining;
    }

    public void setWarining(int warining) {
        this.warining = warining;
    }

    public int getAlert() {
        return alert;
    }

    public void setAlert(int alert) {
        this.alert = alert;
    }
}
