package sample.model.modelJSON;

/**
 * Created by Margonar on 04.04.2017.
 */
public class ItemConstruction {
    private String brand;
    private String name;
    private String articleId;
    private String construction;
    private int quantity;

    public ItemConstruction(String name, String brand, String articleId, String construction, int quantity) {
        this.name = name;
        this.brand = brand;
        this.articleId = articleId;
        this.construction = construction;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getWarehouse() {
        return construction;
    }

    public void setWarehouse(String warehouse) {
        this.construction = warehouse;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
