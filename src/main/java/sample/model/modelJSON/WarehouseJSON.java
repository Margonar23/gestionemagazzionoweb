package sample.model.modelJSON;

/**
 * Created by Margonar on 14.03.2017.
 */
public class WarehouseJSON {
    private long id;
    private String name;
    private String address;
    private String user;
    private boolean isActive;

    public WarehouseJSON(long id, String name, String address, String user, boolean isActive) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.user = user;
        this.isActive = isActive;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
