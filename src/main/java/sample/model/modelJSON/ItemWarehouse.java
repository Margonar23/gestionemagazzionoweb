package sample.model.modelJSON;

/**
 * Created by Margonar on 04.04.2017.
 */
public class ItemWarehouse {
    private String brand;
    private String name;
    private String articleId;
    private String warehouse;
    private int quantity;
    private int alarm;

    public ItemWarehouse(String name,String brand, String articleId, String warehouse, int quantity, int alarm) {
        this.name = name;
        this.brand = brand;
        this.articleId = articleId;
        this.warehouse = warehouse;
        this.quantity = quantity;
        this.alarm = alarm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAlarm() {
        return alarm;
    }

    public void setAlarm(int alarm) {
        this.alarm = alarm;
    }
}
