package sample.model.modelJSON;

/**
 * Created by vincenzo on 19/03/17.
 */

public class ArticleDetailJSON {

        private long idFeature;
        private long idDetail;
        private String attributeName;
        private String attributeValue;

        public ArticleDetailJSON(long idFeature, String attributeName) {
                this.idFeature = idFeature;
                this.attributeName = attributeName;
        }

        public ArticleDetailJSON(long idFeature, long idDetail, String attributeName, String attributeValue) {
                this.idFeature = idFeature;
                this.idDetail = idDetail;
                this.attributeName = attributeName;
                this.attributeValue = attributeValue;
        }

        public long getIdDetail() {
                return idDetail;
        }

        public void setIdDetail(long idDetail) {
                this.idDetail = idDetail;
        }

        public long getIdFeature() {
                return idFeature;
        }

        public void setIdFeature(long idFeature) {
                this.idFeature = idFeature;
        }

        public String getAttributeName() {
                return attributeName;
        }

        public void setAttributeName(String attributeName) {
                this.attributeName = attributeName;
        }

        public String getAttributeValue() {
                return attributeValue;
        }

        public void setAttributeValue(String attributeValue) {
                this.attributeValue = attributeValue;
        }
}
