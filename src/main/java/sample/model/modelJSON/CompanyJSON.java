package sample.model.modelJSON;

/**
 * Created by Margonar on 15.03.2017.
 */
public class CompanyJSON {
    private long id;
    private String name;
    private String route;
    private String nap;
    private String nation;
    private String phone;
    private String fax;
    private String user;

    public CompanyJSON(long id, String name, String route, String nap, String nation, String phone, String fax, String user) {
        this.id = id;
        this.name = name;
        this.route = route;
        this.nap = nap;
        this.nation = nation;
        this.phone = phone;
        this.fax = fax;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getNap() {
        return nap;
    }

    public void setNap(String nap) {
        this.nap = nap;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
