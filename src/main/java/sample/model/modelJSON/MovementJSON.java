package sample.model.modelJSON;

import java.time.LocalDate;

/**
 * Created by Margonar on 01.05.2017.
 */
public class MovementJSON {
    private long id;
    private LocalDate date;
    private String fromLocation;
    private String toLocation;
    private long itemId;
    private String user;

    public MovementJSON(long id, LocalDate date, String fromLocation, String toLocation, long itemId, String user) {
        this.id = id;
        this.date = date;
        this.fromLocation = fromLocation;
        this.toLocation = toLocation;
        this.itemId = itemId;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "MovementJSON{" +
                "id=" + id +
                ", date=" + date +
                ", fromLocation='" + fromLocation + '\'' +
                ", toLocation='" + toLocation + '\'' +
                ", itemId=" + itemId +
                ", user='" + user + '\'' +
                '}';
    }
}
