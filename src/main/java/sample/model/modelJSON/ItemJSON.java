package sample.model.modelJSON;

import sample.model.Movement;

import java.util.List;

/**
 * Created by Margonar on 19.03.2017.
 */
public class ItemJSON {
    private long id;
    private String itemCod;
    private int quantity;
    private String brand;
    private String name;
    private String articleNr;

    public ItemJSON(long id, String itemCod, int quantity) {
        this.id = id;
        this.itemCod = itemCod;
        this.quantity = quantity;
    }

    public ItemJSON(long id, String itemCod, int quantity, String brand, String name, String articleNr) {
        this.id = id;
        this.itemCod = itemCod;
        this.quantity = quantity;
        this.brand = brand;
        this.name = name;
        this.articleNr = articleNr;

    }


    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArticleNr() {
        return articleNr;
    }

    public void setArticleNr(String articleNr) {
        this.articleNr = articleNr;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getItemCod() {
        return itemCod;
    }

    public void setItemCod(String itemCod) {
        this.itemCod = itemCod;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
