package sample.model.modelJSON;

/**
 * Created by Margonar on 11.04.2017.
 */
public class ConstructionSiteJSON {
    private long id;
    private String name;
    private String address;
    private long respMun;
    private String imprCostr;
    private String imprPav;
    private String projectManager;
    private String responsable;

    public ConstructionSiteJSON(long id, String name, String address, long respMun, String imprCostr, String imprPav, String projectManager, String responsable) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.respMun = respMun;
        this.imprCostr = imprCostr;
        this.imprPav = imprPav;
        this.projectManager = projectManager;
        this.responsable = responsable;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getRespMun() {
        return respMun;
    }

    public void setRespMun(long respMun) {
        this.respMun = respMun;
    }

    public String getImprCostr() {
        return imprCostr;
    }

    public void setImprCostr(String imprCostr) {
        this.imprCostr = imprCostr;
    }

    public String getImprPav() {
        return imprPav;
    }

    public void setImprPav(String imprPav) {
        this.imprPav = imprPav;
    }

    public String getProjectManager() {
        return projectManager;
    }

    public void setProjectManager(String projectManager) {
        this.projectManager = projectManager;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }
}
