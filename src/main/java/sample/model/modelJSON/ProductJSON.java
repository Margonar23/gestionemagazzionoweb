package sample.model.modelJSON;

/**
 * Created by Margonar on 27.03.2017.
 */
public class ProductJSON {
    private long id;
    private String brand;
    private boolean isActive;
    private String article_nr;
    private String name;
    private String category;
    private String location;
    private int quantity;
    private int alarm;
    private int warning;
    private boolean isBox;
    private int label;


    public ProductJSON(long id, String brand, boolean isActive, String location, String article_nr, String category, String name, int quantity, int alarm, int warning, boolean isBox, int label) {
        this.id = id;
        this.brand = brand;
        this.isActive = isActive;
        this.article_nr = article_nr;
        this.name = name;
        this.location = location;
        this.alarm = alarm;
        this.quantity = quantity;
        this.category = category;
        this.warning = warning;
        this.isBox = isBox;
        this.label = label;
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getArticle_nr() {
        return article_nr;
    }

    public void setArticle_nr(String article_nr) {
        this.article_nr = article_nr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAlarm() {
        return alarm;
    }

    public void setAlarm(int alarm) {
        this.alarm = alarm;
    }

    public int getWarning() {
        return warning;
    }

    public void setWarning(int warning) {
        this.warning = warning;
    }

    public boolean isBox() {
        return isBox;
    }

    public void setBox(boolean box) {
        isBox = box;
    }

    @Override
    public String toString() {
        return "ProductJSON{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", isActive=" + isActive +
                ", article_nr='" + article_nr + '\'' +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", location='" + location + '\'' +
                ", quantity=" + quantity +
                ", alarm=" + alarm +
                ", warning=" + warning +
                ", isBox=" + isBox +
                ", label=" + label +
                '}';
    }
}
