package sample.model;

import javax.persistence.*;

/**
 * Created by vincenzo on 15/03/17.
 */
@Entity
public class ArticleDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @ManyToOne
    private ArticleFeature feature;
    private String attributeValue;
    @ManyToOne
    Article article;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFeature(ArticleFeature feature) {
        this.feature = feature;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public ArticleFeature getFeature() {
        return feature;
    }

    public void setFeatures(ArticleFeature feature) {
        this.feature = feature;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }
}
