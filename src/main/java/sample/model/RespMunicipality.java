package sample.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by vincenzo on 06/03/17.
 */
@Entity
public class RespMunicipality {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long number;
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    private Date date;
    @OneToOne(mappedBy = "respMunicipality")
    private ConstructionSite constructionSite;
    private double credit;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ConstructionSite getConstructionSite() {
        return constructionSite;
    }

    public void setConstructionSite(ConstructionSite constructionSite) {
        this.constructionSite = constructionSite;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }
}
