package sample.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincenzo on 06/03/17.
 */
@Entity
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String brand;
    private boolean isActive;
    private String article_nr;
    private String name;
    private boolean boxItem;

    @ManyToOne
    ArticleCategory articleCategory;
    @OneToMany(mappedBy = "article")
    List<ArticleDetail> details = new ArrayList<>();

    private int checkNotification;

    public int getCheckNotification() {
        return checkNotification;
    }

    public void setCheckNotification(int checkNotification) {
        this.checkNotification = checkNotification;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }


    public List<ArticleDetail> getDetails() {
        return details;
    }

    public void setDetails(List<ArticleDetail> details) {
        this.details = details;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getArticle_nr() {
        return article_nr;
    }

    public void setArticle_nr(String article_nr) {
        this.article_nr = article_nr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArticleCategory getArticleCategory() {
        return articleCategory;
    }

    public void setArticleCategory(ArticleCategory articleCategory) {
        this.articleCategory = articleCategory;
    }

    public boolean isBoxItem() {
        return boxItem;
    }

    public void setBoxItem(boolean boxItem) {
        this.boxItem = boxItem;
    }
}
