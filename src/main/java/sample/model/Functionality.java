package sample.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincenzo on 06/03/17.
 */
@Entity
public class Functionality {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String functionality;
    @ManyToMany(mappedBy = "functionalities")
    private List<UserRole> roles = new ArrayList<>();

    public Functionality(String functionality, List<UserRole> roles) {
        this.functionality = functionality;
        this.roles = roles;
    }

    public Functionality() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFunctionality() {
        return functionality;
    }

    public void setFunctionality(String functionality) {
        this.functionality = functionality;
    }

    public List<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }
}
