package sample.model;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Authorities {

    @Id
    private String username;
    private String authority; // non modificare questi campi

    public Authorities(){

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}

