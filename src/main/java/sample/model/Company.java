package sample.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by vincenzo on 06/03/17.
 */
@Entity
public class Company extends Location{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, mappedBy="constructionFirm")
    private List<ConstructionSite> assignedConstructionFirm;
    @OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, mappedBy = "flooringCompany")
    private List<ConstructionSite> assignedFlooringCompany;
    @OneToOne
    private Person contact;

    private String fax;
    private String phone;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public List<ConstructionSite> getAssignedConstructionFirm() {
        return assignedConstructionFirm;
    }

    public void setAssignedConstructionFirm(List<ConstructionSite> assignedConstructionFirm) {
        this.assignedConstructionFirm = assignedConstructionFirm;
    }

    public List<ConstructionSite> getAssignedFlooringCompany() {
        return assignedFlooringCompany;
    }

    public void setAssignedFlooringCompany(List<ConstructionSite> assignedFlooringCompany) {
        this.assignedFlooringCompany = assignedFlooringCompany;
    }

    public Person getContact() {
        return contact;
    }

    public void setContact(Person contact) {
        this.contact = contact;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
