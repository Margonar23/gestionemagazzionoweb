package sample.model;

import javax.persistence.*;

/**
 * Created by vincenzo on 15/03/17.
 */
@Entity
public class ArticleFeature {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String attributeName;
    @ManyToOne
    ArticleCategory articleCategory;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public ArticleCategory getArticleCategories() {
        return articleCategory;
    }

    public void setArticleCategories(ArticleCategory articleCategories) {
        this.articleCategory = articleCategories;
    }


}
