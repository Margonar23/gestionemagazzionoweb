package sample.model;


import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Users implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String lastname;
    private String username;
    private String email;
    private String password;
    private boolean enabled;


    @ManyToOne
    private UserRole role;

    private boolean isBusy = false;
    @ManyToOne
    private Users substitute;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Notification notifications;
    @OneToMany
    private transient List<Warehouse> warehousesManager = new ArrayList<>();

    @OneToMany(mappedBy = "projectManager")
    private transient  List<ConstructionSite> projectManager;
    @OneToMany(mappedBy = "responsible")
    private transient  List<ConstructionSite> responsible;

    public Users() {
    }

    public Users(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        this.password = encoder.encode(password);
    }

    public List<ConstructionSite> getProjectManager() {
        return projectManager;
    }

    public void setProjectManager(List<ConstructionSite> projectManager) {
        this.projectManager = projectManager;
    }

    public List<ConstructionSite> getResponsible() {
        return responsible;
    }

    public void setResponsible(List<ConstructionSite> responsible) {
        this.responsible = responsible;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public boolean isBusy() {
        return isBusy;
    }

    public void setBusy(boolean busy) {
        isBusy = busy;
    }

    public Users getSubstitute() {
        return substitute;
    }

    public void setSubstitute(Users substitute) {
        this.substitute = substitute;
    }

    public Notification getNotifications() {
        return notifications;
    }

    public void setNotifications(Notification notifications) {
        this.notifications = notifications;
    }

    public List<Warehouse> getWarehousesManager() {
        return warehousesManager;
    }

    public void setWarehousesManager(List<Warehouse> warehousesManager) {
        this.warehousesManager = warehousesManager;
    }

    @Override
    public String toString() {
        return getName() + " " + getLastname() + " (" + getUsername() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Users users = (Users) o;

        if (id != users.id) return false;
        if (enabled != users.enabled) return false;
        if (isBusy != users.isBusy) return false;
        if (name != null ? !name.equals(users.name) : users.name != null) return false;
        if (lastname != null ? !lastname.equals(users.lastname) : users.lastname != null) return false;
        if (username != null ? !username.equals(users.username) : users.username != null) return false;
        if (email != null ? !email.equals(users.email) : users.email != null) return false;
        if (role != null ? !role.equals(users.role) : users.role != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (enabled ? 1 : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (isBusy ? 1 : 0);

        return result;
    }
}
