package sample.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by vincenzo on 06/03/17.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String route;
    private String nap;
    private String nation;
    @OneToMany
    private List<Item> itemList= new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getNap() {
        return nap;
    }

    public void setNap(String nap) {
        this.nap = nap;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public Location getObject(){
        return this;
    }


}
