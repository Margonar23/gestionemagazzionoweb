package sample.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincenzo on 15/03/17.
 */
@Entity

public class ArticleCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String category;
    @OneToMany(mappedBy = "articleCategory")
    List<ArticleFeature> features = new ArrayList<>();
    @OneToMany(mappedBy = "articleCategory")
    List<Article> articles = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<ArticleFeature> getFeatures() {
        return features;
    }

    public void setFeatures(List<ArticleFeature> features) {
        this.features = features;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
}
