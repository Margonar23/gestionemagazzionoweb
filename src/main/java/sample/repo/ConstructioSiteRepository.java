package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.ConstructionSite;

import java.util.List;

/**
 * Created by Margonar on 07.03.2017.
 */

@Repository
public interface ConstructioSiteRepository extends CrudRepository<ConstructionSite,Long>{

    List<ConstructionSite> findByIsActive(boolean active);
    Iterable<ConstructionSite> findByNameContainingIgnoreCase (String name);

}
