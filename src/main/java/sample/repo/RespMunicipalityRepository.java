package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.RespMunicipality;

/**
 * Created by Margonar on 07.03.2017.
 */

@Repository
public interface RespMunicipalityRepository extends CrudRepository<RespMunicipality,Long>{

}
