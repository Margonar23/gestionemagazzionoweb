package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Person;

/**
 * Created by Margonar on 07.03.2017.
 */

@Repository
public interface PersonRepository extends CrudRepository<Person,Long>{
    Iterable<Person> findByNameOrLastnameContainingIgnoreCase(String name, String lastname);
}
