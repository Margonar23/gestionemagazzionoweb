package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Item;
import sample.model.Movement;

import java.util.List;

/**
 * Created by Margonar on 07.03.2017.
 */

@Repository
public interface MovementRepository extends CrudRepository<Movement, Long> {
    List<Movement> findByItem(Item item);

}
