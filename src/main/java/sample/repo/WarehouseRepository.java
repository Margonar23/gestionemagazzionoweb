package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Users;
import sample.model.Warehouse;

import java.util.List;

/**
 * Created by Margonar on 07.03.2017.
 */

@Repository
public interface WarehouseRepository extends CrudRepository<Warehouse, Long> {

    List<Warehouse> findByIsActive(boolean active);

    Warehouse findWarehouseByRoute(String route);

    public Iterable<Warehouse> findByNameContainingIgnoreCase(String name);

    List<Warehouse> findByUsersAndIsActive(Users user, boolean active);

}
