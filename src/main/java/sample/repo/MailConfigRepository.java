package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Authorities;
import sample.model.MailConfig;

/**
 * Created by Margonar on 07.03.2017.
 */
@Repository
public interface MailConfigRepository extends CrudRepository<MailConfig,Long>{

}
