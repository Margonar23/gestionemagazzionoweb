package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.LogOperation;

import java.util.List;

/**
 * Created by Margonar on 07.03.2017.
 */

@Repository
public interface LogOperationRepository extends CrudRepository<LogOperation,Long>{
    List<LogOperation> findLogOperationByUser(String user);
    List<LogOperation> findAllByOrderByIdDesc();
}
