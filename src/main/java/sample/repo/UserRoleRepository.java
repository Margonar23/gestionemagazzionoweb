package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.UserRole;

/**
 * Created by Margonar on 07.03.2017.
 */

@Repository
public interface UserRoleRepository extends CrudRepository<UserRole,Long>{


}
