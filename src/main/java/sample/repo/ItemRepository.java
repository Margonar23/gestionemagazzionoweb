package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Article;
import sample.model.Item;
import sample.model.Location;

import java.util.List;

/**
 * Created by vincenzo on 15/03/17.
 */
@Repository
public interface ItemRepository extends CrudRepository<Item,Long> {
    List<Item> findItemByArticle(Article article);
    int countItemByArticleAndLocation(Article a,Location location);
    List<Item> findItemByLocation(Location location);
    List<Item> findBylocation(Location location);
    List<Item> findByArticleAndLocation(Article a, Location l);
    Item findItemByItemCod(String code);

}
