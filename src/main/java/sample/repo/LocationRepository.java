package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Location;
import sample.model.Movement;

/**
 * Created by Margonar on 07.03.2017.
 */

@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {
}
