package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Functionality;

/**
 * Created by Margonar on 07.03.2017.
 */
@Repository
public interface FunctionalityRepository extends CrudRepository<Functionality,Long>{

}
