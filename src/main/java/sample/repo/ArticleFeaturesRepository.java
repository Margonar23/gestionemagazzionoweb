package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.ArticleCategory;
import sample.model.ArticleFeature;

import java.util.List;

/**
 * Created by vincenzo on 15/03/17.
 */
@Repository
public interface ArticleFeaturesRepository extends CrudRepository<ArticleFeature,Long> {
    List<ArticleFeature> findByArticleCategory(ArticleCategory articleCategories);
}
