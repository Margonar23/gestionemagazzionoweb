package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Article;

import java.util.List;

/**
 * Created by vincenzo on 15/03/17.
 */
@Repository
public interface ArticleRepository extends CrudRepository<Article,Long> {
    List<Article> findByIsActive(boolean active);
    Iterable<Article> findByNameOrBrandContainingIgnoreCase(String name, String brand);

}
