package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Article;
import sample.model.Location;
import sample.model.Product_warehouse;
import sample.model.Warehouse;

import java.util.List;

/**
 * Created by vincenzo on 15/03/17.
 */
@Repository
public interface ProductWarehouseRepository extends CrudRepository<Product_warehouse,Long> {
    List<Product_warehouse> findByArticleAndWarehouse(Article a, Location w);
    List<Product_warehouse> findByArticle(Article a);
    List<Product_warehouse> findByWarehouse(Warehouse w);
}
