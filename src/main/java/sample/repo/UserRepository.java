package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sample.model.Users;

import java.util.List;

/**
 * Created by Margonar on 07.03.2017.
 */

@Repository
public interface UserRepository extends CrudRepository<Users, Long> {
    public Users findUsersByUsername(@Param("username") String username);
    public List<Users> findUsersByEnabled(boolean enabled);


    public Iterable<Users> findByUsernameOrEmailOrNameOrLastnameContainingIgnoreCase (String username, String email, String name, String lastname);
}
