package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Company;

/**
 * Created by Margonar on 07.03.2017.
 */

@Repository
public interface CompanyRepository extends CrudRepository<Company,Long>{
    public Iterable<Company> findByNameContainingIgnoreCase (String name);
}
