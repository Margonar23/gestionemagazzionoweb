package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Authorities;

/**
 * Created by Margonar on 07.03.2017.
 */
@Repository
public interface AuthoritiesRepository extends CrudRepository<Authorities,Long>{
    Authorities findAuthoritiesByUsername(String username);
}
