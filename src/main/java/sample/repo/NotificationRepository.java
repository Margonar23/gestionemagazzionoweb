package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.Notification;

/**
 * Created by vincenzo on 15/03/17.
 */
@Repository
public interface NotificationRepository extends CrudRepository<Notification,Long> {

}
