package sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class Application {
	
	// per funzionare serve la connessione ad un server mysql, vedi application.properties e data.sql
	// un primo user viene creato automaticamente user= user e password= password
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}