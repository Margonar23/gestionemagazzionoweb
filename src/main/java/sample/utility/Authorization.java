package sample.utility;

import sample.model.Functionality;
import sample.model.Users;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Margonar on 15.05.2017.
 */
public class Authorization {

    public static boolean isAuthorized(Users user, String functionality) {
        List<Functionality> functionalities = user.getRole().getFunctionalities();
        List<String> func = new ArrayList<>();
        for (Functionality functionality1 : functionalities) {
            func.add(functionality1.getFunctionality());
        }
        return func.contains(functionality);

    }

}
