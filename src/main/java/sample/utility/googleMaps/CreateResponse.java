package sample.utility.googleMaps;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucam on 25.04.2017.
 */
public class CreateResponse {

    public String createResponse(List<Response> response){

        List<JsonResponse> jsonResponses = new ArrayList<>();
        for (Response obj : response) {
            jsonResponses.add(new JsonResponse(obj.getName(),Double.parseDouble(obj.getLat()),Double.parseDouble(obj.getLng()),obj.getSubtext()));
        }
        Gson gson = new Gson();
        String r = gson.toJson(jsonResponses);

        return r;
    }
}
