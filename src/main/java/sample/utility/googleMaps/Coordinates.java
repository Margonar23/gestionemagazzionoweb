package sample.utility.googleMaps;

/**
 * Created by lucam on 25.04.2017.
 */
public class Coordinates {
    private String lat;
    private String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
