package sample.utility.googleMaps;

/**
 * Created by Margonar on 26.04.2017.
 */
public class JsonResponse {
    String title;
    double lat;
    double lng;
    String subtext;

    public JsonResponse(String title, double lat, double lng, String subtext) {
        this.title = title;
        this.lat = lat;
        this.lng = lng;
        this.subtext = subtext;
    }
}
