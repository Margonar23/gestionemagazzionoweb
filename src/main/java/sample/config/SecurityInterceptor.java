package sample.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;


@Component
public class SecurityInterceptor extends HandlerInterceptorAdapter
{


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
    {
        if (modelAndView != null)
        {
            if(!modelAndView.getViewName().startsWith("redirect:") && !modelAndView.getViewName().startsWith("login") ){
                modelAndView.getModel().put("viewDashboard", modelAndView.getViewName());
                modelAndView.setViewName("masterDashboard");
            }else if(modelAndView.getViewName().startsWith("login") ){
                modelAndView.getModel().put("view", modelAndView.getViewName());
                modelAndView.setViewName("masterLogin");
            }
        }
    }
}
