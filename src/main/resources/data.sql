
INSERT INTO user_role VALUES(null,"super user");
INSERT INTO user_role VALUES(null,"gestore magazzino");
INSERT INTO user_role VALUES(null,"magazziniere");


INSERT INTO functionality VALUES(null,"Gestione utenti");
INSERT INTO functionality VALUES(null,"Gestione ruoli");
INSERT INTO functionality VALUES(null,"Visualizzazione log");
INSERT INTO functionality VALUES(null,"Gestione prodotti");
INSERT INTO functionality VALUES(null,"Gestione categorie prodotti");
INSERT INTO functionality VALUES(null,"Gestione magazzini");
INSERT INTO functionality VALUES(null,"Gestione magazzini personali");
INSERT INTO functionality VALUES(null,"Registrazione magazzini");
INSERT INTO functionality VALUES(null,"Gestione cantieri");
INSERT INTO functionality VALUES(null,"Registrazione cantieri");
INSERT INTO functionality VALUES(null,"Gestione cantieri personali");
INSERT INTO functionality VALUES(null,"Configurazione mail");


INSERT INTO notification VALUES(null,"low");
INSERT INTO notification VALUES(null,"full");
INSERT INTO notification VALUES(null,"busy");


INSERT INTO users VALUES(null,"luca.margonar@gmail.com",true,false,"margonar", "luca","$2a$10$Hp8c7mb7oHk7nXrjmfY8pOtzT79Q3x7tVluPwzNNwuuowv4Td40We","marglu",1,1,1);
INSERT INTO authorities VALUES("marglu",1);
INSERT INTO user_role_functionalities VALUES(1,1);
INSERT INTO user_role_functionalities VALUES(1,2);
INSERT INTO user_role_functionalities VALUES(1,3);
INSERT INTO user_role_functionalities VALUES(1,4);
INSERT INTO user_role_functionalities VALUES(1,5);
INSERT INTO user_role_functionalities VALUES(1,6);
INSERT INTO user_role_functionalities VALUES(1,7);
INSERT INTO user_role_functionalities VALUES(1,8);
INSERT INTO user_role_functionalities VALUES(1,9);
INSERT INTO user_role_functionalities VALUES(1,10);
INSERT INTO user_role_functionalities VALUES(1,11);
INSERT INTO user_role_functionalities VALUES(1,12);

INSERT INTO mail_config VALUES(1,"Default Title", "Default Subject");




