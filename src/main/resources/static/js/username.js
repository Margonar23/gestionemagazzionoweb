/**
 * Created by Margonar on 07.03.2017.
 */
$(document).ready(function () {
    $("input[name='name'], input[name='lastname']").keyup(function () {
        var fnVal = $("input[name='name']").val(), lnVal = $("input[name='lastname']").val();
        $("#username").val(lnVal.substring(0,4).toLowerCase()+""+ fnVal.substring(0,2).toLowerCase());
    });
})