/**
 * Created by Margonar on 27.03.2017.
 */
$(document).ready(function () {
    $("#tableProduct").hide();
    $("#tableProductRegisterSelection").hide();

    $("#searchProductRegistration").keyup(function (e) {
        fetchProduct($("#searchProductRegistration").val());
        e.preventDefault();
    });

    $(document).on('click', '.updateFieldRegistrationProduct', function () {
        updateFieldProductRegistration($(this).attr('id'));
    });
});

function fetchProduct(searchText) {
    var url = "/json/product/register/" +  $('#idWarehouse').val() + ((searchText) ? "/search?q=" + searchText : "");

    $.ajax({
        url: url,
        dataType: "json"
    })
        .done(function (data) {

            $('#tableProduct').show();
            $('#foundProduct').empty();
            $(data).each(function (index, element) {
                $('#foundProduct').append('' +
                    '<tr>' +
                    '<td>' + element.id + '</a></td>' +
                    '<td> ' + element.name + ' </td>' +
                    '<td> ' + element.brand + ' </td>' +
                    '<td> ' + element.article_nr + ' </td>' +
                    '<td><span class="updateFieldRegistrationProduct glyphicon glyphicon-ok"  title="Scegli articolo" id="' + element.id + '"></span></td>' +
                    '</tr>');
            })
        })
        .fail(function () {

        });
}

function updateFieldProductRegistration(id) {
    $('#tableProductRegisterSelection').show();
    url = "/json/product/register/choose/"+  $('#idWarehouse').val()+"/" + id;

    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        console.log(data.box);
        if(data.box){
            $("#tableAppenderProduct").append(
                '<tr>' +
                '<td>' + data.id + '</td>' +
                '<td>' + data.name + '</td>' +
                '<td>' + data.brand + '</td>' +
                '<td>' + data.article_nr + '</td>' +
                '<td><input type="number" name="qta" /></td>' +
                '<td><input type="number" name="pezzi"/></td>' +
                '<td><input type="number" name="warning" value="'+data.warning+'"/></td>' +
                '<td><input type="number" name="alarm" value="'+data.alarm+'"/></td>' +
                '<input type="hidden" name="idArticle" value="'+data.id+'"/>' +
                '</tr>'
            );
        }else{
            $("#tableAppenderProduct").append(
                '<tr>' +
                '<td>' + data.id + '</td>' +
                '<td>' + data.name + '</td>' +
                '<td>' + data.brand + '</td>' +
                '<td>' + data.article_nr + '</td>' +
                '<td><input type="number" name="qta"/></td>' +
                '<td><input type="number" name="pezzi" readonly value="1"/></td>' +
                '<td><input type="number" name="warning" value="'+data.warning+'"/></td>' +
                '<td><input type="number" name="alarm" value="'+data.alarm+'"/></td>' +
                '<input type="hidden" name="idArticle" value="'+data.id+'"/>' +
                '</tr>'
            );
        }

    }).fail();
}