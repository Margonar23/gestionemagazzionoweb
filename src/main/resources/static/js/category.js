/**
 * Created by vincenzo on 19/03/17.
 */
$(document).ready(function() {
    $("#generateField").hide();

    $('#category').on('change',function(){
     updateFieldCategory(this.value);
    });
    if(!($('[name=category]').val() == undefined && $('[name=category]').attr('id') == undefined )){
        updateFieldCategoryU( $('[name=category]').val(),$('[name=category]').attr('id'));
    }


    $('[name=category]').on('change',function(){

        updateFieldCategoryU(this.value, $(this).attr('id'));
    });
});

function updateFieldCategory(value){
    var url = "/json/product/register/"+value;
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function(data){
        $("#generateField").show();
        $("#generateField").empty();

        $(data).each(function(index,element) {

           $("#generateField").append(
               '<div class="form-group">' +
               '<label class="col-md-2 control-label">' + element.attributeName + '</label>' +
               '<div class="col-md-7">' +
               '<input name="attributeValue" type="text" placeholder="Inserisci '+ element.attributeName +'" class="form-control input-md" required="">'+
               '</div>'+
               '</div>'
           )
        });
    })
}
function updateFieldCategoryU(value,id){
    var url = "/json/product/update/"+id +"/" + value;
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function(data){
        $("#generateFieldU").show();
        $("#generateFieldU").empty();

        $(data).each(function(index,element) {

            $("#generateFieldU").append(
                '<div class="form-group">' +
                '<label class="col-md-2 control-label">' + element.attributeName + '</label>' +
                '<div class="col-md-7">' +
                '<input name="attributeValues" type="text" value="'+element.attributeValue+'" class="form-control input-md" required="">'+
                '</div>'+
                '</div>'
            )
        });
    })
}