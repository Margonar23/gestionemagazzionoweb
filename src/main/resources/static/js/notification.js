/**
 * Created by vincenzo on 18/04/17.
 */
$(document).ready(function() {
    $("#userSub").hide();
    $("#notificatioLabel").hide();

    updateNotification($('[name=notificationS]').val());

    $('#notification').on('change',function(){
        updateNotification(this.value);
    });

})
function updateNotification(value){
    if(value==3){
        $("#userSub").show();
        $("#notificatioLabel").show();
    }else{
        $("#userSub").hide();
        $("#notificatioLabel").hide();
    }

}