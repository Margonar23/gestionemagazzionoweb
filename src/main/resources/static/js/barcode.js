/**
 * Created by vincenzo on 18/04/17.
 */
$(document).ready(function () {

    readBarCode(43);

});

function readBarCode(value) {

    $('#livevideo').show();
    Quagga.init({
        inputStream: {
            name: "Live",
            type: "LiveStream",
            target: document.querySelector('#livevideo')
        },
        decoder: {
            readers: ["code_128_reader"]
        },
        locate: true
    }, function (err) {
        if (err) {
            console.log(err);
            return
        }
        console.log("Initialization finished. Ready to start");
        Quagga.start();
        console.log("start");
        //Quagga.onProcessed(callback);
        Quagga.onDetected(callback);

    });

}
function callback(data) {
    console.log("decodificato");
    console.log(data.codeResult.code);
    Quagga.stop();
    $('#livevideo').hide();
    var e = document.getElementById("spostamento");
    var spostamentovalue = e.options[e.selectedIndex].value;
    if (spostamentovalue == "spostamento") {
        moveitemRequest(data, hiddenLocation.value, "1");
    } else if (spostamentovalue == "mostramovimenti") {
        movementRequest(data);
    } else if (spostamentovalue == "visualizza") {
        informationRequest(data);
    }
    //invia_dati('http://localhost:8091/parts/'+data.codeResult.code,{ 'nome':'Mario','cognome':'Rossi','id':123},'post');


}
function invia_dati(servURL, params, method) {
    method = method || "post"; // il metodo POST è usato di default
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", servURL);
    for (var key in params) {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value", params[key]);
        form.appendChild(hiddenField);
    }
    document.body.appendChild(form);
    form.submit();
}
// function moveitemRequest(dataCode,location,username) {
//     var url = "http://localhost:8091/moveitem/"+dataCode+"/"+location+"/"+username;
//     $.ajax({
//         url: url,
//         type: "POST",
//         dataType: "json"
//     }).done(function(data){
//         console.log(data);
//     })
// }
// function movementRequest(dataCode) {
//     var url = "http://localhost:8091/movement/"+dataCode;
//     $.ajax({
//         url: url,
//         type: "GET",
//         dataType: "json"
//     }).done(function(data){
//         console.log(data);
//     })
// }
// function informationRequest(dataCode) {
//     var url = "http://localhost:8091/information/"+dataCode;
//     $.ajax({
//         url: url,
//         type: "GET",
//         dataType: "json"
//     }).done(function(data){
//         console.log(data);
//     })
// }

