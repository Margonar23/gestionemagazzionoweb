/**
 * Created by Margonar on 07.03.2017.
 */
$(document).ready(function () {

    //impresa costruzione
    $("#impresaCostruzione").hide();
    $("#ricercaCostruzione").show();

    $("#hideCostruzione").click(function () {
        $("#impresaCostruzione").hide();
        $("#parteNascostaPersonaCostruzione").hide();
        $("#ricercaCostruzione").show();
        $("#ricercaPersonaCostruzione").show();
    });

    $("#showCostruzione").click(function () {
        $("#impresaCostruzione").show();
        $("#ricercaCostruzione").hide();
    });

    //persona
    $("#parteNascostaPersona").hide();
    $("#parteNascostaPersonaCostruzione").hide();
    $("#ricercaPersonaCostruzione").show();

    $("#showPerson").click(function () {
        $("#parteNascostaPersonaCostruzione").show();
        $("#ricercaPersonaCostruzione").hide();
    });

    $("#hidePerson").click(function () {
        $("#parteNascostaPersonaCostruzione").hide();
        $("#ricercaPersonaCostruzione").show();
    });


    //impresa pavimentazione
    $("#impresaPavimentazione").hide();
    $("#ricercaPavimentazione").show();

    $("#hidePavimentazione").click(function () {
        $("#impresaPavimentazione").hide();
        $("#parteNascostaPersonaPavimentazione").hide();
        $("#ricercaPavimentazione").show();
        $("#ricercaPersonaPavimentazione").show();
    });

    $("#showPavimentazione").click(function () {
        $("#impresaPavimentazione").show();
        $("#ricercaPavimentazione").hide();
    });

    //persona
    $("#parteNascostaPersonaPavimentazione").hide();
    $("#ricercaPersonaPavimentazione").show();

    $("#showPersonPavimentazione").click(function () {
        $("#parteNascostaPersonaPavimentazione").show();
        $("#ricercaPersonaPavimentazione").hide();
    });

    $("#hidePersonPavimentazione").click(function () {
        $("#parteNascostaPersonaPavimentazione").hide();
        $("#ricercaPersonaPavimentazione").show();
    });

})