/**
 * Created by vincenzo on 19/03/17.
 */
$(document).ready(function () {
    $("#generateFieldItem").hide();
    $("#generateFieldItemCS").hide();
    $("#itemSet").hide();
    $("#itemSetCS").hide();
    $("#buttonSubmit").hide();
    $("#buttonSubmitCS").hide();
    $('[name=choose]').on('click', function () {
        findItem(this.value, $(this).attr('id'));
        $("#" + $(this).attr('id')).attr('disabled', 'true');
    });
    $('[name=chooseCS]').on('click', function () {
        findItemCS(this.value, $(this).attr('id'));
        $("#" + $(this).attr('id')).attr('disabled', 'true');
    });

});
function findItem(value, id) {

    var id = id.split("_");
    var url = "/json/warehouse/itemListforArticle/" + id[0] + "/" + id[1];
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        $("#itemSet").fadeIn('slow');
        $("#generateFieldItem").fadeIn('slow');
        $("#buttonSubmit").fadeIn('slow');

        $(data).each(function (index, element) {
            $(element).each(function (i, e) {


                $("#generateFieldItem").append(
                    '<tr>' +
                    '<td>' + e.itemCod + '</td>' +

                    '<td><input name="items" type="checkbox" class="form-control input-sm" value="' + e.id + '"></td>' +
                    '</tr>'
                )

            });
        });


    })
}
function findItemCS(value, id) {
    $("#itemSetCS").fadeIn('slow');
    $("#generateFieldItemCS").fadeIn('slow');
    $("#buttonSubmitCS").fadeIn('slow');
    var id = id.split("_");
    var url = "/json/construction/itemListforArticle/" + id[0] + "/" + id[1];
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {

        $(data).each(function (index, element) {
            $(element).each(function (i, e) {


                $("#generateFieldItemCS").append(
                    '<tr>' +
                    '<td>' + e.itemCod + '</td>' +

                    '<td><input name="items" type="checkbox" class="form-control input-sm" value="' + e.id + '"></td>' +
                    '</tr>'
                )

            });
        });


    })
}
