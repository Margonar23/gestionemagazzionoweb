/**
 * Created by Margonar on 20.04.2017.
 */
$(document).ready(function () {
    $("#printBtn").click(function () {
        var divToPrint = document.getElementById('printArea');
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
        newWin.document.close();
    })
});