/**
 * Created by Margonar on 04.04.2017.
 */
$(document).ready(function () {
    $('#logTable').dataTable({
        "searching": true,
        "pageLength": 10,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Italian.json"
        },
        responsive: true
    });

    $('#userTable').dataTable({
        "searching": true, //ricerca automatica nella tabella
        "pageLength": 10, // nr di righe nella tabella
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Italian.json"
        },
        responsive: true
    });

    $('#dataTable').dataTable({
        "searching": true,
        "pageLength": 10,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Italian.json"
        },
        responsive: true
    });
    $('#productTable').dataTable({
        "searching": true,
        "pageLength": 10,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Italian.json"
        },
        responsive: true
    });
    $('#closedTable').dataTable({
        "searching": true,
        "pageLength": 10,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Italian.json"
        },
        responsive: true
    });
});