/**
 * Created by Margonar on 29.03.2017.
 */

// $(document).ready(function () {
//     if ($("#warehouse").val() == 0)
//         initMap();
//     $("#warehouse").on('change', function () {
//         if ($("#warehouse").val() == 0) {
//             initMap();
//         } else {
//             initSingleWarehouseMap($("#warehouse").val());
//         }
//     });
// });

var map;
function initMap() {
    // Creating a new map
    map = new google.maps.Map(document.getElementById("map"), {
        center: {lat: 46.020964, lng: 8.96551049},
        zoom: 8,
        mapTypeId: 'roadmap'
    });


    var json = httpGet("/addressConverter");
    var obj = $.parseJSON(json);

    var i;
    var bounds = new google.maps.LatLngBounds();
    for (i = 0; i < obj.length; i++) {
        console.log(obj[i]['title'])
        var content = createContent(obj[i]['title'],obj[i]['subtext']);

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(parseFloat(obj[i]['lat']), parseFloat(obj[i]['lng']))
        });
        bounds.extend(marker.getPosition());
        marker.setMap(map);
        var infowindow = new google.maps.InfoWindow({
            content: content
        });

        infowindow.open(map, marker);
    }
    map.fitBounds(bounds);

}


function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
}


function createContent(title, content) {
    return '<div><h4>' + title + '</h4><p>' + content + '</p></div>';

}



