$(document).ready(function () {
    $("#reader").hide();
    $("#moveItem").hide();


    var arg = {
        resultFunction: function (result) {
            $("#canvas").WebCodeCamJQuery(arg).data().plugin_WebCodeCamJQuery.stop();
            $("#reader").hide();
            if ($("#spostamento").val() == "seleziona") {
                $("#reader").hide();
                $("#moveItem").hide();
            } else if ($("#spostamento").val() == "visualizza") {
                $("#moveItem").hide();
                showItem(result.code);
            } else if ($("#spostamento").val() == "mostramovimenti") {
                $("#moveItem").hide();
                showMovement(result.code);
            } else if ($("#spostamento").val() == "spostaitem") {
                $("#moveItem").show();
                moveItem(result.code);
            }

        }
    };
    var decoder = $("#canvas").WebCodeCamJQuery(arg).data().plugin_WebCodeCamJQuery;

    decoder.init();

    decoder.buildSelectMenu('#camera-select', 'environment|back').init(arg);


    $('#spostamento').change(function () {
        $("#reader").show();
        $("#itemFound").empty();
        $("#movementItem").empty();
        decoder.play();
    });


});

function showItem(code) {
    var url = "/json/product/findItem/" + code;
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        if (data.itemCod == "error") {
            $("#itemFound").empty();
            $("#itemFound").append(
                '<tr class="alert alert-warninig"><td>Item not found</td></tr>'
            );
        } else {
            $("#itemFound").empty();
            $("#itemFound").append(
                '<tr>' +
                '<th>ID</th>' +
                '<td>' + data.id + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Marca</th>' +
                '<td>' + data.brand + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Nome</th>' +
                '<td>' + data.name + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Nr Articolo</th>' +
                '<td>' + data.articleNr + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Codice item</th>' +
                '<td>' + data.itemCod + '</td>' +
                '</tr>'
            );
        }
    }).fail(function (data) {
        $("#itemFound").empty();
        $("#itemFound").append(
            '<tr class="alert alert-warninig"><td>Item not found</td></tr>'
        );
        console.log(data);
    });
}

function showMovement(code) {
    var url = "/json/product/findItemMovement/" + code;
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        if (data[0].user == "error") {
            $("#movementItem").empty();
            $("#movementItem").append(
                '<tr class="alert alert-warninig"><td>Item not found</td></tr>'
            );
        } else {
            $("#movementItem").empty();
            $("#movementItem").append(
                '<tr><th>ID</th><th>Data movimento</th><th>Da</th><th>A</th><th>Utente</th></tr>'
            )
            $(data).each(function (index, element) {
                $("#movementItem").append(
                    '<tr>' +
                    '<td>' + element.id + '</td>' +
                    '<td>' + element.date.dayOfMonth + '.' + element.date.monthValue + '.' + element.date.year + '</td>' +
                    '<td>' + element.fromLocation + '</td>' +
                    '<td>' + element.toLocation + '</td>' +
                    '<td>' + element.user + '</td>' +
                    '</tr>'
                );
            });
        }
    }).fail(function (data) {
        $("#movementItem").empty();
        $("#movementItem").append(
            '<tr class="alert alert-warninig"><td>Item not found</td></tr>'
        );
    });
}

function moveItem(code) {
    if (code.indexOf("/") >= 0 || code.indexOf(" ") >= 0) {
        $("#movementItem").empty();
    } else {
        $("#itemCode").val(code);
    }
}