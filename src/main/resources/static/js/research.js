$(document).ready(function () {
    $("#foundPavimentazione").hide();
    $("#foundCostruzione").hide();

    $("#search-warehouse-open").keyup(function (e) {
        fetchWarehouse($("#search-warehouse-open").val());
        e.preventDefault();
    });

    $("#userSearch").keyup(function (e) {
        fetchUsers($("#userSearch").val());
        e.preventDefault();
    });

    $("#searchConstructionSite").keyup(function (e) {
        fetchConstructionSite($("#searchConstructionSite").val());
        e.preventDefault();
    });

    $("#ricercaImpresa").keyup(function (e) {
        fetchCompany($("#ricercaImpresa").val());
        e.preventDefault();
    });
    $("#ricercaImpresCostr").keyup(function (e) {
        fetchCompanyCostruzione($("#ricercaImpresCostr").val());
        e.preventDefault();
    });

    $("#searchProduct").keyup(function (e) {
        fetchArticle($("#searchProduct").val());
        e.preventDefault();
    });

    $(document).on('click', '.updateField', function () {
        updateFieldPavimentazione($(this).attr('id'));
    });
    $(document).on('click', '.updateFieldCostruzione', function () {
        updateFieldCostruzione($(this).attr('id'));
    });


    $("#foundPersonPavimentazione").hide();
    $(document).on('keyup', '#ricercaPersonaImpresaPavimentazione', function () {
        $("#foundPersonPavimentazione").show();
        fetchPersonPavimentazione($("#ricercaPersonaImpresaPavimentazione").val());
    });

    $(document).on('click', '.updateFieldPersonPav', function () {
        updateFieldPersonPavimentazione($(this).attr('id'));
    });

    $("#foundPersonCostruzione").hide();
    $(document).on('keyup', '#ricercaPersonaImpresaCostruzione', function () {
        $("#foundPersonCostruzione").show();
        fetchPersonCostruzione($("#ricercaPersonaImpresaCostruzione").val());
    });

    $(document).on('click', '.updateFieldPersonCostr', function () {
        updateFieldPersonCostruzione($(this).attr('id'));
    });


});


function fetchUsers(searchText) {
    var url = "/json/utenti" + ((searchText) ? "/search?q=" + searchText : "");

    $.ajax({
        url: url,
        dataType: "json"
    })
        .done(function (data) {
            $('#usersTable').empty();
            $(data).each(function (index, element) {
                $('#usersTable').append('' +
                    '<tr>' +
                    '<td>' + element.id + '</a></td>' +
                    '<td> ' + element.name + ' </td>' +
                    '<td> ' + element.lastname + ' </td>' +
                    '<td> ' + element.username + ' </td>' +
                    '<td> ' + element.email + ' </td>' +
                    '<td> ' + element.role + ' </td>' +
                    '<td><span class="glyphicon glyphicon-repeat" data-placement="top" title="Reset Password" data-toggle="modal" data-target="#resetPassword' + element.id + '"></span></td>' +
                    '<td><a href="/user/update/' + element.id + '"><span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Modifica utente"></span></a></td>' +
                    '<td><span class="glyphicon glyphicon-trash" data-placement="top" title="Disattiva utente" data-toggle="modal" data-target="#disabledUser' + element.id + '"></span></td>' +
                    '</tr>');
            })
        })
        .fail(function () {

        });
}

function fetchArticle(searchText) {
    var url = "/json/product" + ((searchText) ? "/search?q=" + searchText : "");

    $.ajax({
        url: url,
        dataType: "json"
    })
        .done(function (data) {
            $('#fetchArticle').empty();
            $(data).each(function (index, element) {
                $('#fetchArticle').append('' +
                    '<tr>' +
                    '<td>' + element.id + '</a></td>' +
                    '<td> ' + element.name + ' </td>' +
                    '<td> ' + element.brand + ' </td>' +
                    '<td> ' + element.article_nr + ' </td>' +
                    '<td> ' + element.category + ' </td>' +
                    '<td><a href="/product/information/' + element.id + '"><span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="top" title="informazioni"/></a></td>' +
                    '<td><a href="/product/update/' + element.id + '"><span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Modifica prodotto" /></a></td>' +
                    '<td><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Elimina articolo" data-toggle="modal" data-target="#delete' + element.id + '"></span></td>' +
                    '</tr>');
            })
        })
        .fail(function () {

        });
}

function fetchConstructionSite(searchText) {
    var url = "/json/constructionSite" + ((searchText) ? "/search?q=" + searchText : "");

    $.ajax({
        url: url,
        dataType: "json"
    })
        .done(function (data) {
            $('#constructionSiteTable').empty();
            $(data).each(function (index, element) {
                $('#constructionSiteTable').append('' +
                    '<tr>' +
                    '<td>' + element.id + '</a></td>' +
                    '<td> ' + element.name + ' </td>' +
                    '<td> ' + element.address + ' </td>' +
                    '<td> ' + element.respMun + ' </td>' +
                    '<td> ' + element.imprCostr + ' </td>' +
                    '<td> ' + element.imprPav + ' </td>' +
                    '<td> ' + element.projectManager + ' </td>' +
                    '<td> ' + element.responsable + ' </td>' +
                    '<td><a href="/construction/information/' + element.id + '"><span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="top" title="Informazioni cantiere"></span></a></td>' +
                    '<td><a href="/construction/update/' + element.id + '"><span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Modifica cantiere"></span></a></td>' +
                    '<td><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Elimina Cantiere" data-toggle="modal" data-target="#delete' + element.id + '"></span></td>' +
                    '</tr>');
            })
        })
        .fail(function () {

        });
}

function fetchWarehouse(searchText) {
    var url = "/json/warehouse" + ((searchText) ? "/search?q=" + searchText : "");

    $.ajax({
        url: url,
        dataType: "json"
    })
        .done(function (data) {
            $('#warehouseOpen').empty();
            $(data).each(function (index, element) {
                $('#warehouseOpen').append('' +
                    '<tr>' +
                    '<td>' + element.id + '</a></td>' +
                    '<td> ' + element.name + ' </td>' +
                    '<td> ' + element.address + ' </td>' +
                    '<td> ' + element.user + ' </td>' +
                    '<td><a href="/warehouse/addproduct/' + element.id + '"><span class="glyphicon glyphicon-log-in"  title="registra entrata in magazzino"></span></a></td>' +
                    '<td><a href="/warehouse/inventory"><span class="glyphicon glyphicon-barcode" title="Reset Password"></span></a></td>' +
                    '<td><a href="/warehouse/update/' + element.id + '"><span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" title="Modifica magazzino"></span></a></td>' +
                    '<td><span class="glyphicon glyphicon-trash" data-placement="top" title="Disattiva magazzino" data-toggle="modal" data-target="#delete' + element.id + '"></span></td>' +
                    '</tr>');
            })
        })
        .fail(function () {

        });
}

function fetchCompany(searchText) {
    var url = "/json/company" + ((searchText) ? "/search?q=" + searchText : "");

    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        $("#imprese").empty();
        $(data).each(function (index, element) {
            $("#imprese").append(
                '<tr>' +
                '<td>' + element.name + '</td>' +
                '<td>' + element.route + '</td>' +
                '<td>' + element.nap + '</td>' +
                '<td>' + element.nation + '</td>' +
                '<td>' + element.phone + '</td>' +
                '<td>' + element.fax + '</td>' +
                '<td>' + element.user + '</td>' +
                '<td><span class="updateField glyphicon glyphicon-ok" id="' + element.id + '"></span></td>' +
                '</tr>'
            );
        })
    })
}
function fetchCompanyCostruzione(searchText) {
    var url = "/json/company" + ((searchText) ? "/search?q=" + searchText : "");

    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        $("#impreseCostruzione").empty();
        $(data).each(function (index, element) {
            $("#impreseCostruzione").append(
                '<tr>' +
                '<td>' + element.name + '</td>' +
                '<td>' + element.route + '</td>' +
                '<td>' + element.nap + '</td>' +
                '<td>' + element.nation + '</td>' +
                '<td>' + element.phone + '</td>' +
                '<td>' + element.fax + '</td>' +
                '<td>' + element.user + '</td>' +
                '<td><span class="updateFieldCostruzione glyphicon glyphicon-ok" id="' + element.id + '"></span></td>' +
                '</tr>'
            );
        })
    })
}

function updateFieldPavimentazione(id) {
    $("#foundPavimentazione").show();
    $("#ricercaPavimentazione").hide();
    $("#imprese").empty();

    var url = "/json/company/" + id;
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        $("#idPavimentazione").val(data.id);
        $("#impresaTrovata").append(
            '<tr><th>Nome</th><td>' + data.name + '</td></tr>' +
            '<tr><th>Indirizzo</th><td>' + data.route + ", " + data.nap + " " + data.nation + '</td></tr>' +
            '<tr><th>Telefono</th><td>' + data.phone + '</td></tr>' +
            '<tr><th>Fax</th><td>' + data.fax + '</td></tr>' +
            '<tr><th>Contatto</th><td>' + data.user + '</td></tr>'
        );
    }).fail();
}
function updateFieldCostruzione(id) {
    $("#foundCostruzione").show();
    $("#ricercaImpresCostr").hide();
    $("#ricercaCostruzione").hide();
    $("#impreseCostruzione").empty();

    var url = "/json/company/" + id;
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        $("#idCostruzione").val(data.id);
        $("#impresaCostruzioneTrovata").append(
            '<tr><th>Nome</th><td>' + data.name + '</td></tr>' +
            '<tr><th>Indirizzo</th><td>' + data.route + ", " + data.nap + " " + data.nation + '</td></tr>' +
            '<tr><th>Telefono</th><td>' + data.phone + '</td></tr>' +
            '<tr><th>Fax</th><td>' + data.fax + '</td></tr>' +
            '<tr><th>Contatto</th><td>' + data.user + '</td></tr>'
        );
    }).fail();
}
function fetchPersonPavimentazione(searchText) {
    var url = "/json/company/personPavimentazione/" + ((searchText) ? "/search?q=" + searchText : "");
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        $("#personaImpresaPavimentazione").empty();
        $(data).each(function (index, element) {
            $("#personaImpresaPavimentazione").append(
                '<tr>' +
                '<td>' + element.name + '</td>' +
                '<td>' + element.lastname + '</td>' +
                '<td>' + element.email + '</td>' +
                '<td><span class="updateFieldPersonPav glyphicon glyphicon-ok" id="' + element.id + '"></span></td>' +
                '</tr>'
            );
        });
    }).fail();
}
function fetchPersonCostruzione(searchText) {
    var url = "/json/company/personPavimentazione/" + ((searchText) ? "/search?q=" + searchText : "");
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        $("#personaCostrTrovata").empty();
        $(data).each(function (index, element) {
            $("#personaCostrTrovata").append(
                '<tr>' +
                '<td>' + element.name + '</td>' +
                '<td>' + element.lastname + '</td>' +
                '<td>' + element.email + '</td>' +
                '<td><span class="updateFieldPersonCostr glyphicon glyphicon-ok" id="' + element.id + '"></span></td>' +
                '</tr>'
            );
        });
    }).fail();
}
function updateFieldPersonPavimentazione(id) {
    $("#personaTrovataPavimentazione").show();
    $("#ricercaPersonaPavimentazione").hide();
    $("#foundPersonPavimentazione").hide();


    var url = "/json/company/personPavimentazione/" + id;
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        $("#idPersonPavimentazione").val(data.id);
        $("#personaPavTrovata").append(
            '<tr><th>Nome</th><td>' + data.name + '</td></tr>' +
            '<tr><th>Cognome</th><td>' + data.lastname + '</td></tr>' +
            '<tr><th>E-Mail</th><td>' + data.email + '</td></tr>'
        );
    }).fail();
}
function updateFieldPersonCostruzione(id) {
    $("#personaTrovataCostruzione").show();
    $("#ricercaPersonaCostruzione").hide();
    $("#personaCostrTrovata").empty();
    $("#foundPersonCostruzione").hide();


    var url = "/json/company/personPavimentazione/" + id;
    $.ajax({
        url: url,
        dataType: "json"
    }).done(function (data) {
        $("#idPersonCostruzione").val(data.id);
        $("#personaCostrTrovata").append(
            '<tr><th>Nome</th><td>' + data.name + '</td></tr>' +
            '<tr><th>Cognome</th><td>' + data.lastname + '</td></tr>' +
            '<tr><th>E-Mail</th><td>' + data.email + '</td></tr>'
        );
    }).fail();
}