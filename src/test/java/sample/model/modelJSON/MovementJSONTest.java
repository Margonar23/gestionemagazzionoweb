package sample.model.modelJSON;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

/**
 * Created by Margonar on 09.05.2017.
 */
public class MovementJSONTest {
    MovementJSON movement;

    @Before
    public void setUp() throws Exception {
        LocalDate.now();
        movement = new MovementJSON(100l, LocalDate.now(), "", "", 100l, "");
    }

    @Test
    public void getId() throws Exception {
        movement.setId(100l);
        Assert.assertEquals(100l, movement.getId());
    }

    @Test
    public void getDate() throws Exception {
        LocalDate date = LocalDate.now();
        movement.setDate(date);
        Assert.assertEquals(date, movement.getDate());
    }

    @Test
    public void getFromLocation() throws Exception {
        movement.setFromLocation("value");
        Assert.assertEquals("value", movement.getFromLocation());
    }

    @Test
    public void getToLocation() throws Exception {
        movement.setToLocation("value");
        Assert.assertEquals("value", movement.getToLocation());
    }

    @Test
    public void getItemId() throws Exception {
        movement.setItemId(100l);
        Assert.assertEquals(100l, movement.getItemId());
    }

    @Test
    public void getUser() throws Exception {
        movement.setUser("value");
        Assert.assertEquals("value", movement.getUser());
    }


}