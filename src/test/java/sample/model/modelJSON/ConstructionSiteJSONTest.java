package sample.model.modelJSON;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Margonar on 08.05.2017.
 */
public class ConstructionSiteJSONTest {
    ConstructionSiteJSON construction;

    @Before
    public void setUp() throws Exception {
        construction = new ConstructionSiteJSON(0l, "", "", 0l, "", "", "", "");
    }

    @Test
    public void getId() throws Exception {
        construction.setId(100l);
        Assert.assertEquals(100l, construction.getId());
    }

    @Test
    public void getName() throws Exception {
        construction.setName("value");
        Assert.assertEquals("value", construction.getName());
    }

    @Test
    public void getAddress() throws Exception {
        construction.setAddress("value");
        Assert.assertEquals("value", construction.getAddress());
    }

    @Test
    public void getRespMun() throws Exception {
        construction.setRespMun(100l);
        Assert.assertEquals(100l, construction.getRespMun());
    }

    @Test
    public void getImprCostr() throws Exception {
        construction.setImprCostr("value");
        Assert.assertEquals("value", construction.getImprCostr());
    }

    @Test
    public void getImprPav() throws Exception {
        construction.setImprPav("value");
        Assert.assertEquals("value", construction.getImprPav());
    }

    @Test
    public void getProjectManager() throws Exception {
        construction.setProjectManager("value");
        Assert.assertEquals("value", construction.getProjectManager());
    }

    @Test
    public void getResponsable() throws Exception {
        construction.setResponsable("value");
        Assert.assertEquals("value", construction.getResponsable());
    }

}