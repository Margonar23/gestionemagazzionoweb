package sample.model.modelJSON;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Margonar on 08.05.2017.
 */
public class ArticleDetailJSONTest {
    ArticleDetailJSON detail;

    @Before
    public void setup(){
        detail = new ArticleDetailJSON(0l,"");
        detail = new ArticleDetailJSON(0l,0l,"","");
    }

    @Test
    public void getIdDetail() throws Exception {
        detail.setIdDetail(100l);
        Assert.assertEquals(100l,detail.getIdDetail());
    }

    @Test
    public void getIdFeature() throws Exception {
        detail.setIdFeature(100l);
        Assert.assertEquals(100l,detail.getIdFeature());
    }

    @Test
    public void getAttributeName() throws Exception {
        detail.setAttributeName("Name");
        Assert.assertEquals("Name", detail.getAttributeName());
    }

    @Test
    public void getAttributeValue() throws Exception {
        detail.setAttributeValue("Value");
        Assert.assertEquals("Value", detail.getAttributeValue());
    }



}
