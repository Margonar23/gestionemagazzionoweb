package sample.model.modelJSON;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Margonar on 09.05.2017.
 */
public class ItemWarehouseTest {
    ItemWarehouse itemWarehouse;

    @Before
    public void setUp() throws Exception {
        itemWarehouse = new ItemWarehouse("", "", "", "", 0, 0);
    }

    @Test
    public void getName() throws Exception {
        itemWarehouse.setName("value");
        Assert.assertEquals("value", itemWarehouse.getName());
    }

    @Test
    public void getBrand() throws Exception {
        itemWarehouse.setBrand("value");
        Assert.assertEquals("value", itemWarehouse.getBrand());
    }

    @Test
    public void getArticleId() throws Exception {
        itemWarehouse.setArticleId("value");
        Assert.assertEquals("value", itemWarehouse.getArticleId());
    }

    @Test
    public void getWarehouse() throws Exception {
        itemWarehouse.setWarehouse("value");
        Assert.assertEquals("value", itemWarehouse.getWarehouse());
    }

    @Test
    public void getQuantity() throws Exception {
        itemWarehouse.setQuantity(10);
        Assert.assertEquals(10, itemWarehouse.getQuantity());
    }

    @Test
    public void getAlarm() throws Exception {
        itemWarehouse.setAlarm(10);
        Assert.assertEquals(10, itemWarehouse.getAlarm());
    }

}