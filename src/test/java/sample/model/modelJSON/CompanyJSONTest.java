package sample.model.modelJSON;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Margonar on 08.05.2017.
 */
public class CompanyJSONTest {
    CompanyJSON company;
    @Before
    public void setUp() throws Exception {
        company = new CompanyJSON(0l,"","","","","","","");
    }

    @Test
    public void getId() throws Exception {
        company.setId(100l);
        Assert.assertEquals(100l,company.getId());
    }

    @Test
    public void getName() throws Exception {
        company.setName("value");
        Assert.assertEquals("value",company.getName());
    }

    @Test
    public void getRoute() throws Exception {
        company.setRoute("value");
        Assert.assertEquals("value",company.getRoute());
    }

    @Test
    public void getNap() throws Exception {
        company.setNap("value");
        Assert.assertEquals("value",company.getNap());
    }

    @Test
    public void getNation() throws Exception {
        company.setNation("value");
        Assert.assertEquals("value",company.getNation());
    }

    @Test
    public void getPhone() throws Exception {
        company.setPhone("value");
        Assert.assertEquals("value",company.getPhone());
    }

    @Test
    public void getFax() throws Exception {
        company.setFax("value");
        Assert.assertEquals("value",company.getFax());
    }

    @Test
    public void getUser() throws Exception {
        company.setUser("value");
        Assert.assertEquals("value",company.getUser());
    }

}