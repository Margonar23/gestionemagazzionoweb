package sample.model.modelJSON;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Margonar on 09.05.2017.
 */
public class WarehouseJSONTest {

    WarehouseJSON warehouse;

    @Before
    public void setUp() throws Exception {
        warehouse = new WarehouseJSON(0l, "", "", "", true);
    }

    @Test
    public void getId() throws Exception {
        warehouse.setId(100l);
        Assert.assertEquals(100l,warehouse.getId());
    }

    @Test
    public void getName() throws Exception {
        warehouse.setName("value");
        Assert.assertEquals("value",warehouse.getName());
    }

    @Test
    public void getAddress() throws Exception {
        warehouse.setAddress("value");
        Assert.assertEquals("value",warehouse.getAddress());
    }

    @Test
    public void getUser() throws Exception {
        warehouse.setUser("value");
        Assert.assertEquals("value",warehouse.getUser());
    }

    @Test
    public void isActive() throws Exception {
        warehouse.setActive(true);
        Assert.assertEquals(true,warehouse.isActive());
    }

}