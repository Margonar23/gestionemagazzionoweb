package sample.model.modelJSON;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Margonar on 09.05.2017.
 */
public class PersonJSONTest {
    PersonJSON person;
    @Before
    public void setUp() throws Exception {
        person = new PersonJSON(0l,"","","");
    }

    @Test
    public void getId() throws Exception {
        person.setId(100l);
        Assert.assertEquals(100l,person.getId());
    }

    @Test
    public void getName() throws Exception {
        person.setName("value");
        Assert.assertEquals("value",person.getName());
    }

    @Test
    public void getLastname() throws Exception {
        person.setLastname("value");
        Assert.assertEquals("value",person.getLastname());
    }

    @Test
    public void getEmail() throws Exception {
        person.setEmail("value");
        Assert.assertEquals("value",person.getEmail());
    }

}