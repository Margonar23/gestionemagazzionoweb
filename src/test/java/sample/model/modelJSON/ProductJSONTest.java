package sample.model.modelJSON;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Margonar on 09.05.2017.
 */
public class ProductJSONTest {
    ProductJSON product;

    @Before
    public void setUp() throws Exception {
        product = new ProductJSON(0l, "", true, "", "", "", "", 0, 0, 0, true, 0);
    }

    @Test
    public void getLabel() throws Exception {
        product.setLabel(10);
        Assert.assertEquals(10, product.getLabel());

    }

    @Test
    public void getQuantity() throws Exception {
        product.setQuantity(10);
        Assert.assertEquals(10, product.getQuantity());
    }

    @Test
    public void getLocation() throws Exception {
        product.setLocation("value");
        Assert.assertEquals("value", product.getLocation());
    }

    @Test
    public void getCategory() throws Exception {
        product.setCategory("value");
        Assert.assertEquals("value", product.getCategory());
    }

    @Test
    public void getId() throws Exception {
        product.setId(10l);
        Assert.assertEquals(10l, product.getId());
    }

    @Test
    public void getBrand() throws Exception {
        product.setBrand("value");
        Assert.assertEquals("value", product.getBrand());
    }

    @Test
    public void isActive() throws Exception {
        product.setActive(true);
        Assert.assertEquals(true, product.isActive());
    }

    @Test
    public void getArticle_nr() throws Exception {
        product.setArticle_nr("value");
        Assert.assertEquals("value", product.getArticle_nr());
    }

    @Test
    public void getName() throws Exception {
        product.setName("value");
        Assert.assertEquals("value", product.getName());
    }

    @Test
    public void getAlarm() throws Exception {
        product.setAlarm(10);
        Assert.assertEquals(10, product.getAlarm());
    }

    @Test
    public void getWarning() throws Exception {
        product.setWarning(10);
        Assert.assertEquals(10, product.getWarning());
    }

    @Test
    public void isBox() throws Exception {
        product.setBox(true);
        Assert.assertEquals(true, product.isBox());
    }

}