package sample.model.modelJSON;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Margonar on 09.05.2017.
 */
public class ItemJSONTest {
    ItemJSON item;

    @Before
    public void setUp() throws Exception {
        item = new ItemJSON(100l, "", 10);
        item = new ItemJSON(100l, "", 10, "", "", "");
    }

    @Test
    public void getBrand() throws Exception {
        item.setBrand("value");
        Assert.assertEquals("value", item.getBrand());
    }

    @Test
    public void getName() throws Exception {
        item.setName("value");
        Assert.assertEquals("value", item.getName());
    }

    @Test
    public void getArticleNr() throws Exception {
        item.setArticleNr("value");
        Assert.assertEquals("value", item.getArticleNr());
    }

    @Test
    public void getId() throws Exception {
        item.setId(100l);
        Assert.assertEquals(100l, item.getId());
    }

    @Test
    public void getItemCod() throws Exception {
        item.setItemCod("value");
        Assert.assertEquals("value", item.getItemCod());
    }

    @Test
    public void getQuantity() throws Exception {
        item.setQuantity(10);
        Assert.assertEquals(10, item.getQuantity());
    }

}