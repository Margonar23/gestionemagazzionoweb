package sample.model.modelJSON;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Margonar on 09.05.2017.
 */
public class ItemConstructionTest {

    ItemConstruction itemConstruction;

    @Before
    public void setUp() throws Exception {
        itemConstruction = new ItemConstruction("","","","",0);
    }

    @Test
    public void getName() throws Exception {
        itemConstruction.setName("value");
        Assert.assertEquals("value",itemConstruction.getName());
    }

    @Test
    public void getBrand() throws Exception {
        itemConstruction.setBrand("value");
        Assert.assertEquals("value",itemConstruction.getBrand());
    }

    @Test
    public void getArticleId() throws Exception {
        itemConstruction.setArticleId("value");
        Assert.assertEquals("value",itemConstruction.getArticleId());
    }

    @Test
    public void getWarehouse() throws Exception {
        itemConstruction.setWarehouse("value");
        Assert.assertEquals("value",itemConstruction.getWarehouse());
    }

    @Test
    public void getQuantity() throws Exception {
        itemConstruction.setQuantity(10);
        Assert.assertEquals(10,itemConstruction.getQuantity());
    }

}