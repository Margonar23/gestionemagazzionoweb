package sample.model.modelJSON;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Margonar on 09.05.2017.
 */
public class UsersJSONTest {
    UsersJSON user;

    @Before
    public void setUp() throws Exception {
        user = new UsersJSON(10l, "", "", "", "", "", true);
    }

    @Test
    public void getId() throws Exception {
        user.setId(10l);
        Assert.assertEquals(10l,user.getId());
    }

    @Test
    public void getName() throws Exception {
        user.setName("value");
        Assert.assertEquals("value",user.getName());
    }

    @Test
    public void getLastname() throws Exception {
        user.setLastname("value");
        Assert.assertEquals("value",user.getLastname());
    }

    @Test
    public void getUsername() throws Exception {
        user.setUsername("value");
        Assert.assertEquals("value",user.getUsername());
    }

    @Test
    public void getEmail() throws Exception {
        user.setEmail("value");
        Assert.assertEquals("value",user.getEmail());
    }

    @Test
    public void getRole() throws Exception {
        user.setRole("value");
        Assert.assertEquals("value",user.getRole());
    }

    @Test
    public void isActive() throws Exception {
        user.setActive(true);
        Assert.assertEquals(true,user.isActive());
    }

}