package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 18/04/17.
 */
public class ItemTest {
    Item item;
    @Before
    public void setUpClass(){
        item = new Item();
    }
    @Test
    public void getId() throws Exception {
        item.setId(89);
        Assert.assertEquals(89,item.getId());
    }

    @Test
    public void getArticle() throws Exception {
        Article a = new Article();
        item.setArticle(a);
        assertEquals(a,item.getArticle());
    }

    @Test
    public void getQuantity() throws Exception {
        item.setQuantity(100);
        Assert.assertEquals(100,item.getQuantity());
    }

    @Test
    public void getDate() throws Exception {
        LocalDate ld = LocalDate.now();
        item.setDate(ld);
        Assert.assertEquals(ld,item.getDate());
    }

    @Test
    public void getLocation() throws Exception {
        Location l = new Warehouse();
        item.setLocation(l);
        Assert.assertEquals(l,item.getLocation());

    }

    @Test
    public void getItemCod() throws Exception {
        item.setItemCod("AB894523");
        Assert.assertEquals("AB894523",item.getItemCod());
    }

}