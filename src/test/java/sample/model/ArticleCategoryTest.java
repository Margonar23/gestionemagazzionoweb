package sample.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 14/04/17.
 */
public class ArticleCategoryTest {
    ArticleCategory articleCategory;
    @Before
    public void setUpClass(){
        articleCategory = new ArticleCategory();
    }


    @Test
    public void getId() throws Exception {
        articleCategory.setId(89);
        assertEquals(89,articleCategory.getId());
    }

    @Test
    public void getCategory() throws Exception {
        articleCategory.setCategory("pressione");
        assertEquals("pressione",articleCategory.getCategory());
    }

    @Test
    public void getFeatures() throws Exception {
        ArrayList<ArticleFeature> articleFeatures = new ArrayList<>();
        articleFeatures.add(new ArticleFeature());

        articleCategory.setFeatures(articleFeatures);
        assertEquals(articleFeatures, articleCategory.getFeatures());

    }

    @Test
    public void getArticles() throws Exception {
        ArrayList<Article> articles = new ArrayList<>();
        articles.add(new Article());
        articleCategory.setArticles(articles);
        assertEquals(articles,articleCategory.getArticles());
    }

}