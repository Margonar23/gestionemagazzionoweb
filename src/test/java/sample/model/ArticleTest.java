package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by vincenzo on 14/04/17.
 */
public class ArticleTest {
    Article article;
    @Before
    public void setUp(){
        article = new Article();
    }
    @Test
    public void isActive() throws Exception {
        article.setActive(false);
        Assert.assertEquals(false,article.isActive());
        article.setActive(true);
        Assert.assertEquals(true,article.isActive());
    }


    @Test
    public void getDetails() throws Exception {

        ArticleCategory articleCategory = new ArticleCategory();
        articleCategory.setCategory("tubo");
        ArticleFeature articleFeature = new ArticleFeature();
        articleFeature.setAttributeName("pressione");
        ArrayList<ArticleFeature> features = new ArrayList<>();
        features.add(articleFeature);
        articleCategory.setFeatures(features);
        article.setArticleCategory(articleCategory);
        ArrayList<ArticleDetail> details = new ArrayList<>();

        for (ArticleFeature feature : article.getArticleCategory().getFeatures()){
            ArticleDetail articleDetail = new ArticleDetail();
            articleDetail.setFeature(feature);
            articleDetail.setAttributeValue("32");
            details.add(articleDetail);
        }

        article.setDetails(details);
        for (ArticleDetail articleDetail : article.getDetails()) {
            System.out.println(articleDetail.getFeature().getAttributeName()+ ": " +articleDetail.getAttributeValue());
        }

        Assert.assertEquals(details,article.getDetails());
    }


    @Test
    public void getId() throws Exception {
        article.setId(98);
        Assert.assertEquals(98,article.getId());

    }


    @Test
    public void getBrand() throws Exception {
    article.setBrand("marca");
    Assert.assertEquals("marca",article.getBrand());
    }

    @Test
    public void getArticle_nr() throws Exception {
        article.setArticle_nr("ABCDEF_12");
        Assert.assertEquals("ABCDEF_12",article.getArticle_nr());
    }

    @Test
    public void getName() throws Exception {
        article.setName("nome tubo");
        Assert.assertEquals("nome tubo",article.getName());
    }

    @Test
    public void getArticleCategory() throws Exception {
        ArticleCategory articleCategory = new ArticleCategory();
        article.setArticleCategory(articleCategory);
        Assert.assertEquals(articleCategory,article.getArticleCategory());

    }

    @Test
    public void isBoxItem() throws Exception {
        article.setBoxItem(false);
        Assert.assertEquals(false,article.isBoxItem());
        article.setBoxItem(true);
        Assert.assertEquals(true,article.isBoxItem());


    }

}