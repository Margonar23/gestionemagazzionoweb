package sample.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 18/04/17.
 */
public class UserRoleTest {
    UserRole userRole;
    @Before
    public void setUpClass(){
        userRole= new UserRole();
    }

    @Test
    public void getId() throws Exception {
        userRole.setId(89);
        assertEquals(89,userRole.getId());
    }

    @Test
    public void getRole() throws Exception {
        userRole.setRole("role");
        assertEquals("role",userRole.getRole());
    }

    @Test
    public void getUsers() throws Exception {
        Users u = new Users();
        ArrayList<Users> users = new ArrayList<>();
        users.add(u);
        userRole.setUsers(users);
        assertEquals(users,userRole.getUsers());

    }

    @Test
    public void getFunctionalities() throws Exception {
        Functionality f = new Functionality();
        ArrayList<Functionality> functionalities = new ArrayList<>();
        functionalities.add(f);
        userRole.setFunctionalities(functionalities);
        assertEquals(functionalities,userRole.getFunctionalities());

    }

}