package sample.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 18/04/17.
 */
public class CompanyTest {
    Company company;
    @Before
    public void setUpClass(){
        company = new Company();
    }

    @Test
    public void getId() throws Exception {
        company.setId(89);
        assertEquals(89,company.getId());
    }

    @Test
    public void getAssignedConstructionFirm() throws Exception {
        ConstructionSite c = new ConstructionSite();
        ArrayList<ConstructionSite> constructionSites = new ArrayList<>();
        constructionSites.add(c);
        company.setAssignedConstructionFirm(constructionSites);
        assertEquals(constructionSites, company.getAssignedConstructionFirm());
    }

    @Test
    public void getAssignedFlooringCompany() throws Exception {
        ConstructionSite c = new ConstructionSite();
        ArrayList<ConstructionSite> constructionSites = new ArrayList<>();
        constructionSites.add(c);
        company.setAssignedFlooringCompany(constructionSites);
        assertEquals(constructionSites, company.getAssignedFlooringCompany());
    }

    @Test
    public void getContact() throws Exception {
        Person p = new Person();
        company.setContact(p);
        assertEquals(p,company.getContact());

    }

    @Test
    public void getFax() throws Exception {
        company.setFax("231456789");
        assertEquals("231456789",company.getFax());
    }

    @Test
    public void getPhone() throws Exception {
        company.setPhone("231456789");
        assertEquals("231456789",company.getPhone());
    }

}