package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincenzo on 18/04/17.
 */
public class FunctionalityTest {
    Functionality functionality;
    @Before
    public void setUpClass(){
        functionality = new Functionality();
    }
    @Test
    public void getId() throws Exception {
    functionality.setId(89);
        Assert.assertEquals(89,functionality.getId());
    }

    @Test
    public void getFunctionality() throws Exception {
        functionality.setFunctionality("aggiungere utenti");
        Assert.assertEquals("aggiungere utenti",functionality.getFunctionality());
    }

    @Test
    public void getRoles() throws Exception {
        List<UserRole> userRoles = new ArrayList<>();
        userRoles.add(new UserRole());
        functionality.setRoles(userRoles);
        Assert.assertEquals(userRoles,functionality.getRoles());
    }

}