package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by vincenzo on 18/04/17.
 */
public class Product_warehouseTest {
    Product_warehouse product_warehouse;
    @Before
    public void setUpClass(){
        product_warehouse = new Product_warehouse();
    }
    @Test
    public void getId() throws Exception {
        product_warehouse.setId(89);
        Assert.assertEquals(89,product_warehouse.getId());
    }

    @Test
    public void getArticle() throws Exception {
       Article a = new Article();
       product_warehouse.setArticle(a);
       Assert.assertEquals(a,product_warehouse.getArticle());
    }

    @Test
    public void getWarehouse() throws Exception {
        Warehouse w = new Warehouse();
        product_warehouse.setWarehouse(w);
        Assert.assertEquals(w,product_warehouse.getWarehouse());
    }

    @Test
    public void getWarining() throws Exception {
        product_warehouse.setWarining(32);
        Assert.assertEquals(32,product_warehouse.getWarining());
    }

    @Test
    public void getAlert() throws Exception {
        product_warehouse.setAlert(50);
        Assert.assertEquals(50,product_warehouse.getAlert());
    }

}