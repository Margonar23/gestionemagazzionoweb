package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

/**
 * Created by vincenzo on 18/04/17.
 */
public class LogOperationTest {
    LogOperation lo;

    @Before
    public void setUpClass(){
        lo =  new LogOperation();
    }


    @Test
    public void getId() throws Exception {
        lo.setId(89);
        Assert.assertEquals(89,lo.getId());
    }

    @Test
    public void getDate() throws Exception {
        Date d = new Date();
        lo.setDate(d);
        Assert.assertEquals(d,lo.getDate());
    }

    @Test
    public void getUser() throws Exception {
        lo.setUser("user");
        Assert.assertEquals("user",lo.getUser());
    }

    @Test
    public void getOperation() throws Exception {
       lo.setOperation("operation");
       Assert.assertEquals("operation",lo.getOperation());
    }

    @Test
    public void getData() throws Exception {
        lo.setData("12/05/2016");
        Assert.assertEquals("12/05/2016",lo.getData());
    }

    @Test
    public void getIdData() throws Exception {
        lo.setIdData(32);
        Assert.assertEquals(32,lo.getIdData());
    }

}