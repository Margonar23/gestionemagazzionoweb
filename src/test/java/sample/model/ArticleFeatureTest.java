package sample.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 14/04/17.
 */
public class ArticleFeatureTest {
    ArticleFeature articleFeature;
    @Before
    public void setUpClass(){
        articleFeature = new ArticleFeature();
    }
    @Test
    public void getId() throws Exception {
        articleFeature.setId(89);
        assertEquals(89,articleFeature.getId());
    }

    @Test
    public void getAttributeName() throws Exception {
        articleFeature.setAttributeName("pressione");
        assertEquals("pressione",articleFeature.getAttributeName());
    }

    @Test
    public void getArticleCategories() throws Exception {
        ArticleCategory articleCategory = new ArticleCategory();
        articleFeature.setArticleCategories(articleCategory);
        assertEquals(articleCategory,articleFeature.getArticleCategories());
    }

}