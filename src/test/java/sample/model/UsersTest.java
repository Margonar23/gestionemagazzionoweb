package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by vincenzo on 18/04/17.
 */
public class UsersTest {
    Users user;
    @Before
    public void setUpClass(){
        user = new Users();
    }

    @Test
    public void getId() throws Exception {
        user.setId(89);
        Assert.assertEquals(89,user.getId());
    }

    @Test
    public void getName() throws Exception {
        user.setName("name");
        Assert.assertEquals("name",user.getName());
    }

    @Test
    public void getLastname() throws Exception {
        user.setLastname("lastname");
        Assert.assertEquals("lastname",user.getLastname());
    }

    @Test
    public void getUsername() throws Exception {
        user.setUsername("username");
        Assert.assertEquals("username",user.getUsername());
    }

    @Test
    public void getEmail() throws Exception {
        user.setEmail("mail");
        Assert.assertEquals("mail",user.getEmail());
    }

    @Test
    public void isEnabled() throws Exception {
        Assert.assertEquals(false,user.isEnabled());
        user.setEnabled(true);
        Assert.assertEquals(true,user.isEnabled());
    }


    @Test
    public void getProjectManager() throws Exception {
        ConstructionSite cs = new ConstructionSite();
        ArrayList<ConstructionSite> constructionSites = new ArrayList<>();
        constructionSites.add(cs);
        user.setProjectManager(constructionSites);
        Assert.assertEquals(constructionSites,user.getProjectManager());

    }

    @Test
    public void getResponsible() throws Exception {
        ConstructionSite cs = new ConstructionSite();
        ArrayList<ConstructionSite> constructionSites = new ArrayList<>();
        constructionSites.add(cs);
        user.setResponsible(constructionSites);
        Assert.assertEquals(constructionSites,user.getResponsible());
    }

    @Test
    public void getRole() throws Exception {
        UserRole userRole = new UserRole();
        user.setRole(userRole);
        Assert.assertEquals(userRole,user.getRole());

    }

    @Test
    public void isBusy() throws Exception {
        Assert.assertEquals(false,user.isBusy());
        user.setBusy(true);
        Assert.assertEquals(true,user.isBusy());
    }

    @Test
    public void getSubstitute() throws Exception {
        Users userSub = new Users();
        user.setSubstitute(userSub);
        Assert.assertEquals(userSub,user.getSubstitute());

    }

    @Test
    public void getNotifications() throws Exception {
        Notification n = new Notification();
        n.setNotification("low");
        user.setNotifications(n);
        Assert.assertEquals(n,user.getNotifications());

    }

    @Test
    public void getWarehousesManager() throws Exception {
        Warehouse w = new Warehouse();
        ArrayList<Warehouse> warehouses = new ArrayList<>();
        warehouses.add(w);
        user.setWarehousesManager(warehouses);
        Assert.assertEquals(warehouses,user.getWarehousesManager());

    }

}