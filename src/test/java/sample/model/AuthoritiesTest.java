package sample.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 14/04/17.
 */
public class AuthoritiesTest {
    Authorities authorities;
    @Before
    public void setUpClass(){
        authorities = new Authorities();
    }
    @Test
    public void getUsername() throws Exception {
        authorities.setUsername("username");
        assertEquals("username",authorities.getUsername());
    }

    @Test
    public void getAuthority() throws Exception {
        authorities.setAuthority("authority");
        assertEquals("authority",authorities.getAuthority());
    }

}