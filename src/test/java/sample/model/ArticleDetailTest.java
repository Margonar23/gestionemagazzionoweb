package sample.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 14/04/17.
 */
public class ArticleDetailTest {
    ArticleDetail articleDetail;
    @Before
    public void setUpClass(){
        articleDetail = new ArticleDetail();
    }

    @Test
    public void getId() throws Exception {
        articleDetail.setId(89);
        assertEquals(89,articleDetail.getId());
    }

    @Test
    public void getArticle() throws Exception {
        Article a = new Article();
        articleDetail.setArticle(a);
        assertEquals(a,articleDetail.getArticle());
    }

    @Test
    public void getFeature() throws Exception {
        ArticleFeature articleFeature = new ArticleFeature();
        articleFeature.setAttributeName("pressione");
        articleDetail.setFeature(articleFeature);
        assertEquals(articleFeature,articleDetail.getFeature());
    }

    @Test
    public void getAttributeValue() throws Exception {
        articleDetail.setAttributeValue("32");
        assertEquals("32",articleDetail.getAttributeValue());
    }

}