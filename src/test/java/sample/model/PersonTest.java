package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by vincenzo on 18/04/17.
 */
public class PersonTest {
    Person p;
    @Before
    public void setUpClass(){
        p = new Person();
    }

    @Test
    public void getId() throws Exception {
        p.setId(89);
        Assert.assertEquals(89,p.getId());
    }

    @Test
    public void getName() throws Exception {
        p.setName("name");
        Assert.assertEquals("name",p.getName());
    }

    @Test
    public void getLastname() throws Exception {
        p.setLastname("lastname");
        Assert.assertEquals("lastname",p.getLastname());
    }

    @Test
    public void getEmail() throws Exception {
        p.setEmail("mail@gmail.com");
        Assert.assertEquals("mail@gmail.com",p.getEmail());
    }

}