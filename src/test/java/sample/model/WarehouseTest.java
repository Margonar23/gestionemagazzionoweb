package sample.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 18/04/17.
 */
public class WarehouseTest {
    Warehouse warehouse;
    @Before
    public void setUpClass(){
        warehouse = new Warehouse();
    }
    @Test
    public void getId() throws Exception {
        warehouse.setId(89);
        assertEquals(89,warehouse.getId());
    }

    @Test
    public void getUsers() throws Exception {
        Users users = new Users();
        warehouse.setUsers(users);
        assertEquals(users,warehouse.getUsers());
    }

    @Test
    public void isActive() throws Exception {
        assertEquals(false,warehouse.isActive());
        warehouse.setActive(true);
        assertEquals(true,warehouse.isActive());
    }

    @Test
    public void getMovements() throws Exception {
        Movement m = new Movement();
        ArrayList<Movement> movements = new ArrayList<>();
        movements.add(m);
        warehouse.setMovements(movements);
        assertEquals(movements,warehouse.getMovements());
    }


}