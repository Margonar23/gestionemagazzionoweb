package sample.model;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 18/04/17.
 */
public class MovementTest {
    Movement movement;
    @Before
    public void setUpClass(){
        movement = new Movement();
    }
    @Test
    public void getId() throws Exception {
        movement.setId(89);
        assertEquals(89,movement.getId());
    }

    @Test
    public void getUsers() throws Exception {
        Users u = new Users();
        movement.setUsers(u);
        assertEquals(u,movement.getUsers());
    }

    @Test
    public void getItem() throws Exception {
        Item i = new Item();
        movement.setItem(i);
        assertEquals(i,movement.getItem());
    }

    @Test
    public void getDate() throws Exception {
        LocalDate ld = LocalDate.now();
        movement.setDate(ld);
        assertEquals(ld,movement.getDate());
    }

    @Test
    public void getFrom() throws Exception {
        Location l = new ConstructionSite();
        movement.setFrom(l);
        assertEquals(l,movement.getFrom());

    }

    @Test
    public void getTo() throws Exception {
        Location l = new ConstructionSite();
        movement.setTo(l);
        assertEquals(l,movement.getTo());
    }

}