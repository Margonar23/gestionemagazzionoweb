package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 18/04/17.
 */
public class ConstructionSiteTest {
    ConstructionSite constructionSite;
    @Before
    public void setUpClass(){
        constructionSite = new ConstructionSite();
    }
    @Test
    public void getId() throws Exception {
        constructionSite.setId(89);
        Assert.assertEquals(89,constructionSite.getId());
    }

    @Test
    public void getRespMunicipality() throws Exception {
        RespMunicipality respMunicipality = new RespMunicipality();
        respMunicipality.setId(89);
        respMunicipality.setConstructionSite(constructionSite);
        respMunicipality.setCredit(8956.45);
        respMunicipality.setDate(new Date());
        respMunicipality.setNumber(895222);
        constructionSite.setRespMunicipality(respMunicipality);
        Assert.assertEquals(respMunicipality,constructionSite.getRespMunicipality());
    }

    @Test
    public void getConstructionFirm() throws Exception {
       Company c = new Company();
        constructionSite.setConstructionFirm(c);
        assertEquals(c,constructionSite.getConstructionFirm());
    }

    @Test
    public void getFlooringCompany() throws Exception {
        Company c = new Company();
        constructionSite.setFlooringCompany(c);
        assertEquals(c,constructionSite.getFlooringCompany());
    }

    @Test
    public void getProjectManager() throws Exception {
        Users u = new Users();
        constructionSite.setProjectManager(u);
        assertEquals(u,constructionSite.getProjectManager());
    }

    @Test
    public void getResponsible() throws Exception {
        Users u = new Users();
        constructionSite.setResponsible(u);
        assertEquals(u,constructionSite.getResponsible());
    }

    @Test
    public void getMovementsC() throws Exception {

    }

    @Test
    public void isActive() throws Exception {
        Assert.assertEquals(false, constructionSite.isActive());
        constructionSite.setActive(true);
        Assert.assertEquals(true, constructionSite.isActive());
    }

}