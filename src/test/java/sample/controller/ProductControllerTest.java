package sample.controller;

import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import sample.model.*;
import sample.repo.*;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by vincenzo on 08/05/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private ArticleCategoriesRepository articleCategoriesRepository;
    @Autowired
    private ArticleFeaturesRepository articleFeaturesRepository;
    @Autowired
    protected WarehouseRepository warehouseRepository;
    @Autowired
    protected LocationRepository locationRepository;
    @Autowired
            ItemRepository itemRepository;
    @Autowired
            ProductWarehouseRepository productWarehouseRepository;

    Article article = new Article();
    ArticleCategory articleCategory;
    ArrayList<ArticleFeature> features;
    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .defaultRequest(get("/").with(user("marglu").password("password").roles("super user")))
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
        article = new Article();
        article.setActive(false);
        article.setName("TUBO PVC");
        article.setBrand("TUBO MARCA");
        article.setArticle_nr("TPVC_43");
         articleCategory = new ArticleCategory();
        articleCategory.setCategory("tubo");
        ArticleFeature articleFeature = new ArticleFeature();
        articleFeature.setAttributeName("pressione");
         features = new ArrayList<>();
        features.add(articleFeature);
        articleCategory.setFeatures(features);
        article.setArticleCategory(articleCategory);
        ArrayList<ArticleDetail> details = new ArrayList<>();

        for (ArticleFeature feature : article.getArticleCategory().getFeatures()){
            ArticleDetail articleDetail = new ArticleDetail();
            articleDetail.setFeature(feature);
            articleDetail.setAttributeValue("32");
            details.add(articleDetail);
        }

        article.setDetails(details);
        for (ArticleDetail articleDetail : article.getDetails()) {
            System.out.println(articleDetail.getFeature().getAttributeName()+ ": " +articleDetail.getAttributeValue());
        }
    }
    @Ignore
    @Test
    public void greetingRoot() throws Exception {
        for (Item item : itemRepository.findAll()) {
            if (productWarehouseRepository.findByArticleAndWarehouse(item.getArticle(), item.getLocation()).size() == 0) {
                Product_warehouse pw = new Product_warehouse();
                pw.setArticle(item.getArticle());
                pw.setWarehouse((Warehouse) item.getLocation());
                pw.setWarining(10);
                pw.setAlert(5);
                productWarehouseRepository.save(pw);
            }
        }
        this.mockMvc.perform(get("/products")).andExpect(status().isOk());
        Matcher<String> matcher = allOf(containsString("Prodotti"));
        this.mockMvc.perform(get("/products")).andExpect(content().string(matcher));
        matcher = allOf(containsString("Prodotti critici"));
        this.mockMvc.perform(get("/products")).andExpect(content().string(matcher));
    }

    @Test
    public void productsRegistration() throws Exception {
        this.mockMvc.perform(get("/product/register")).andExpect(status().isOk());
        Matcher<String> matcher = allOf(containsString("Inserisci nuovo prodotto"));
        this.mockMvc.perform(get("/product/register")).andExpect(content().string(matcher));
        matcher = allOf(containsString("Gestione prodotto"));
        this.mockMvc.perform(get("/product/register")).andExpect(content().string(matcher));
    }

    @Test
    public void productsRegistration1() throws Exception {
        articleCategoriesRepository.save(articleCategory);
        String[] attributes = {"30"};
        this.mockMvc.perform(post("/product/register").param("category","1").param("attributeValue",attributes).
                param("name",article.getName()).param("brand",article.getBrand()).param("article_nr",article.getArticle_nr())).andExpect(redirectedUrl("/products"));

    }

    @Test
    public void productsDelete() throws Exception {
        articleFeaturesRepository.save(features);
        articleCategoriesRepository.save(articleCategory);
        Article a = articleRepository.save(article);
      this.mockMvc.perform(get("/product/delete/{id}",1)).andExpect(redirectedUrl("/products"));
        a = articleRepository.findOne(a.getId());
        Assert.assertEquals(false,a.isActive());
       this.mockMvc.perform(get("/product/enable/{id}",a.getId())).andExpect(redirectedUrl("/products"));
       a = articleRepository.findOne(a.getId());
       Assert.assertEquals(true,a.isActive());

    }

    @Test
    public void productsUpdate() throws Exception {
        articleFeaturesRepository.save(features);
        articleCategoriesRepository.save(articleCategory);
        Article a = articleRepository.save(article);
        this.mockMvc.perform(get("/product/update/{id}",a.getId())).andExpect(status().isOk());
        String[] attributes = {"30"};
        this.mockMvc.perform(post("/product/update/{id}", a.getId()).param("category","1").param("attributeValues",attributes).
                param("name",article.getName()).param("brand",article.getBrand()).param("article_nr",article.getArticle_nr())).andExpect(redirectedUrl("/products"));

    }

    @Test
    public void productsInformation() throws Exception {
        articleFeaturesRepository.save(features);
        articleCategoriesRepository.save(articleCategory);
        Article a = articleRepository.save(article);
        this.mockMvc.perform(get("/product/information/{id}",a.getId())).andExpect(status().isOk());
        Matcher<String> matcher = allOf(containsString("Informazioni prodotto"));
        this.mockMvc.perform(get("/product/information/{id}",a.getId())).andExpect(content().string(matcher));

    }

    @Test
    public void productsBarcode() throws Exception {
        Warehouse w = new Warehouse();
        w.setName("magazzino locarno");
        warehouseRepository.save(w);

        this.mockMvc.perform(get("/product/barcode")).andExpect(status().isOk());

    }

    @Test
    public void categoryRegistration() throws Exception {
        this.mockMvc.perform(get("/product/category/register")).andExpect(status().isOk());
        this.mockMvc.perform(post("/product/category/register").param("category",articleCategory.getCategory())).andExpect(redirectedUrl("/product/category"));
    }

    @Test
    public void categoryConfig() throws Exception {
        ArticleCategory ac = articleCategoriesRepository.save(articleCategory);
        this.mockMvc.perform(get("/product/category/configuration/{id}",ac.getId())).andExpect(status().isOk());
        this.mockMvc.perform(post("/product/category/configuration/{id}",ac.getId()).param("attributeName", "pressione")).andExpect(redirectedUrl("/product/category/configuration/"+ac.getId()));
    }



    @Test
    public void category() throws Exception {
        this.mockMvc.perform(get("/product/category")).andExpect(status().isOk());
    }



    @Test
    public void addItem() throws Exception {
        Warehouse w = new Warehouse();
        w.setName("magazzino locarno");
        w = warehouseRepository.save(w);
        articleFeaturesRepository.save(features);
        articleCategoriesRepository.save(articleCategory);
        Article a = articleRepository.save(article);
        String[] idArticle = {""+a.getId()};
        String[] alarm = {"2"};
        String[] warning = {"3"};
        String[] pezzi = {"1"};
        String[] qta = {"5"};

        this.mockMvc.perform(post("/product/register/item/{idLocation}", w.getId()).param("idArticle",idArticle).param("qta",qta)
                .param("pezzi",pezzi).param("warning",warning).param("alarm",alarm)).andExpect(redirectedUrl("/product/barcodePrint"));
    }

    @Test
    public void printArticle() throws Exception {
        articleFeaturesRepository.save(features);
        articleCategoriesRepository.save(articleCategory);
        Article a = articleRepository.save(article);
        this.mockMvc.perform(get("/product/print/article/{idArticle}",a.getId())).andExpect(redirectedUrl("/product/barcodePrint"));

    }

    @Test
    public void printItem() throws Exception {
        this.mockMvc.perform(get("/product/print/item/{idItem}",1l)).andExpect(redirectedUrl("/product/barcodePrint"));
    }

    @Test
    public void moveItemTo() throws Exception {
        articleFeaturesRepository.save(features);
        articleCategoriesRepository.save(articleCategory);
        Article a = articleRepository.save(article);

        this.mockMvc.perform(post("/product/move").param("toLocation","1").param("itemCode",a.getArticle_nr()).
                param("name",article.getName()).param("brand",article.getBrand())).andExpect(redirectedUrl("/product/barcode"));
    }

}