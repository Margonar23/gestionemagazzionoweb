package sample.controller;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import sample.model.Article;
import sample.repo.ArticleRepository;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.core.AllOf.allOf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Margonar on 24.04.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IndexControllerTest {


    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;


    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .defaultRequest(get("/").with(user("marglu").password("password").roles("super user")))
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Test
    public void forbiddenTest() throws Exception{
        this.mockMvc.perform(get("/notAuthorized")).andExpect(status().isOk());
        Matcher<String> matcher = allOf(either(containsString("Attenzione")).or(containsString("Non sei autorizzato a visitare questa pagina!")));
        this.mockMvc.perform(get("/notAuthorized")).andExpect(content().string(matcher));
    }

    @Test
    public void dashboardTest() throws Exception{
        this.mockMvc.perform(get("/dashboard")).andExpect(status().isOk());
        Matcher<String> matcher = allOf(either(containsString("Pannello di gestione")).or(containsString("Visualizzazione grafica")));
        this.mockMvc.perform(get("/dashboard")).andExpect(content().string(matcher));
    }
}
