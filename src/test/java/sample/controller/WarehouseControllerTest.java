package sample.controller;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import sample.model.*;
import sample.repo.*;


import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.core.AllOf.allOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;



/**
 * Created by vincenzo on 18/04/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT)

public class WarehouseControllerTest {

    @Autowired
    ArticleRepository articleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    ConstructioSiteRepository constructionSiteRepository;
    @Autowired
    LogOperationRepository logOperationRepository;


    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;
    Warehouse w;

    //@Autowired
    //private Filter springContextFilter;


    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .defaultRequest(get("/").with(user("marglu").password("password").roles("super user")))
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
        w = new Warehouse();
        w.setActive(true);
        w.setId(100);
        long idUserResponsable = 1;
        w.setUsers(userRepository.findOne(idUserResponsable));
        w.setName("magloc");
        w.setNation("svizzera");
        w.setRoute("via gastaldi 21");
        w.setNap("21050");
    }


    @Test
    public void warehouseManagement() throws Exception {
        this.mockMvc.perform(get("/warehouse/register")).andExpect(status().isOk());

    }

    @Test
    public void warehouseRegistration() throws Exception {
        this.mockMvc.perform(get("/warehouse/register")).andExpect(status().isOk());
        Matcher<String> matcher = allOf(either(containsString("Inserisci nuovo magazzino")).or(containsString("Gestione Magazzino")));
        this.mockMvc.perform(get("/warehouse/register")).andExpect(content().string(matcher));

        long idUserResponsable = 1;
        w.setUsers(userRepository.findOne(idUserResponsable));


        //this.mockMvc.perform(post("/warehouse/register")).andExpect(status().isOk());
        RequestBuilder request = post("/warehouse/register").param("name", w.getName())
                .param("nation",w.getNation()).param("nap",w.getNap()).param("route",w.getRoute()).param("responsable",idUserResponsable+"").param("id",w.getId()+"").param("isActive",w.isActive()+"").param("id",w.getId()+"");
        this.mockMvc.perform(request).andExpect(redirectedUrl("/warehouse"));
        ArrayList<Warehouse> arrayList = (ArrayList<Warehouse>) warehouseRepository.findAll();
        Warehouse w1 = arrayList.get(arrayList.size()-1);
        Assert.assertEquals(true,w1!=null);
        ArrayList<LogOperation> logs = (ArrayList<LogOperation>) logOperationRepository.findAll();
        Assert.assertEquals("REGISTRAZIONE",logs.get(logs.size()-1).getOperation());
        Assert.assertEquals("Magazzino",logs.get(logs.size()-1).getData());
        Assert.assertEquals("marglu",logs.get(logs.size()-1).getUser());
        warehouseRepository.delete(w1);

    }


    @Test
    public void warehouseUpdate() throws Exception {

        w = warehouseRepository.save(w);
        mockMvc.perform(get("/warehouse/update/{id}", w.getId())).andExpect(status().isOk());
        warehouseRepository.delete(w);

    }



    @Test
    public void warehouseDelete() throws Exception {
         w = warehouseRepository.save(w);
        this.mockMvc.perform(get("/warehouse/delete/{id}",w.getId()));
        Assert.assertEquals(false,warehouseRepository.findOne(w.getId()).isActive());
       // this.mockMvc.perform(post("/user/roles").param("role", "ROLE_ADMIN"));
    }

    @Test
    public void warehouseActive() throws Exception {

        w = warehouseRepository.save(w);
        this.mockMvc.perform(get("/warehouse/open/{id}",w.getId())).andExpect(redirectedUrl("/warehouse"));
        warehouseRepository.delete(w);
    }



    @Test
    public void warehouseInventory() throws Exception {
        w = warehouseRepository.save(w);
        MvcResult result  = this.mockMvc.perform(get("/warehouse/inventory/{id}",w.getId())).andExpect(status().isOk()).andReturn();
        ArrayList<Article> list2 = (ArrayList<Article>) result.getModelAndView().getModel().get("articles");
        Assert.assertEquals(true, list2!=null);
        warehouseRepository.delete(w);
    }

    @Test
    public void registerEntryToWarehouse() throws Exception {
 // /warehouse/addproduct/{id}
        w = warehouseRepository.save(w);
        MvcResult result  = this.mockMvc.perform(get("/warehouse/addproduct/{id}",w.getId())).andExpect(status().isOk()).andReturn();
        long idWarehouse = (long) result.getModelAndView().getModel().get("idWarehouse");
        Assert.assertEquals(w.getId(), idWarehouse);

    }





}
