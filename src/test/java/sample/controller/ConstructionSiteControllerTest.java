package sample.controller;

import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import sample.model.*;
import sample.repo.*;

import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by vincenzo on 09/05/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ConstructionSiteControllerTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ConstructioSiteRepository constructionSiteRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private RespMunicipalityRepository respMunicipalityRepository;
    @Autowired
    protected PersonRepository personRepository;
    @Autowired
    protected ArticleRepository articleRepository;
    @Autowired
            WarehouseRepository warehouseRepository;
    @Autowired
            ItemRepository itemRepository;
    @Autowired
    ProductWarehouseRepository productWarehouseRepository;


    ConstructionSite constructionSite;
    RespMunicipality respMunicipality;
    Company c;
    Users u;
    Person p;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .defaultRequest(get("/").with(user("marglu").password("password").roles("super user")))
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
        constructionSite = new ConstructionSite();
        constructionSiteRepository.save(constructionSite);
        respMunicipality = new RespMunicipality();
        respMunicipality.setConstructionSite(constructionSite);
        respMunicipality.setCredit(8956.45);
        respMunicipality.setDate(new Date());
        respMunicipality.setNumber(895222);
        constructionSite.setRespMunicipality(respMunicipality);
        c = new Company();
        constructionSite.setConstructionFirm(c);
        constructionSite.setFlooringCompany(c);
        u = new Users();
        p = new Person();
        constructionSite.getFlooringCompany().setContact(p);
        constructionSite.getConstructionFirm().setContact(p);
        p = personRepository.save(p);
        respMunicipality = respMunicipalityRepository.save(respMunicipality);
        constructionSite.setProjectManager(u);
        constructionSite.setResponsible(u);
        u = userRepository.save(u);
        c = companyRepository.save(c);
        constructionSite = constructionSiteRepository.save(constructionSite);
    }

    @Test
    public void constructionManagement() throws Exception {
        this.mockMvc.perform(get("/construction")).andExpect(status().isOk());
        Matcher<String> matcher = allOf(containsString("Gestione cantieri"));
        this.mockMvc.perform(get("/construction")).andExpect(content().string(matcher));
    }

    @Ignore
    @Test
    public void constructionRegistration() throws Exception {
        this.mockMvc.perform(post("/construction/register").param("name", "cantiere x").param("dateRespMun", "2017-01-14")
                .param("pManager", "" + u.getId()).param("responsable", "" + u.getId()).param("idPavimentazione", "" + c.getId()).param("idCostruzione", "" + c.getId()).param("idPersonPavimentazione", "" + p.getId()).param("idPersonCostruzione", "" + p.getId())).andExpect(status().isOk());

    }

    @Test
    public void constructionRegistration1() throws Exception {
        this.mockMvc.perform(get("/construction/register")).andExpect(status().isOk());

    }


    @Test
    public void constructionInformation() throws Exception {
        this.mockMvc.perform(get("/construction/information/{id}", constructionSite.getId())).andExpect(status().isOk());
    }

    @Test
    public void constructionToWarehouse() throws Exception {
        Article a = new Article();
        a = articleRepository.save(a);
        String[] articles = {""+a.getId()};
        this.mockMvc.perform(post("/construction/constructionToWarehouse/{id}", constructionSite.getId())
                .param("checked", articles)).andExpect(redirectedUrl("/construction/constructionToWarehouse/"+constructionSite.getId()));
    }

    @Test
    public void constructionDelete() throws Exception {
        this.mockMvc.perform(get("/construction/delete/{id}",constructionSite.getId())).andExpect(redirectedUrl("/construction"));
        Assert.assertEquals(false,constructionSiteRepository.findOne(constructionSite.getId()).isActive());
        this.mockMvc.perform(get("/construction/open/{id}",constructionSite.getId())).andExpect(redirectedUrl("/construction"));
        Assert.assertEquals(true,constructionSiteRepository.findOne(constructionSite.getId()).isActive());
    }


    @Test
    public void construction() throws Exception {
        Warehouse w = new Warehouse();
        Item i = new Item();
        Article a = new Article();
        i.setArticle(a);
        w = warehouseRepository.save(w);
        articleRepository.save(a);
        i = itemRepository.save(i);
        Product_warehouse pw = new Product_warehouse();
        pw.setArticle(a);
        pw.setWarehouse(w);
        productWarehouseRepository.save(pw);

        String[] items = {""+i.getId()};

        this.mockMvc.perform(post("/contruction/constructionToWarehouse/{id}",constructionSite.getId())
                .param("items", items).param("warehouse",""+ w.getId())).andExpect(redirectedUrl("/construction/information/"+constructionSite.getId()));
    }


}