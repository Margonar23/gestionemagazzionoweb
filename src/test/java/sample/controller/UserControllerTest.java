package sample.controller;
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import sample.model.UserRole;
import sample.model.Users;
import sample.repo.UserRepository;
import sample.repo.UserRoleRepository;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.core.AllOf.allOf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Margonar on 24.04.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .defaultRequest(get("/").with(user("marglu").password("password").roles("super user")))
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }


    @Test
    public void userResetPassword() throws Exception {
        this.mockMvc.perform(get("/user/resetpassword/{id}","1")).andExpect(redirectedUrl("/user"));
        Users u = userRepository.findOne(1l);
        PasswordEncoder token = new BCryptPasswordEncoder();
        Assert.assertEquals(true,token.matches("password", u.getPassword()));
    }

    @Test
    public void userLoggedUpdate() throws Exception {
        Users u = userRepository.findOne(1l);
        this.mockMvc.perform(post("/user/logged/update/{id}","1").param("role","1").param("name",u.getName()).param("lastname",u.getLastname()).
                param("username",u.getUsername()).param("email",u.getEmail())).andExpect(redirectedUrl("/user/logged"));

    }

    @Test
    public void userLogged() throws Exception {
        this.mockMvc.perform(get("/user/logged")).andExpect(status().isOk());
        Matcher<String> matcher = allOf(containsString("Gestione Utenti"));
        this.mockMvc.perform(get("/user/logged")).andExpect(content().string(matcher));

    }

    @Test
    public void userChangeLevelNotification() throws Exception {
        this.mockMvc.perform(post("/user/logged/notification").param("usersubstitute","1").param("notificationS","1")).andExpect(redirectedUrl("/user/logged"));
        Users u = userRepository.findUsersByUsername("marglu");
        Assert.assertEquals(u.getNotifications().getNotification(),"low");
        this.mockMvc.perform(post("/user/logged/notification").param("usersubstitute","1").param("notificationS","3")).andExpect(redirectedUrl("/user/logged"));
        u = userRepository.findUsersByUsername("marglu");
        Assert.assertEquals(u.getNotifications().getNotification(),"busy");
        Assert.assertEquals(u.getSubstitute().getUsername(),userRepository.findOne(1l).getUsername());
    }

    @Test
    public void userChangePassword() throws Exception {

        this.mockMvc.perform(post("/user/logged/changepassword").param("oldPassword","password").param("newPassword","password1").param("newPasswordRepet","password1")).andExpect(redirectedUrl("/user/logged"));
        Users u = userRepository.findUsersByUsername("marglu");
        PasswordEncoder token = new BCryptPasswordEncoder();
        Assert.assertEquals(true,token.matches("password1", u.getPassword()));
        this.mockMvc.perform(post("/user/logged/changepassword").param("oldPassword","password1").param("newPassword","password").param("newPasswordRepet","password")).andExpect(redirectedUrl("/user/logged"));
         u = userRepository.findUsersByUsername("marglu");
         token = new BCryptPasswordEncoder();
        Assert.assertEquals(true,token.matches("password", u.getPassword()));

        this.mockMvc.perform(post("/user/logged/changepassword").param("oldPassword","boh").param("newPassword","password").param("newPasswordRepet","password")).andExpect(redirectedUrl("/user/logged?error"));
        this.mockMvc.perform(post("/user/logged/changepassword").param("oldPassword","password").param("newPassword","boh").param("newPasswordRepet","password")).andExpect(redirectedUrl("/user/logged?error"));

    }

    @Test
    public void rolesManagement() throws Exception {
        this.mockMvc.perform(get("/user/roles")).andExpect(status().isOk());
        Matcher<String> matcher = allOf(containsString("Gestione ruoli"));
        this.mockMvc.perform(get("/user/roles")).andExpect(content().string(matcher));
         matcher = allOf(containsString("Lista ruoli"));
        this.mockMvc.perform(get("/user/roles")).andExpect(content().string(matcher));

    }

    @Test
    public void rolesAdd() throws Exception {
        this.mockMvc.perform(post("/user/roles").param("role","nuovo ruolo")).andExpect(redirectedUrl("/user/roles"));
    }

    @Test
    public void updateRole() throws Exception {
        this.mockMvc.perform(get("/user/roles/update/{id}", 1)).andExpect(status().isOk());
    }

    @Test
    public void disableUser() throws Exception {

        this.mockMvc.perform(get("/user/disable/{id}", 1)).andExpect(redirectedUrl("/user"));
        Users u = userRepository.findOne(1l);
        Assert.assertEquals(false,u.isEnabled());
        this.mockMvc.perform(get("/user/enable/{id}", 1)).andExpect(redirectedUrl("/user"));
        u = userRepository.findOne(1l);
        Assert.assertEquals(true,u.isEnabled());
    }


    @Test
    public void addFunctionalityToRole() throws Exception {
        rolesAdd();
        ArrayList<UserRole> roleList = (ArrayList)userRoleRepository.findAll();
        String[]  roles = {"1","3","6"};
        this.mockMvc.perform(post("/user/roles/update/{id}/addFunctionality",roleList.size()-1).param("roles",roles))
                .andExpect(redirectedUrl("/user/roles/update/"+(roleList.size()-1)));
    }



    @Test
    public void userManagementTest() throws Exception {
        this.mockMvc.perform(get("/user")).andExpect(status().isOk());

        Users user = userRepository.findUsersByUsername("marglu");

        Matcher<String> userMatcher = allOf(either(containsString(user.getName())).or(containsString(user.getEmail())).or(containsString(user.getLastname())));
        this.mockMvc.perform(get("/user")).andExpect(content().string(userMatcher));
    }

    @Test
    public void userRegistrationTest() throws Exception {
        // TEST GET
        // controllo lo stato della pagina.
        this.mockMvc.perform(get("/user/register")).andExpect(status().isOk());
        // controllo se la pagina contiene una stringa
        Matcher<String> matcher = allOf(containsString("Inserisci nuovo utente"));
        this.mockMvc.perform(get("/user/register")).andExpect(content().string(matcher));

        // TEST POST

        Users user = new Users();
        user.setName("Pippo");
        user.setLastname("Pluto");
        user.setEmail("pippo.pluto@paperino.com");
        user.setUsername("pippopluto");

        // invio uno user al url /user/register
        this.mockMvc.perform(post("/user/register")
                .param("role", "1")
                .param("id", user.getId() + "")
                .param("name", user.getName())
                .param("lastname", user.getLastname())
                .param("username", user.getUsername())
                .param("email", user.getEmail())
        ).andExpect(redirectedUrl("/user"));
        // controllo se esiste sul database l'utente appena registrato
        Assert.assertEquals(true, userRepository.findUsersByUsername("pippopluto") != null);
    }

    @Test
    public void userUpdateTest() throws Exception {
        this.mockMvc.perform(get("/user/update/{id}","1")).andExpect(status().isOk());
    }

    @Test
    public void roleRegistration() throws Exception {
        this.mockMvc.perform(post("/user/roles").param("role", "ROLE_ADMIN"));
    }


}
